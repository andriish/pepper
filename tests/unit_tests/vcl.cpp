#include "tests.h"

#include "src/vcl_vec4.h"

DOCTEST_TEST_CASE("VCL_wrappers")
{
  Vcl_vec4 a {1.0, 2.0, 3.0, 4.0};
  Vcl_vec4 b {2.0, 3.0, 4.0, 5.0};
  DOCTEST_CHECK(a * b == doctest::Approx(-36.0));
  Vcl_vec4 c = a + b;
  DOCTEST_CHECK(c[0] == doctest::Approx(3.0));
  DOCTEST_CHECK(c[1] == doctest::Approx(5.0));
  DOCTEST_CHECK(c[2] == doctest::Approx(7.0));
  DOCTEST_CHECK(c[3] == doctest::Approx(9.0));
  Vcl_cvec4 d {1.0i, 2.0, 0.0, 1.0 - 1.0i};
  C ad = d * a;
  DOCTEST_CHECK(ad.real() == doctest::Approx(-8.0));
  DOCTEST_CHECK(ad.imag() == doctest::Approx(5.0));
  Vcl_cvec4 e = d * (3.0 + 4.0i);
  DOCTEST_CHECK(e[0].real() == doctest::Approx(-4.0));
  DOCTEST_CHECK(e[1].real() == doctest::Approx(6.0));
  DOCTEST_CHECK(e[2].real() == doctest::Approx(0.0));
  DOCTEST_CHECK(e[3].real() == doctest::Approx(7.0));
  DOCTEST_CHECK(e[0].imag() == doctest::Approx(3.0));
  DOCTEST_CHECK(e[1].imag() == doctest::Approx(8.0));
  DOCTEST_CHECK(e[2].imag() == doctest::Approx(0.0));
  DOCTEST_CHECK(e[3].imag() == doctest::Approx(1.0));
  Vcl_cvec4 f {-1.0, 1.0+2.0i, 2.0, 1.0i};
  C df = d * f;
  DOCTEST_CHECK(df.real() == doctest::Approx(-3.0));
  DOCTEST_CHECK(df.imag() == doctest::Approx(-6.0));
}
