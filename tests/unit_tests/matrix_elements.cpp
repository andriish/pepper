#include "tests.h"

#include "pepper/config.h"

#include "src/cuts.h"
#include "src/event_data.h"
#include "src/flavour_process.h"
#include "src/generator.h"
#include "src/rambo.h"
#include "src/recursion.h"
#include <iomanip>
#include <limits>

#include "phase_space_points.h"

Settings me_settings()
{
  // Adapt common test settings for matrix element tests
  Settings s {settings};
  s.helicity_settings.treatment = Helicity_treatment::summed;
  if (s.helicity_settings.summing_options == Helicity_summing_option::simple) {
    s.helicity_settings.summing_options |=
        (Helicity_summing_option::cached |
         Helicity_summing_option::parity_symmetric);
  }
  s.batch_size = 1;
  s.n_batches = 1;
  s.me2_only = true;
  s.event_output_disabled = true;
  s.phase_space_settings.use_cached_results_if_possible = false;
  s.cuts.ll_m2_min = 0.0;
  s.cuts.ll_m2_max = std::numeric_limits<double>::max();
  s.fixed_alpha_s = 0.118;
  return s;
}

DOCTEST_TEST_CASE("gggg event")
{
  // Setup a generator for a single g g -> g g event.
  Settings s {me_settings()};
  s.process_specs = {"g g -> g g"};
  Generator gen {s, sm};
  gen.process_group().set_fixed_momenta(phase_space_points_n4[0]);
  MC_result mc_result = gen.generate_batches(s.n_batches);

  Event_handle evt {gen.event_data()};
  evt.debug_p(0);
  evt.pull_momenta_from_device();
  DOCTEST_CHECK(evt.host.e(0, 0) == doctest::Approx(-7000));
  DOCTEST_CHECK(evt.host.e(0, 1) == doctest::Approx(-7000));
  DOCTEST_CHECK(evt.host.e(0, 2) == doctest::Approx(6999.9999999999627));
  DOCTEST_CHECK(evt.host.e(0, 3) == doctest::Approx(6999.9999999999809));
  DOCTEST_CHECK(evt.host.px(0, 0) == doctest::Approx(0));
  DOCTEST_CHECK(evt.host.px(0, 1) == doctest::Approx(0));
  DOCTEST_CHECK(evt.host.px(0, 2) == doctest::Approx(5889.1628963490375));
  DOCTEST_CHECK(evt.host.px(0, 3) == doctest::Approx(-5889.1628963490011));
  DOCTEST_CHECK(evt.host.py(0, 0) == doctest::Approx(0));
  DOCTEST_CHECK(evt.host.py(0, 1) == doctest::Approx(0));
  DOCTEST_CHECK(evt.host.py(0, 2) == doctest::Approx(1312.2138663035253));
  DOCTEST_CHECK(evt.host.py(0, 3) == doctest::Approx(-1312.2138663035221));
  DOCTEST_CHECK(evt.host.pz(0, 0) == doctest::Approx(-7000));
  DOCTEST_CHECK(evt.host.pz(0, 1) == doctest::Approx(7000));
  DOCTEST_CHECK(evt.host.pz(0, 2) == doctest::Approx(3549.0639821432928));
  DOCTEST_CHECK(evt.host.pz(0, 3) == doctest::Approx(-3549.063982143311));

  DOCTEST_CHECK(mc_result.n_nonzero == 1);

  evt.pull_currents_from_device();

  // Polarisation vectors for gluon 0
  DOCTEST_CHECK(evt.host.c0(0, 0, Helicity::plus).real() == doctest::Approx(-0.49999999999999994));
  DOCTEST_CHECK(evt.host.c0(0, 0, Helicity::plus).imag() == doctest::Approx(0.49999999999999994));
  DOCTEST_CHECK(evt.host.c1(0, 0, Helicity::plus).real() == doctest::Approx(-0.70710678118654746));
  DOCTEST_CHECK(evt.host.c1(0, 0, Helicity::plus).imag() == doctest::Approx(0));
  DOCTEST_CHECK(evt.host.c2(0, 0, Helicity::plus).real() == doctest::Approx(0));
  DOCTEST_CHECK(evt.host.c2(0, 0, Helicity::plus).imag() == doctest::Approx(0.70710678118654746));
  DOCTEST_CHECK(evt.host.c3(0, 0, Helicity::plus).real() == doctest::Approx(-0.49999999999999994));
  DOCTEST_CHECK(evt.host.c3(0, 0, Helicity::plus).imag() == doctest::Approx(0.49999999999999994));
  DOCTEST_CHECK(evt.host.c0(0, 0, Helicity::minus).real() == doctest::Approx(-0.49999999999999994));
  DOCTEST_CHECK(evt.host.c0(0, 0, Helicity::minus).imag() == doctest::Approx(-0.49999999999999994));
  DOCTEST_CHECK(evt.host.c1(0, 0, Helicity::minus).real() == doctest::Approx(-0.70710678118654746));
  DOCTEST_CHECK(evt.host.c1(0, 0, Helicity::minus).imag() == doctest::Approx(0));
  DOCTEST_CHECK(evt.host.c2(0, 0, Helicity::minus).real() == doctest::Approx(0));
  DOCTEST_CHECK(evt.host.c2(0, 0, Helicity::minus).imag() == doctest::Approx(-0.70710678118654746));
  DOCTEST_CHECK(evt.host.c3(0, 0, Helicity::minus).real() == doctest::Approx(-0.49999999999999994));
  DOCTEST_CHECK(evt.host.c3(0, 0, Helicity::minus).imag() == doctest::Approx(-0.49999999999999994));
  // Polarisation vectors for gluon 1
  DOCTEST_CHECK(evt.host.c0(0, 1, Helicity::plus).real() == doctest::Approx(0.5));
  DOCTEST_CHECK(evt.host.c0(0, 1, Helicity::plus).imag() == doctest::Approx(0.5));
  DOCTEST_CHECK(evt.host.c1(0, 1, Helicity::plus).real() == doctest::Approx(0.70710678118654746));
  DOCTEST_CHECK(evt.host.c1(0, 1, Helicity::plus).imag() == doctest::Approx(-8.6595605623549316e-17));
  DOCTEST_CHECK(evt.host.c2(0, 1, Helicity::plus).real() == doctest::Approx(8.6595605623549316e-17));
  DOCTEST_CHECK(evt.host.c2(0, 1, Helicity::plus).imag() == doctest::Approx(0.70710678118654746));
  DOCTEST_CHECK(evt.host.c3(0, 1, Helicity::plus).real() == doctest::Approx(-0.5));
  DOCTEST_CHECK(evt.host.c3(0, 1, Helicity::plus).imag() == doctest::Approx(-0.5));
  DOCTEST_CHECK(evt.host.c0(0, 1, Helicity::minus).real() == doctest::Approx(0.5));
  DOCTEST_CHECK(evt.host.c0(0, 1, Helicity::minus).imag() == doctest::Approx(-0.5));
  DOCTEST_CHECK(evt.host.c1(0, 1, Helicity::minus).real() == doctest::Approx(0.70710678118654746));
  DOCTEST_CHECK(evt.host.c1(0, 1, Helicity::minus).imag() == doctest::Approx(-8.6595605623549316e-17));
  DOCTEST_CHECK(evt.host.c2(0, 1, Helicity::minus).real() == doctest::Approx(-8.6595605623549316e-17));
  DOCTEST_CHECK(evt.host.c2(0, 1, Helicity::minus).imag() == doctest::Approx(-0.70710678118654746));
  DOCTEST_CHECK(evt.host.c3(0, 1, Helicity::minus).real() == doctest::Approx(-0.5));
  DOCTEST_CHECK(evt.host.c3(0, 1, Helicity::minus).imag() == doctest::Approx(0.5));
  // Polarisation vectors for gluon 2
  DOCTEST_CHECK(evt.host.c0(0, 2, Helicity::plus).real() == doctest::Approx(-0.78091005029771887));
  DOCTEST_CHECK(evt.host.c0(0, 2, Helicity::plus).imag() == doctest::Approx(1.5997550805232579));
  DOCTEST_CHECK(evt.host.c1(0, 2, Helicity::plus).real() == doctest::Approx(-1.0319847861739633));
  DOCTEST_CHECK(evt.host.c1(0, 2, Helicity::plus).imag() == doctest::Approx(1.2718884075400851));
  DOCTEST_CHECK(evt.host.c2(0, 2, Helicity::plus).real() == doctest::Approx(-0.072388797950526593));
  DOCTEST_CHECK(evt.host.c2(0, 2, Helicity::plus).imag() == doctest::Approx(0.99050692381116945));
  DOCTEST_CHECK(evt.host.c3(0, 2, Helicity::plus).real() == doctest::Approx(0.19896675524504848));
  DOCTEST_CHECK(evt.host.c3(0, 2, Helicity::plus).imag() == doctest::Approx(0.67853683047219837));
  DOCTEST_CHECK(evt.host.c0(0, 2, Helicity::minus).real() == doctest::Approx(-0.78091005029771887));
  DOCTEST_CHECK(evt.host.c0(0, 2, Helicity::minus).imag() == doctest::Approx(-1.5997550805232579));
  DOCTEST_CHECK(evt.host.c1(0, 2, Helicity::minus).real() == doctest::Approx(-1.0319847861739633));
  DOCTEST_CHECK(evt.host.c1(0, 2, Helicity::minus).imag() == doctest::Approx(-1.2718884075400851));
  DOCTEST_CHECK(evt.host.c2(0, 2, Helicity::minus).real() == doctest::Approx(-0.072388797950526593));
  DOCTEST_CHECK(evt.host.c2(0, 2, Helicity::minus).imag() == doctest::Approx(-0.99050692381116945));
  DOCTEST_CHECK(evt.host.c3(0, 2, Helicity::minus).real() == doctest::Approx(0.19896675524504848));
  DOCTEST_CHECK(evt.host.c3(0, 2, Helicity::minus).imag() == doctest::Approx(-0.67853683047219837));
  // Polarisation vectors for gluon 3
  DOCTEST_CHECK(evt.host.c0(0, 3, Helicity::plus).real() == doctest::Approx(0.21871404835925767));
  DOCTEST_CHECK(evt.host.c0(0, 3, Helicity::plus).imag() == doctest::Approx(0.1762171610893431));
  DOCTEST_CHECK(evt.host.c1(0, 3, Helicity::plus).real() == doctest::Approx(0.12410016900951064));
  DOCTEST_CHECK(evt.host.c1(0, 3, Helicity::plus).imag() == doctest::Approx(-0.3744612292902067));
  DOCTEST_CHECK(evt.host.c2(0, 3, Helicity::plus).real() == doctest::Approx(0.18520820446167652));
  DOCTEST_CHECK(evt.host.c2(0, 3, Helicity::plus).imag() == doctest::Approx(0.62366992842564017));
  DOCTEST_CHECK(evt.host.c3(0, 3, Helicity::plus).real() == doctest::Approx(-0.70578531013445467));
  DOCTEST_CHECK(evt.host.c3(0, 3, Helicity::plus).imag() == doctest::Approx(0.043209906253214421));
  DOCTEST_CHECK(evt.host.c0(0, 3, Helicity::minus).real() == doctest::Approx(0.21871404835925767));
  DOCTEST_CHECK(evt.host.c0(0, 3, Helicity::minus).imag() == doctest::Approx(-0.1762171610893431));
  DOCTEST_CHECK(evt.host.c1(0, 3, Helicity::minus).real() == doctest::Approx(0.12410016900951064));
  DOCTEST_CHECK(evt.host.c1(0, 3, Helicity::minus).imag() == doctest::Approx(0.3744612292902067));
  DOCTEST_CHECK(evt.host.c2(0, 3, Helicity::minus).real() == doctest::Approx(0.18520820446167652));
  DOCTEST_CHECK(evt.host.c2(0, 3, Helicity::minus).imag() == doctest::Approx(-0.62366992842564017));
  DOCTEST_CHECK(evt.host.c3(0, 3, Helicity::minus).real() == doctest::Approx(-0.70578531013445467));
  DOCTEST_CHECK(evt.host.c3(0, 3, Helicity::minus).imag() == doctest::Approx(-0.043209906253214421));

  gen.helicity_configurations().reset(evt);
  gen.helicity_configurations().advance(evt,
                                        gen.process_group().helicity_mask());
  evt.push_helicity_configurations_to_device();
  int n_ptcl {gen.process_group().n_ptcl()};
  for (int p {0}; p < gen.process_group().permutations().n(); ++p) {
    const int finally_contracted_index {
        gen.process_group().permutations().get(p, 0)};
    gen.bg_recursion().prepare(evt, gen.helicity_configurations(),
                               gen.process_group().permutations(), p);
    evt.pull_momenta_from_device();
    // Check momentum conservation after summing internal momenta.
    DOCTEST_CHECK(evt.host.internal_e(0, n_ptcl * (n_ptcl - 1) / 2 - 1) ==
                  doctest::Approx(-evt.host.e(0, finally_contracted_index)));
  }
}

constexpr int proc_column_width {31};
constexpr int column_spacing {2};
constexpr int value_column_width {19};
constexpr int table_width {proc_column_width + 2 * value_column_width +
                           2 * column_spacing - 1};

void print_header(const std::string description,
                  const std::string& reference = "Comix")
{
  std::cerr << '\n';
  std::cerr << description << ":\n";
  std::cerr << std::string(table_width, '=') << '\n';
  std::cerr << std::setw(proc_column_width + column_spacing) << std::left << "Process";
  std::cerr << std::setw(value_column_width + column_spacing) << std::left << "Result";
  std::cerr << std::setw(value_column_width) << std::left << reference;
  std::cerr << '\n';
  std::cerr << std::string(table_width, '-') << '\n';
}

void print_footer()
{
  std::cerr << std::string(table_width, '=') << '\n';
}

void check_points(
    const std::vector<std::vector<Vec4>>& phase_space_points,
    const std::map<std::string, std::vector<double>>& comparison_data,
    const Settings& _s = me_settings(),
    const SM_parameters& _sm = sm)
{
  std::cerr << std::scientific;
  for (const auto& proc_results_pair : comparison_data) {
    Settings s {_s};
    s.process_specs = split(proc_results_pair.first, ',');
    Generator gen {s, _sm};
    for (size_t i {0}; i < phase_space_points.size(); i++) {
      if (proc_results_pair.second.size() <= i)
        break;
      gen.process_group().set_fixed_momenta(phase_space_points[i]);
      const MC_result result = gen.generate_batches(s.n_batches);
      const auto reference = proc_results_pair.second[i];
      const auto old_precision = std::cerr.precision(12);
      std::cerr << std::setw(proc_column_width + column_spacing) << std::left
                << proc_results_pair.first;
      if (proc_results_pair.first.size() + 1 >
          proc_column_width + column_spacing) {
        std::cerr << '\n';
        std::cerr << std::string(proc_column_width + column_spacing, ' ');
      }
      std::cerr << std::setw(value_column_width + column_spacing) << std::left
                << result.mean();
      std::cerr << std::setw(value_column_width) << std::left
                << reference;
      std::cerr << '\n';
      std::cerr.precision(old_precision);
      DOCTEST_CHECK_MESSAGE(result.mean() / reference ==
                                doctest::Approx(1.0).epsilon(1e-6),
                            proc_results_pair.first);
    }
  }
}

DOCTEST_TEST_CASE("Single massless QCD-only events")
{
  // Comparison results have been generated using Comix (and occasionally
  // cross-checked using Amegic) and the PS_POINT mechanism.

  print_header("Massless 2->n QCD processes");

  const std::map<std::string, std::vector<double>> me2_results_comix_n4 {
    {"d u  -> d u", {25.21538433095478, 1.683950232971565}},
    {"d d* -> u u*", {0.6142237962480158, 0.641859476396982}},
    {"d u  -> d u", {25.21538433095478, 1.683950232971565}},
    {"d u* -> d u*", {25.21538433095478, 1.683950232971565}},
    {"d d  -> d d", {11.76675747320042, 15.1823994058852}},
    {"d d* -> d d*", {27.33023553875784, 2.366232540493232}},
    {"d d* -> g g", {2.566638395746051, 3.061797517938494}},
    {"g g  -> d d*", {0.7218670488035805, 0.861130551920203}},
    {"g d  -> g d", {58.76789594200876, 8.446025714235159}},
    {"g g  -> g g", {77.42359737943518, 95.51685246612145}},
  };
  check_points(phase_space_points_n4, me2_results_comix_n4);

  const std::map<std::string, std::vector<double>> me2_results_comix_n5 {
    {"d d* -> u u* g", {1.482336902143919e-06}},
    {"d d* -> g u u*", {3.37236387507458e-06}},
    {"d u  -> d u g", {1.069967706457995e-05}},
    {"d u* -> d u* g", {1.107036280810463e-05}},
    {"d u* -> g d u*", {0.0009124737590002958}},
    {"d d  -> d d g", {9.114764354629857e-06}},
    {"d d* -> d d* g", {1.271175550851087e-05}},
    {"g g  -> g g g", {0.001206071978388147}},
  };
  check_points(phase_space_points_n5, me2_results_comix_n5);

  const std::map<std::string, std::vector<double>> me2_results_comix_n6 {
    {"d db -> u s ub sb", {5.605353133882878e-14, 3.033016090455427e-14}},
    {"d db -> g g g g  ", {1.909258441208058e-09, 5.678141223426621e-12}},
    {"g g  -> g g d db ", {1.145591984665393e-08, 1.387720814337025e-11}},
    {"g g  -> g g g g  ", {1.637133191619040e-07, 3.940001193389541e-10}},
    {"d db -> g g u ub ", {1.714227109183813e-10, 5.162480512195427e-13}},
    {"g g  -> d u db ub", {5.517606512373071e-13, 2.376667556021803e-13}},
    {"d db -> g g d db ", {2.240776441157545e-10, 7.487113204069206e-12}},
    {"d db -> d d db db", {3.005449460557305e-11, 2.988918661465124e-13}},
  };
  check_points(phase_space_points_n6, me2_results_comix_n6);

  const std::map<std::string, std::vector<double>> me2_results_comix_n7 {
    {
      {"d db -> g d d db db ", {8.826991060489503e-16}},
      {"d db -> g u s ub sb ", {1.269698057659277e-18}},
      {"d u  -> g d u s sb  ", {1.669392547313705e-16}},
      {"d db -> g g g d db  ", {3.923400363434566e-16}},
      {"d db -> g d u db ub ", {4.843129116786127e-17}},
      {"d d  -> g d d u ub  ", {8.146777935138137e-17}},
      {"d d  -> g d d d db  ", {3.809873297737136e-17}},
    },
  };
  check_points(phase_space_points_n7, me2_results_comix_n7);

  const std::map<std::string, std::vector<double>> me2_results_comix_n8 {
    {
      {"d db -> u ub s sb c cb", {2.869820227622901e-23}},
      {"d db -> u ub s sb s sb", {7.322896585838071e-24}},
      {"d db -> u ub u ub u ub", {1.001434438501387e-24}},
    },
  };
  check_points(phase_space_points_n8, me2_results_comix_n8);

  print_footer();
}

DOCTEST_TEST_CASE("Single massive QCD-only events")
{
  // Masses are assumed to be m_top = 173.21 and m_bottom = 4.8., while the top
  // width is set to zero (since it appears as an external state). These tests
  // will only check out with these settings in place. Currently, those are
  // default values.

  print_header("QCD 2->n processes incl. massive quarks");

  const std::vector<std::vector<Vec4>> phase_space_points_tt_2 {
    {
      {-7000,0,0,-6997.8566930096531},
      {-7000,0,0,6997.8566930096531},
      {6999.9999999999982,-5946.7679808440444,-1979.1104767826828,-3117.5426705495238},
      {6999.9999999999982,5946.7679808440444,1979.1104767826828,3117.5426705495238},
    },
  };
  const std::map<std::string, std::vector<double>> me2_results_comix_tt_2 {
    {
      {"tb t -> g g", {2.140303363362909}},
      {"tb t -> db d", {0.5857768198514959}},
    },
  };
  check_points(phase_space_points_tt_2, me2_results_comix_tt_2);

  const std::vector<std::vector<Vec4>> phase_space_points_tt_bb {
    {
      {-7000,0,0,-6997.8566930096531},
      {-7000,0,0,6997.8566930096531},
      {6999.9999999999982,-5946.7665827465917,-1979.110011489716,-3117.5419376087934},
      {6999.9999999999927,5946.7665827465935,1979.1100114897235,3117.5419376087984},
    },
  };
  const std::map<std::string, std::vector<double>> me2_results_comix_tt_bb {
    {
      {"tb t -> bb b", {0.5857770040597408}},
    },
  };
  check_points(phase_space_points_tt_bb, me2_results_comix_tt_bb);

  const std::vector<std::vector<Vec4>> phase_space_points_tt_tt {
    {
      {-7000,0,0,-6997.8566930096531},
      {-7000,0,0,6997.8566930096531},
      {6999.9999999999982,-5944.9471595035711,-1978.5044994513175,-3116.5881204068819},
      {6999.9999999999918,5944.9471595035729,1978.504499451325,3116.5881204068869},
    },
  };
  const std::map<std::string, std::vector<double>> me2_results_comix_tt_tt {
    {
      {"tb t -> tb t", {2.671291864979422}},
    },
  };
  check_points(phase_space_points_tt_tt, me2_results_comix_tt_tt);

  const std::vector<std::vector<Vec4>> phase_space_points_gg_ttg {
    {
      {-7000,0,0,-7000},
      {-7000,0,0,7000},
      {6300.09060088,-1192.20057852,-3879.21969554,4815.75043057},
      {3710.52222152,2447.42088803,2458.31931035,-1305.66864766},
      {3989.38717759,-1255.22030951,1420.90038519,-3510.0817829},
    },
  };
  const std::map<std::string, std::vector<double>> me2_results_comix_gg_ttg {
    {
      {"g g -> t tb g", {1.204975574833071e-05}},
    },
  };
  check_points(phase_space_points_gg_ttg, me2_results_comix_gg_ttg);

  const std::vector<std::vector<Vec4>> phase_space_points_gg_ttgg {
    {
      {-7000,0,0,-7000},
      {-7000,0,0,7000},
      {1435.30896517,-624.479281086,709.260451544,1066.34204679},
      {5914.12268785,3794.83815154,2466.07673684,3803.22421039},
      {6065.41037545,-2757.84546319,-2995.89251788,-4495.34419651},
      {585.157971524,-412.513407263,-179.444670501,-374.22206067},
    },
  };
  const std::map<std::string, std::vector<double>> me2_results_comix_gg_ttgg {
    {
      {"g g -> t tb g g", {9.149430257526555e-09}},
    },
  };
  check_points(phase_space_points_gg_ttgg, me2_results_comix_gg_ttgg);

  print_footer();
}

DOCTEST_TEST_CASE("Single massless single lepton-pair events (photon only)")
{
  print_header("2->n processes incl. a single lepton pair attached via photon");

  SM_parameters _sm {sm};
  _sm.ew.z_enabled = false;
  _sm.ew.set_alpha(1.0 / 128.802);
  const std::map<std::string, std::vector<double>> me2_results_comix_n4 {
      {"d db -> e- e+", {0.00044316572029024, 0.0004631050098032084}},
      {"e e+ -> d db", {0.00398849148261224, 0.004167945088228875}},
  };
  check_points(phase_space_points_n4, me2_results_comix_n4, me_settings(), _sm);

  const std::map<std::string, std::vector<double>> me2_results_comix_n5 {
      {"d db -> e- e+ g", {1.385633557945832e-09, 1.758974070410129e-09}},
  };
  check_points(phase_space_points_n5, me2_results_comix_n5, me_settings(), _sm);

  const std::map<std::string, std::vector<double>> me2_results_comix_n6 {
      {"d db -> e- e+ g g", {3.160124867059688e-13, 1.343997796598399e-15}},
      {"d db -> e- e+ u ub", {4.655824180348359e-15, 7.13058981175279e-17}},
      {"d db -> e- e+ d db", {5.97226572378023e-15, 1.2303830742716e-16}},
  };
  check_points(phase_space_points_n6, me2_results_comix_n6, me_settings(), _sm);

  const std::map<std::string, std::vector<double>> me2_results_comix_n7 {
    {
      {"d db -> e- e+ g u ub", {2.28043659946208e-21}},
      {"d db -> e- e+ g d db", {3.765975286132314e-20}},
      {"d db -> e- e+ g g g", {3.994469992907988e-19}},
    },
  };
  check_points(phase_space_points_n7, me2_results_comix_n7, me_settings(), _sm);

  const std::map<std::string, std::vector<double>> me2_results_comix_n8 {
    {
      {"d db -> e- e+ u s ub sb", {7.102099881648358e-28}},
      {"d db -> e- e+ u u ub ub", {7.538420089127528e-28}},
      {"d db -> e- e+ d d db db", {1.271164730083819e-26}},
      {"d db -> e- e+ g g d db" , {1.936633551260159e-22}},
      {"d db -> e- e+ g g g g"  , {6.231202380064498e-24}}
    },
  };
  check_points(phase_space_points_n8, me2_results_comix_n8, me_settings(), _sm);

  print_footer();
}

DOCTEST_TEST_CASE("Single massless single lepton-pair events")
{
  print_header("2->n processes incl. a single lepton pair attached via photon/Z");

  const std::vector<std::vector<Vec4>> _phase_space_points_n4 {
      phase_space_points_n4[1]};
  const std::map<std::string, std::vector<double>> me2_results_comix_n4 {
      {"d db -> e- e+", {0.0003553793251265258}},
  };
  check_points(_phase_space_points_n4, me2_results_comix_n4);

  const std::map<std::string, std::vector<double>> me2_results_comix_n5 {
      {"d db -> e- e+ g", {1.2422066506657e-09, 7.552658036421009e-09}},
  };
  check_points(phase_space_points_n5, me2_results_comix_n5);

  const std::map<std::string, std::vector<double>> me2_results_comix_n6 {
      {"d db -> e- e+ g g", {2.811998397334688e-13}},
      {"d db -> e- e+ u ub", {4.644638099947462e-15}},
      {"d db -> e- e+ d db", {5.776378078932654e-15}},
  };
  check_points(phase_space_points_n6, me2_results_comix_n6);

  const std::map<std::string, std::vector<double>> me2_results_comix_n7 {
    {
      {"d db -> e- e+ g u ub", {8.438424827341317e-21}},
      {"d db -> e- e+ g d db", {4.170426167969088e-20}},
      {"d db -> e- e+ g g g",  {6.940369407927204e-19}},
    },
  };
  check_points(phase_space_points_n7, me2_results_comix_n7);

  const std::map<std::string, std::vector<double>> me2_results_comix_n8 {
    {
      {"d db -> e- e+ u s ub sb", {4.399791863945391e-27}},
      {"d db -> e- e+ u u ub ub", {2.319921617500323e-27}},
      {"d db -> e- e+ d d db db", {2.120049052611023e-26}},
      {"d db -> e- e+ g g d db" , {4.638793588146787e-22}},
      {"d db -> e- e+ g g g g"  , {1.66750455342723e-23}}
    },
  };
  check_points(phase_space_points_n8, me2_results_comix_n8);

  print_footer();
}

#if LHAPDF_FOUND
DOCTEST_TEST_CASE("Single massless QCD-only events (H_T scale)")
{
  print_header("Massless 2->n QCD processes (H_T scale)");

  const std::map<std::string, std::vector<double>> me2_results_comix_n4 {
      {"d db -> u ub", {0.669624114668463832}},
  };
  Settings s {me_settings()};
  s.scale2_spec = "H_Tp^2";
  s.fixed_alpha_s = -1.0;
  check_points(phase_space_points_n4_ht_scale, me2_results_comix_n4, s);

  print_footer();
}
#endif
