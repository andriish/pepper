#define DOCTEST_CONFIG_NO_SHORT_MACRO_NAMES
#include "doctest.h"

#include "src/settings.h"
#include "src/sm.h"

// this global Settings instance can be used by all tests after including
// tests.h; this e.g. allows to set the verbosity from the command
// line and ensure that this is applied throughout all tests
extern Settings settings;
extern SM_parameters sm;
