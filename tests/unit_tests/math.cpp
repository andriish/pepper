#include "tests.h"

#include "src/math.h"

DOCTEST_TEST_CASE("Factorials")
{
  DOCTEST_CHECK(factorial(1) == 1);
  DOCTEST_CHECK(factorial(2) == 2);
  DOCTEST_CHECK(factorial(3) == 6);
  DOCTEST_CHECK(factorial(10) == 3628800);
}
