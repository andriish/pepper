#define DOCTEST_CONFIG_IMPLEMENT
#define DOCTEST_CONFIG_NO_UNPREFIXED_OPTIONS
#include "tests.h"
#include "src/kokkos.h"
#include "src/mpi.h"

// Cf.
// https://github.com/doctest/doctest/blob/master/doc/markdown/commandline.md
class dt_removed {
  std::vector<char*> vec;

public:
  dt_removed(char** argv_in)
  {
    for (; *argv_in; ++argv_in)
      if (strncmp(*argv_in, "--dt-", strlen("--dt-")) != 0)
        vec.push_back(*argv_in);
    vec.push_back(NULL);
  }
  int argc() { return static_cast<int>(vec.size()) - 1; }
  char** argv() { return &vec[0]; }
};

int main(int argc, char** argv)
{
  Mpi::initialise_and_register_finalise();
  Pepper_kokkos::initialise_and_register_finalise(argc, argv);
  doctest::Context context(argc, argv);
  dt_removed args(argv);
  settings.msg_verbosity = Msg::Verbosity::warn;
  settings.process_specs = {"d u -> d u"};
  settings.read(args.argc(), args.argv());
  Msg::configure(settings);
  // cuda_set_device(settings.device_id);
  const int result {context.run()};
  return result;
}
