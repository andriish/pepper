# define and configure system-test driver
find_package(Python3)
set(SHELL /bin/bash)
set(DRIVER ${PROJECT_BINARY_DIR}/tests/system_tests/driver.sh)
configure_file (
  "${PROJECT_SOURCE_DIR}/tests/system_tests/driver.sh.in"
  "${DRIVER}"
  @ONLY
)

# define tests, including reference cross section results
add_test(
  du_du_partonic
  /bin/bash ${DRIVER}
  du_du_partonic 1700.57 1.17144)
list(APPEND PEPPER_SYSTEM_TESTS du_du_partonic)
if (LHAPDF_FOUND)
  if (CUDA_FOUND)
    set(PP_JJ pp_jj_cu)
  else()
    set(PP_JJ pp_jj)
  endif()
  add_test(
    pp_jj
    /bin/bash ${DRIVER}
    ${PP_JJ} 1.17672e+08 75031.4)
  list(APPEND PEPPER_SYSTEM_TESTS pp_jj)
  if (Chili_FOUND)
    add_test(
      ddbar_ee
      /bin/bash ${DRIVER}
      ddbar_ee 605.143 0.205978)
    add_test(
      ddbar_eeg
      /bin/bash ${DRIVER}
      ddbar_eeg 22.6554 0.0166994)
    list(APPEND PEPPER_SYSTEM_TESTS ddbar_ee ddbar_eeg)
  endif()
endif()

set_tests_properties(${PEPPER_SYSTEM_TESTS}
  PROPERTIES LABELS system_test
  ENVIRONMENT PEPPER_DATA_PATH=${CMAKE_BINARY_DIR}/data)

# define custom system-tests target
add_custom_target(system_test
  COMMAND ${CMAKE_CTEST_COMMAND} -L system_test
  DEPENDS pepper
)
