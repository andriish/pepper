# SHERPA set-up notes and results

- Repository: git@gitlab.com:shoeche/sherpa.git
- Branch: me
- Commit: 12bec2e5

Configuration:
```
./configure \
  --prefix=/path/to/install \
  --with-libzip=/path/to/libzip \
  --enable-mpi \
  'CXXFLAGS=-O3'
```

Enabling MPI and using '-O3' are optional choices, but accelerate the integration.

Running:
```
/path/to/Sherpa
# or, parallelising using MPI:
mpiexec -n 8 --use-hwthread-cpus -- /path/to/Sherpa
```
The `--use-hwthread-cpus` might be OpenMPI-specific.
It allows to use hardware threads as independent CPUs,
such that we can multiply `-n` by the number of hyperthreads per physical core.

Results:
```
2_2__d__u__d__u : 1700.57 pb +- ( 1.17144 pb = 0.0688852 % )  exp. eff: 0.150669
```
