from math import sqrt
import sys

# get maximum allowed deviation (this script returns an error for deviations
# larger than that)
allowed_max_deviation = float(sys.argv[1])

# get cross section to test
wsum = float(sys.argv[2])
wsum2 = float(sys.argv[3])
n_trials = float(sys.argv[4])
mean = wsum / n_trials
stddev = sqrt((wsum2 / n_trials - mean * mean) / (n_trials - 1))
xs = [mean, stddev]

# get reference cross section
ref_mean = float(sys.argv[5])
ref_stddev = float(sys.argv[6])
ref_xs = [ref_mean, ref_stddev]


def calculate_deviation(xs1, xs2):
    dev = [xs1[0], xs1[1]]
    dev[0] -= xs2[0]
    dev[0] /= sqrt((xs2[1])**2 + (dev[1])**2)
    dev[1] /= sqrt((xs2[1])**2 + (dev[1])**2)
    return dev


dev = calculate_deviation(xs, ref_xs)

print("Deviation:", dev)

if abs(dev[0]) > allowed_max_deviation:
    sys.exit(1)
