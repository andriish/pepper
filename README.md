![Pepper Logo](docs/manual/logo.png)

# Pepper

Pepper
(**P**ortable **E**ngine for the **P**roduction of **P**arton-level **E**vent **R**ecords)
is an efficent parton-level event generator
to simulate high-energy physics at colliders such as the LHC.
It is scalable from a laptop
to a Leadership Class HPC Facility.
Pepper's mascot is a racing honey bee,
because both Pepper and a bee hive embrace parallelism and efficiency.

As of version 1.0.0, Pepper releases come in two variants:
1. A variant written in C++ and optionally enabled Nvidia CUDA,
   which supports running on CPU and Nvidia GPU hardware.
   Note that this variant might be removed in later releases.
2. A variant written in C++ and using the Kokkos portability
   framework, which has been
   validated by us for running on CPU (single- or multi-threaded),
   Nvidia, Intel and AMD GPU,
   see [arXiv:2311.06198](https://arxiv.org/abs/2311.06198).
   For a full list of hardware supported by Kokkos,
   see [the Kokkos README](https://github.com/kokkos/kokkos).  
   **NOTE:** The installation procedure for the Kokkos variant
   is not yet documented,
   the steps below are written for the non-Kokkos variant.
   Please contact us if you need help.

Both variants support parallel execution using MPI
(Message Passing Interface).

Configuring the project requires `CMake` v3.17.0,
`git` and a compiler supporting C++17.

To configure:
```
cmake -S . -B build
```
To build:
```
cmake --build build
```
To test:
```
ctest --test-dir build
```
To install:
```
cmake --build build --target install -DCMAKE_INSTALL_PREFIX=/path/to/install
```
To run:
```
pepper [<path/to/pepper.ini>]
```

# Optional dependencies

During configuration,
Pepper tries to find the following external packages:

- `CUDA`
- `HepMC3`
- `HDF5`
- `MPI`
- `Chili`
- `LHAPDF`

Use the following option to manually specify
the location of a package,
e.g. if it is not in a standard location:
```
cmake -D<PACKAGE>_ROOT=/path/to/package/prefix
```
Packages that install `pkg-config` package definition files
will additionally be found when the `PKG_CONFIG_PATH` environment variable
includes the directory containing the `<package_name>.pc` file.

You can prevent Pepper from trying to find these packages:
```
cmake -D<PACKAGE>_DISABLED=TRUE
```

For both `<PACKAGE>_ROOT` and `<PACKAGE>_DISABLED` options,
`<PACKAGE>` is one of the above listed packages,
and must be given case-sensitively,
i.e. `HepMC3_ROOT` is correct,
while `HEPMC3_ROOT` is incorrect and will have no effect.

# Detailed compilation instructions

Detailed compilation instructions can be found in the manual:
* [Getting started (main Kokkos version)](docs/manual/tutorials/getting_started-kokkos.md)
  [HTML version](https://spice-mc.gitlab.io/pepper/tutorials/getting_started-kokkos.html)
* [Getting started (native version)](docs/manual/tutorials/getting_started-native.md)
  [HTML version](https://spice-mc.gitlab.io/pepper/tutorials/getting_started-native.html)

# Copyright

Copyright (C) 2023 The Pepper Collaboration

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see [GNU licenses](https://www.gnu.org/licenses/).

# Third party components

The following components included in Pepper (or downloaded during configuration
via CMake) are licensed under free or open source licenses. We wish to thank
the contributors to those projects.

- [HighFive - HDF5 header-only C++ Library](https://bluebrain.github.io/HighFive/)
- [argparse](https://github.com/p-ranav/argparse)
- [mINI](https://github.com/pulzed/mINI)
- [VCL Vector Class Library](https://github.com/vectorclass/version2)
- [zstr - A C++ ZLib wrapper](https://github.com/mateidavid/zstr)
- [Bitmask](https://github.com/oliora/bitmask)
- [Boost Histogram](https://www.boost.org/doc/libs/1_83_0/libs/histogram/doc/html/index.html)
- [tree.hh - STL-like templated tree class](https://github.com/kpeeters/tree.hh/)
- [doctest.h - the lightest feature-rich C++ single-header testing framework for unit tests and TDD](https://github.com/doctest)
