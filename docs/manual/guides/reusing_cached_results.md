# Re-using cached optimisation/integration results

For each process,
Pepper stores optimisation/integration results
under `pepper_cache/{process}`.
This are usually written out the first time Pepper is run for that process.
In subsequent runs, the results are read in,
instead of calculating them again,
which can take a significant amount of time.

```{note}
To disable writing cached results,
you can use the setting `write_cached_results = false`.
To ignore existing cached results,
use `use_cached_results = false`.
Both settings should be in the `[main]` section of your `pepper.ini` file.
```

When changing settings other than the actual process
(e.g. the PDF being used),
the stored results are still valid
in the sense that the event generation gives correct results.
However, the efficiency of the event generation might significantly
deteriorate.
This is in particular true for some more drastic changes,
like switching from helicity sampling to helicity summing.
In these cases, it might be preferable
to remove the old results
under `pepper_cache/{process}`
and re-generate them in the next Pepper run.
