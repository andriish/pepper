# PDF options

The `[pdf]` sections is used to select the parton distribution function and corresponding alphas libraries. Note, that all PDF's are supplied by `LHAPDF`, which has to be activeted during installation.
Let's look at a simple example:
```ini
[pdf]
set    = NNPDF30_nlo_as_0118
member = 0
```

Available options are:
- `set`: The pdf set selected. PDF's are suplied via LHAPDF, and LHAPDF has to be able to load the set. For a selection of officially avaiable sets, cf. [the LHAPDF website](https://lhapdf.hepforge.org/pdfsets.html).
  - Default: `None`
  - Options: `None`, `LHAPDF_string`
- `member`: The specific pdf member.
  - Default: `0`

```{note}
Note that the `Chili` phase-space generator can not be used without initial state integration. Disabling the pdf does not automatically sets the initial state energy fixed.
```
