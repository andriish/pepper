# Runcard options

A Pepper run can be steered via setup files which are also called runcards.
These are text files and should conventionally use the `.ini` extension.
If a file named `pepper.ini` is the current working directory,
Pepper will automatically use it as its runcard.
If it is named differently and/or is in another directory,
its path must be provided on the command line:

```bash
pepper path/to/runcard.ini
```

The runcard contents is split into sections that begin with a `[section_name]` line,
where `section_name` is the name of the section.
Actual options are set using `key = value` lines,
where `key` is the name of the option
and `value` specifies the desired setting,
which can be a number or a keyword.
Lines that begin with a semicolon, e.g. `; some text`,
are ignored and can thus be used to add comments.

The same section header can appear several times in the runcard.
However, options must be defined only once
for a given section header.

Sometimes, subsections are used: `[section_name.subsection_name]`.
This is just a naming convention, they work exactly like normal sections.
In particular, subsections do not have to appear immediately below
their parent sections, but can appear anywhere in the runcard,
just like any other section.

The sections and associated options are discussed
in the following pages.
