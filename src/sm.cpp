// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "src/sm.h"

#include <iostream>
#include <sstream>


bool sm_properties::instanceFlag = false;
sm_properties* sm_properties::single = NULL;

Ptcl_num antiparticle(Ptcl_num p) noexcept
{
  if (is_boson(p))
    return p;
  return -p;
}

int pdf_id(Ptcl_num p) noexcept
{
  if (is_quark(p))
    return p + 6;
  if (p == GLUON)
    return 6;
  return -1;
}

std::vector<Ptcl_num> possible_mothers(Ptcl_num j, Ptcl_num k) noexcept
{
  if ((j == GLUON && k == Z) || (j == Z && k == GLUON))
    return {};
  if (j == GLUON && k == GLUON)
    return {GLUON};
  if ((j == ELECTRON && k == -ELECTRON) || (j == -ELECTRON && k == ELECTRON))
    return {PHOTON, Z};
  if (j == -k)
    return {GLUON};
  if (j != k) {
    if (j == GLUON || j == Z) {
      return {k};
    }
    if (k == GLUON || k == Z) {
      return {j};
    }
  }
  return {};
}

bool is_antiparticle(Ptcl_num p) noexcept
{
  if (is_boson(p))
    return true;
  return (p < 0);
}

bool is_colour_charged(Ptcl_num p) noexcept
{
  if (p == GLUON)
    return true;
  if (is_quark(p))
    return true;
  return false;
}

double n_colour_states(Ptcl_num p) noexcept
{
  if (p == GLUON)
    return QCD_NC * QCD_NC - 1.0;
  if (is_quark(p))
    return QCD_NC;
  return 1.0;
}

double spin(Ptcl_num p) noexcept
{
  if (is_fermion(p))
    return 0.5;
  return 1.0;
}

Ptcl_num particle_from_string(const std::string& s)
{
  size_t scan_position {0};
  Ptcl_num p {NO_PTCL};

  // Scan first letter(s) to determine the particle type/class.
  switch (s[0]) {
  case 'g':
    return GLUON;
    break;
  case 'd':
    p = DOWN_QUARK;
    scan_position = 1;
    break;
  case 'u':
    p = UP_QUARK;
    scan_position = 1;
    break;
  case 's':
    p = STRANGE_QUARK;
    scan_position = 1;
    break;
  case 'c':
    p = CHARM_QUARK;
    scan_position = 1;
    break;
  case 'b':
    p = BOTTOM_QUARK;
    scan_position = 1;
    break;
  case 't':
    if (s.size() >= 3 && s[1] == 'a' && s[2] == 'u') {
      p = TAUON;
      scan_position = 3;
    }
    else {
      p = TOP_QUARK;
      scan_position = 1;
    }
    break;
  case 'e':
    p = ELECTRON;
    scan_position = 1;
    break;
  case 'n':
    p = ELECTRON_NEUTRINO;
    scan_position = 2;
    break;
  case 'm':
    p = MUON;
    scan_position = 2;
    break;
  default:
    std::cout << "Error parsing first particle letter '" << s[0] << "'\n";
    abort();
  }

  // For neutrinos, we need another letter, to narrow down the exact flavour.
  if (s.size() > scan_position) {
    if (p == ELECTRON_NEUTRINO) {
      switch (s[scan_position]) {
      case 'e':
        p = ELECTRON_NEUTRINO;
        break;
      case 'm':
        p = MUON_NEUTRINO;
        break;
      case 't':
        p = TAU_NEUTRINO;
        break;
      default:
        std::cout << "Error parsing neutrino modifier '" << s[scan_position]
                  << "'\n";
        abort();
      }
      scan_position++;
    }
  }

  // Finally, scan another letter (if present) to determine if it's an
  // anti-particle.
  if (s.size() > scan_position) {
    switch (s[scan_position]) {
    case '*':
    case 'b':
    case '+':
      p = antiparticle(p);
      break;
    case '-':
      break;
    default:
      std::cout << "Error parsing modifier '" << s[scan_position] << "'\n";
      abort();
    }
  }
  return p;
}

std::string string_from_particle(Ptcl_num i)
{
  std::string s;
  switch (std::abs(i)) {
  case GLUON:
    s = "g";
    break;
  case DOWN_QUARK:
    s = "d";
    break;
  case UP_QUARK:
    s = "u";
    break;
  case STRANGE_QUARK:
    s = "s";
    break;
  case CHARM_QUARK:
    s = "c";
    break;
  case BOTTOM_QUARK:
    s = "b";
    break;
  case TOP_QUARK:
    s = "t";
    break;
  case ELECTRON:
    s = "e";
    break;
  case ELECTRON_NEUTRINO:
    s = "nu_e";
    break;
  case MUON_NEUTRINO:
    s = "nu_mu";
    break;
  case TAU_NEUTRINO:
    s = "nu_tau";
    break;
  case MUON:
    s = "mu";
    break;
  case TAUON:
    s = "tau";
    break;
  case Z:
    return "Z";
  default:
    std::cout << "Error parsing particle integer '" << i << "'\n";
    abort();
  }
  if (i < 0) {
    const double _charge = sm_properties::getInstance().hcharge(i);
    if (_charge == 1.0)
      s += "+";
    else if (_charge == -1.0)
      s += "-";
    else
      s += "b";
  }
  return s;
}

std::string particles_to_process_spec_n(const std::vector<Ptcl_num>& particles,
                                        int n)
{
  std::stringstream s;
  for (int i {0}; i < n; ++i) {
    s << string_from_particle(particles[i]);
    if (i == n - 1)
      break;
    if (i == 1)
      s << " -> ";
    else
      s << ' ';
  }
  return s.str();
}
