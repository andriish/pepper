// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_COLOUR_FACTORS_H
#define PEPPER_COLOUR_FACTORS_H

#include "Kokkos_Core.hpp"
#include <memory>
#include <string>
#include <vector>

class Flavour_process;
struct Settings;

class Colour_factors {

public:
  Colour_factors(const Flavour_process&, int n_perm, const Settings&);
  const Kokkos::View<double*>& data() const;
  const Kokkos::View<int*>& leading_factors_mask_data() const;

private:
  std::vector<double> factors;
  Kokkos::View<double*> d_factors;
  std::vector<int> leading_factors_mask;
  Kokkos::View<int*> d_leading_factors_mask;
};

#endif
