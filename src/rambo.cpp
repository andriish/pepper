// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "rambo.h"
#include "citations.h"
#include "event_handle.h"
#include "kernel_macros.h"
#include "mass_dresser_kernel.h"
#include "massless_rambo_kernel.h"
#include "flavour_process.h"

#include <array>

Rambo::Rambo(const Flavour_process& proc, double _e_cms)
    : Phase_space_generator {_e_cms},
      massless_rambo {proc, _e_cms},
      mass_dresser {proc, _e_cms}
{
  Citations::register_citation("Rambo", "Kleiss:1985gy");
  is_massive_case = false;
  for (int p {2}; p < proc.n_ptcl(); ++p) {
    const double m {sm_properties::getInstance().hmass(proc[p])};
    if (m != 0.0) {
      is_massive_case = true;
      break;
    }
  }
}

void Rambo::fill_momenta_and_weights(Event_handle& evt) const
{
  if (!fixed_momenta.empty()) {
    fill_fixed_momenta(evt);
  } else {
    massless_rambo.fill_momenta_and_weights(evt);
    if (is_massive_case)
      mass_dresser.transform_momenta(evt);
  }
  Kokkos::fence("Rambo::fill_momenta_and_weights");
}

void Rambo::update_weights(Event_handle& evt) const
{
  massless_rambo.update_weights(evt);
  if (is_massive_case)
    mass_dresser.update_weights(evt);
}

void Rambo::fill_fixed_momenta(Event_handle& evt) const
{
  const size_t n {fixed_momenta.size()};
  for (size_t p {0}; p < n; ++p) {
    for (int i {0}; i < evt.host.batch_size; ++i) {
      evt.host.e(i, p) = fixed_momenta[p][0];
      evt.host.px(i, p) = fixed_momenta[p][1];
      evt.host.py(i, p) = fixed_momenta[p][2];
      evt.host.pz(i, p) = fixed_momenta[p][3];
      evt.host.w(i) = 1.;
    }
  }
  evt.push_momenta_to_device();
  evt.push_weights_to_device();
}

Massless_rambo::Massless_rambo(const Flavour_process& proc, double _e_cms)
    : Phase_space_generator {_e_cms},
      _n_ptcl {proc.n_ptcl()}
{
}

void Massless_rambo::fill_momenta_and_weights(Event_handle& evt) const
{
  const auto& _e_cms = e_cms;
  RUN_KERNEL(massless_rambo_fill_momenta_and_weights_kernel, evt, _e_cms);
}

void Massless_rambo::update_weights(Event_handle& evt) const
{
  const auto& _e_cms = e_cms;
  RUN_KERNEL(massless_rambo_update_weights_kernel, evt, _e_cms);
}

Mass_dresser::Mass_dresser(const Flavour_process& proc, double e_cms):
    d_data("massdresser_data"),
    h_data(Kokkos::create_mirror_view(d_data))
{
  double m_total {0.0};
  for (int p {2}; p < proc.n_ptcl(); p++) {
    m_total += sm_properties::getInstance().hmass(proc[p]);
  }
  h_data().e_cms = e_cms;
  h_data().chi_start = std::sqrt(1.0 - (m_total / e_cms) * (m_total / e_cms));
  h_data().accuracy = 1e-14 * e_cms;
  Kokkos::deep_copy(d_data,h_data);
}

void Mass_dresser::transform_momenta(Event_handle& evt) const
{
  const auto& dd = d_data;
  const auto& smprop = sm_properties::getInstance();
  RUN_KERNEL(mass_dresser_transform_momenta_kernel, evt, dd, smprop);
}

void Mass_dresser::update_weights(Event_handle& evt) const
{
  const auto& dd = d_data;
  RUN_KERNEL(mass_dresser_update_weights_kernel, evt, dd);
}
