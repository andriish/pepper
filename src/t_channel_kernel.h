// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_T_CHANNEL_KERNEL_H
#define PEPPER_T_CHANNEL_KERNEL_H

#include "Kokkos_Core.hpp"
#include "event_data.h"
#include "kernel_macros.h"
#include "math.h"
#include "vec4.h"
#include "sm.h"
#include <random>

KOKKOS_INLINE_FUNCTION
double save_pow(double base, double power) {
  // this is certainly not ideal
  if(base<1e-7)
    return 0.;
  else
    return Kokkos::pow(base,power);
}

KOKKOS_INLINE_FUNCTION
Vec4 LT(const Vec4& a, const Vec4& b, const Vec4& c)
{
  double t(a[1] * b[2] * c[3] + a[2] * b[3] * c[1] + a[3] * b[1] * c[2] -
           a[1] * b[3] * c[2] - a[3] * b[2] * c[1] - a[2] * b[1] * c[3]);
  double x(-a[0] * b[2] * c[3] - a[2] * b[3] * c[0] - a[3] * b[0] * c[2] +
           a[0] * b[3] * c[2] + a[3] * b[2] * c[0] + a[2] * b[0] * c[3]);
  double y(-a[1] * b[0] * c[3] - a[0] * b[3] * c[1] - a[3] * b[1] * c[0] +
           a[1] * b[3] * c[0] + a[3] * b[0] * c[1] + a[0] * b[1] * c[3]);
  double z(-a[1] * b[2] * c[0] - a[2] * b[0] * c[1] - a[0] * b[1] * c[2] +
           a[1] * b[0] * c[2] + a[0] * b[2] * c[1] + a[2] * b[1] * c[0]);
  return Vec4 {t, -x, -y, -z};
}

KOKKOS_INLINE_FUNCTION
double MassivePropWeight(double m, double g, double smin, double smax, double s)
{
  double m2(m * m), mw(m * g);
  double ymax(Kokkos::atan((smin - m2) / mw)), ymin(Kokkos::atan((smax - m2) / mw));
  double w(mw / ((s - m2) * (s - m2) + mw * mw));
  w = (ymin - ymax) / w;
  return 1./w;
}

KOKKOS_INLINE_FUNCTION
double MassivePropMomenta(double m, double g, double smin, double smax,
                          double ran)
{
  double m2(m * m), mw(m * g), s;
  double ymax(Kokkos::atan((smin - m2) / mw)), ymin(Kokkos::atan((smax - m2) / mw));
  s = m2 + mw * tan(ymin + ran * (ymax - ymin));
  return s;
}

KOKKOS_INLINE_FUNCTION
double SqLam(double s, double s1, double s2)
{
  double arg((s - s1 - s2)*(s - s1 - s2) - 4. * s1 * s2);
  if (arg > 0.)
    return Kokkos::sqrt(arg) / s;
  return 0.;
}

KOKKOS_INLINE_FUNCTION
void Boost(int lflag, const Vec4& q, const Vec4& ph, Vec4& p)
{
  double rsq = Kokkos::sqrt(q.abs2());
  if (lflag==0) {
    const double p0 {(q[0]*ph[0]+q.vec3()*ph.vec3())/rsq};
    const double c1 = (ph[0]+p0)/(rsq+q[0]);
    const auto tmp {ph.vec3()+c1*q.vec3()};
    p = Vec4 {p0,tmp[0], tmp[1], tmp[2] };
  }
  else {
    const double p0 {q*ph/rsq};
    double c1 = (p0+ph[0])/(rsq+q[0]);
    const auto tmp {ph.vec3()-c1*q.vec3()};
    p = Vec4{p0,tmp[0], tmp[1], tmp[2]};
  }
}


KOKKOS_INLINE_FUNCTION
double PeakedDist(double a,double cn,double cxm,double cxp,int k,double ran)
{
  double ce(1.-cn);
  double ret;
  if (ce!=0.)
    ret = k*(save_pow(ran*save_pow(a+k*cxp,ce)+(1.-ran)*save_pow(a+k*cxm,ce),1/ce)-a);
  else
    ret = k*((a+k*cxm)*save_pow((a+k*cxp)/(a+k*cxm),ran)-a);
  return ret;
}

KOKKOS_INLINE_FUNCTION
void SChannelMomenta(Vec4 p, double s1, double s2, Vec4& p1, Vec4& p2,
		     double ran1, double ran2, double ctmin, double ctmax)
{
  double s    = p.abs2();
  double rs   = Kokkos::sqrt(Kokkos::abs(s));
  double p1h0 = (s+s1-s2)/rs/2.;
  double p1m  = rs*SqLam(s,s1,s2)/2.;
  double ct   = ctmin+(ctmax-ctmin)*ran1;
  double st   = Kokkos::sqrt(1.-ct*ct);
  double phi  = 2.*M_PI*ran2;
  const auto tmp {p1m*Vec3{st*Kokkos::sin(phi),st*Kokkos::cos(phi),ct} };
  Vec4 p1h {p1h0,tmp[0], tmp[1], tmp[2]};
  Boost(0,p,p1h,p1);
  p2  = p+(-1.)*p1;
}

KOKKOS_INLINE_FUNCTION
double SChannelWeight(const Vec4& p1, const Vec4& p2,
                      double ctmin, double ctmax)
{
  double massfactor = SqLam((p1+p2).abs2(),p1.abs2(),p2.abs2());
  if (massfactor < 1e-9) return 0.;
  return 2./M_PI/massfactor*2.0/(ctmax-ctmin);
}

KOKKOS_INLINE_FUNCTION
double PeakedWeight(double a,double cn,double cxm,double cxp,int k)
{
  double ce(1.-cn), w;
  if (ce!=0.) {
    double amin=save_pow(a+k*cxm,ce);
    w=save_pow(a+k*cxp,ce)-amin;
    w/=k*ce;
  }
  else {
    double amin=a+k*cxm;
    w=Kokkos::log((a+k*cxp)/amin);

    w/=k;
  }
  return w;
}


template<typename event_data,typename etmin_type,typename ms_type,typename cuts_data>
KOKKOS_INLINE_FUNCTION
void t_channel_fill_momenta_and_weights_kernel(const event_data& evt,
                                              const int& ii,
                                              const double& e_cms,
                                              const etmin_type& etmin,
                                              const ms_type& ms,
                                              const cuts_data& cuts,
                                              const int& m_type,
                                              const sm_properties& smprops)
{

  const int nin = 2;
  const int nout = evt.n_ptcl - nin;

  int irn {0};

  double S = e_cms*e_cms;
  double rtS(Kokkos::sqrt(S)), pt2max(S/4), nnoj {1};
  double weight {2*M_PI/S};
  Vec4 psum {0.,0.,0.,0.};
  for (size_t j{0}; j<2; ++j) {
    Vec4 psum2 {0., psum[1] / nnoj, psum[2]/nnoj, 0};
    const double m_alpha = 2.0;
    for(int i(nin+m_type);i<nin+nout;++i) {
      double pt2min(etmin[i]*etmin[i]-ms[i]), pt2;
      if (pt2min > 1e-9) {
        if(j==1) continue;
        pt2=PeakedDist
          (0.,m_alpha,pt2min,pt2max,1,evt.r(ii,irn++));
        weight*=save_pow(pt2,m_alpha)*
          PeakedWeight(0.,m_alpha,pt2min,pt2max,1);
            } else {
        if(j==0) {
          ++nnoj;
          continue;
        }
        double xr(evt.r(ii,irn++));
        double ptmin(Kokkos::sqrt(ms[i])), ptmax(Kokkos::sqrt(pt2max));
        double pt(2.*ptmin*ptmax*xr/(2.*ptmin+ptmax*(1.-xr)));
        pt2=pt*pt;
        weight*=2.*pt*ptmax/2./ptmin/(2.*ptmin+ptmax)*(2.*ptmin+pt)*(2.*ptmin+pt);
      }
      weight/=32.*Kokkos::pow(M_PI,3);
      double yb(Kokkos::log(Kokkos::sqrt(S/4/pt2)+Kokkos::sqrt(S/4/pt2-1.)));
      double ymax(yb), ymin(-yb);

      double y(ymin+(evt.r(ii,irn++))*(ymax-ymin));
      weight*=ymax-ymin;
      double phi(2*M_PI*(evt.r(ii,irn++)));
      weight*=2.*M_PI;

      double sinhy(Kokkos::sinh(y)), coshy(Kokkos::sqrt(1+sinhy*sinhy));
      double _pt(Kokkos::sqrt(pt2)), ref(psum.phi());;
      pt2 = (Vec4 {0.,_pt*Kokkos::cos(phi+ref),_pt*Kokkos::sin(phi+ref),0.} - psum2 ).p_perp2();
      double mt(Kokkos::sqrt(pt2+ms[i]));
      Vec4 p_i {mt*coshy,_pt*Kokkos::cos(phi+ref),_pt*Kokkos::sin(phi+ref),mt*sinhy};
      p_i -= psum2;
      psum += p_i;
      evt.e(ii, i)  = p_i[0];
      evt.px(ii, i) = p_i[1];
      evt.py(ii, i) = p_i[2];
      evt.pz(ii, i) = p_i[3];
    }
  }

  double m2(ms[nin]);
  if (m_type == 2) {
    const double _s {(rtS - psum.abs2()) * (rtS - psum.abs2())};
    const double smin {cuts().ll_m2_min};
    m2 = MassivePropMomenta(smprops.mass(Z), smprops.width(Z), smin, _s, evt.r(ii, irn++));
    weight /= 2. * M_PI * MassivePropWeight(smprops.mass(Z), smprops.width(Z), smin, _s, m2);
  }

  double mj2(psum.abs2()), ptj2(psum.p_perp2());

  double yj(nout>m_type?psum.y():0);

  double Qt(Kokkos::sqrt(m2+ptj2)), mt(Kokkos::sqrt(mj2+ptj2));
  double ymin(-Kokkos::log(rtS/Qt*(1.-mt/rtS*exp(-yj))));
  double ymax(Kokkos::log(rtS/Qt*(1.-mt/rtS*exp(yj))));

  double yv(ymin+(evt.r(ii,irn++)) *(ymax-ymin));
  weight*=ymax-ymin;
  evt.set_r(ii,irn-1,(yv-ymin)/(ymax-ymin));
  double sinhy(Kokkos::sinh(yv)), coshy(Kokkos::sqrt(1 + sinhy * sinhy));

  Vec4 p_nin = {Qt * coshy, -psum[1], -psum[2], Qt * sinhy};
  evt.e (ii, nin)  = p_nin[0];
  evt.px(ii, nin) = p_nin[1];
  evt.py(ii, nin) = p_nin[2];
  evt.pz(ii, nin) = p_nin[3];

  double pp((p_nin + psum).p_plus());
  double pm((p_nin + psum).p_minus());

  evt.e(ii, 0) = -pp / 2;
  evt.px(ii, 0) = 0.;
  evt.py(ii, 0) = 0.;
  evt.pz(ii, 0) = -pp / 2;

  evt.e(ii,  1) = -pm/2;
  evt.px(ii, 1) = 0.;
  evt.py(ii, 1) = 0.;
  evt.pz(ii, 1) = pm/2;

  // Only works for pp with same energy
  bool m_status;
  m_status=pp>0.&&pp<=e_cms;;
  m_status&=pm>0.&&pm<=e_cms;

  //const Vec4 xref {1., 1., 0., 0.};
  if (m_status && m_type == 2) {

    Vec4 pv = evt.p(ii,2);

    // TODO: eventually, the methods should just accept the event data
    Vec4 p_2 = evt.p(ii, 2);
    Vec4 p_3 = evt.p(ii, 3);
    SChannelMomenta(pv, ms[2], ms[3], p_2, p_3, evt.r(ii, irn),
                    evt.r(ii, irn+1), -1, 1);
    irn += 2;

    evt.e(ii, 2) = p_2[0];
    evt.px(ii, 2) = p_2[1];
    evt.py(ii, 2) = p_2[2];
    evt.pz(ii, 2) = p_2[3];

    evt.e(ii, 3) = p_3[0];
    evt.px(ii, 3) = p_3[1];
    evt.py(ii, 3) = p_3[2];
    evt.pz(ii, 3) = p_3[3];

    weight /= (4 * M_PI * M_PI);
    weight /= SChannelWeight(p_2, p_3, -1, 1);
  }
  evt.x1(ii) = Kokkos::abs(-pp / 2 / (e_cms / 2));
  evt.x2(ii) = Kokkos::abs(-pm/2 / (e_cms / 2));

  evt.w(ii) = weight;

}

DEFINE_CUDA_KERNEL_NARGS_6(t_channel_fill_momenta_kernel, double, e_cms,
                           const double *, etmin, const double*, ms,
			                     const detail::Cuts_data&, cuts, int, m_type,
                           const sm_properties&, smprops)

#endif
