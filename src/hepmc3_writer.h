// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_HEPMC3_WRITER_H
#define PEPPER_HEPMC3_WRITER_H

#include "pepper/config.h"

#include "event_writer.h"
#include "sm.h"

#include <array>
#include <cassert>
#include <memory>
#include <string>

#include "HepMC3/GenRunInfo.h"
#include "HepMC3/WriterAscii.h"

class Hepmc3_writer : public Event_writer {

public:
  Hepmc3_writer(const std::string& filepath, const Settings&,
                const SM_parameters&);
  Hepmc3_writer(std::ostream&, const Settings&, const SM_parameters&);

  void initialise(const Process&, const Pdf_and_alpha_s&) override;
  void finalise(const MC_result&) override;

  void write_n(const Event_handle&, int n, const MC_result&, long n_nonzero,
               long n_trials, const Process&) override;

  bool prints_leading_colour_information() const override
  {
    return leading_colour_flow_output_enabled;
  }

private:
  template <class T> void initialise_ascii_writer(T&& out)
  {
    gen_run_info = std::make_shared<HepMC3::GenRunInfo>();
    HepMC3::GenRunInfo::ToolInfo gen_tool_info {PROJECT_NAME, PROJECT_VERSION, ""};
    gen_run_info->tools().push_back(gen_tool_info);
    gen_run_info->set_weight_names({"Weight", "EXTRA__NTrials"});
    ascii_writer = std::make_unique<HepMC3::WriterAscii>(out, gen_run_info);
  }

  std::shared_ptr<HepMC3::GenRunInfo> gen_run_info;
  std::unique_ptr<HepMC3::WriterAscii> ascii_writer;
  std::shared_ptr<HepMC3::GenPdfInfo> gen_pdf_info;

  std::array<Ptcl_num, 2> beams {NO_PTCL, NO_PTCL};
  bool leading_colour_flow_output_enabled;
  std::vector<int> colours;
  std::vector<int> anticolours;
  double e_cms;
  double ew_alpha;
  size_t n_written {0};
  bool initialised {false};

  std::string filepath;
  std::ostream* stream {nullptr};
  std::unique_ptr<std::ostream> zstream;
};

#endif
