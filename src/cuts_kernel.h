// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_CUTS_KERNEL_H
#define PEPPER_CUTS_KERNEL_H

#include "cuts.h"
#include "event_handle.h"
#include "kernel_macros.h"
#include "sm.h"
#include <cmath>

template <typename evt_type, typename cuts_type>
KOKKOS_INLINE_FUNCTION
void cuts_kernel(const evt_type& evt,const int& i,
                        const cuts_type& in_cuts)
{
  const auto& cuts = in_cuts();
  const int n_ptcl {evt.n_ptcl};
  // check if momenta are valid
  for (int p {0}; p < n_ptcl; p++) {
    if (Kokkos::isnan(evt.e(i, p))) {
      evt.w(i) = 0.0;
#if 0
      if (p < 2) {
        // TODO: We must check with the Chili authors, if we should discard such
        // points, or count them as zeros (as we do here).
        WARN_ONCE("Found an incoming particle with an energy = NaN. "
                  "Will set event weight to 0.0 and continue.");
      }
      else {
        // see https://gitlab.com/spice-mc/Chili/-/issues/5
        WARN_ONCE("Found an outgoing particle with an energy = NaN. "
                  "Will set event weight to 0.0 and continue.");
      }
#endif
      return;
    }
  }
  // check if invariant masses of incoming partons is "small" enough
  double m2 = evt.e(i, 0) * evt.e(i, 1) - evt.px(i, 0) * evt.px(i, 1) -
              evt.py(i, 0) * evt.py(i, 1) - evt.pz(i, 0) * evt.pz(i, 1);
  if ((m2 > cuts.e_cms * cuts.e_cms) ||
      (-evt.e(i, 0) > cuts.e_cms / 2. || -evt.e(i, 1) > cuts.e_cms / 2.)) {
    evt.w(i) = 0.0;
    return;
  }

  // check (pseudo-)rapidity, which are the same in the massless case we
  // consider here
  for (int p {2}; p < n_ptcl; p++) {
    // Do not require this for non-jet particles. Note that this means that we
    // never calculate y (and phi below) for non-jet particles, so we need to
    // amend here (and below) as soon as we add additional lepton cuts beyond
    // the invariant mass cut below.
    if (!is_jet(evt.ptcl(p)))
      continue;
    evt.y(i, p - 2) = 0.5 * Kokkos::log((evt.e(i, p) + evt.pz(i, p)) /
                                     (evt.e(i, p) - evt.pz(i, p)));
    if (std::abs(evt.y(i, p - 2)) > cuts.nu_max) {
      evt.w(i) = 0.0;
      return;
    }
  }

  for (int p {2}; p < n_ptcl; p++) {
    // do not require this for non-jet particles
    if (!is_jet(evt.ptcl(p)))
      continue;
    const double pT = Kokkos::sqrt(evt.px(i, p) * evt.px(i, p) +
                                evt.py(i, p) * evt.py(i, p));
    // check transverse momentum
    if (pT < cuts.pT_min) {
      evt.w(i) = 0.0;
      return;
    }
    evt.phi(i, p - 2) = Kokkos::atan2(evt.py(i, p), evt.px(i, p));
  }

  // check dR cuts between massless qcd particles
  for (int p {2}; p < n_ptcl; p++) {
    for (int other_p {p + 1}; other_p < n_ptcl; other_p++) {
      // only check dR between jets
      if (!(is_jet(evt.ptcl(p)) && is_jet(evt.ptcl(other_p))))
        continue;
      const double dy = evt.y(i, p - 2) - evt.y(i, other_p - 2);
      // calculate dphi (wrapping around the circle, i.e. the absolute
      // difference never exceeds Pi; the following logic requires that phi is
      // within the interval [-Pi, Pi], which is guaranteed by the above
      // std::atan2 call used to calculate it)
      double dphi = evt.phi(i, p - 2) - evt.phi(i, other_p - 2);
      if (dphi < -M_PI)
        dphi += 2.0 * M_PI;
      else if (dphi > M_PI)
        dphi -= 2.0 * M_PI;
      const double dR = Kokkos::sqrt(dy * dy + dphi * dphi);
      if (dR < cuts.dR_min) {
        evt.w(i) = 0.0;
        return;
      }
    }
  }

  // if necessary, check invariant mass of ew-gauge boson
  int p1 {-1};
  int p2 {-1};
  for (int p {2}; p < n_ptcl; p++) {
    if (!is_lepton(evt.ptcl(p)))
      continue;
    if (p1 == -1)
      p1 = p;
    else
      p2 = p;
  }

  if (p1 != -1 && p2 != -1) {
    const double ll_m2 {square(evt.e(i, p1) + evt.e(i, p2)) -
                 square(evt.px(i, p1) + evt.px(i, p2)) -
                 square(evt.py(i, p1) + evt.py(i, p2)) -
                 square(evt.pz(i, p1) + evt.pz(i, p2))};
    if (ll_m2 < cuts.ll_m2_min || cuts.ll_m2_max < ll_m2) {
      evt.w(i) = 0.0;
      return;
    }
  }
}
DEFINE_CUDA_KERNEL_NARGS_1(cuts_kernel, const detail::Cuts_data&, cuts)

#endif
