// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_MPI_H
#define PEPPER_MPI_H

#include "pepper/config.h"

#include <array>
#include <cstddef>
#include <vector>

#if MPI_FOUND
#include <mpi.h>
#endif

// The below functions gracefully handle the case where MPI is not available,
// e.g. by doing nothing, or always tell the user that the current rank is
// the main rank, and so on.

namespace Mpi {
void initialise_and_register_finalise();

// there is one "main" rank, all other ranks are called "workers"
constexpr int main_rank {0};
int rank();
int n_ranks();
inline bool is_main_rank() { return rank() == main_rank; };
inline bool is_worker_rank() { return !is_main_rank(); };
inline bool has_workers() { return n_ranks() > 1; }

inline void barrier() {
#if MPI_FOUND
  MPI_Barrier(MPI_COMM_WORLD);
#endif
}

void cast_d(double&);
void cast_i(int&);
void cast_d(std::vector<double>&);
void max_d(double&);
bool equal_i(int);

// calculating sums
void all_sum_d(double&);
void sum_d(double&);
void sum_d(std::vector<double>&);
void all_sum_sizes(size_t&);
template <size_t N> void all_sum_d(std::array<double, N>& a)
{
#if MPI_FOUND
  MPI_Allreduce(MPI_IN_PLACE, a.data(), N, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#else
  (void)a;
#endif
}
template <size_t N> void sum_d(std::array<double, N>& a)
{
#if MPI_FOUND
  if (is_main_rank())
    MPI_Reduce(MPI_IN_PLACE, a.data(), N, MPI_DOUBLE, MPI_SUM, Mpi::main_rank,
               MPI_COMM_WORLD);
  else
    MPI_Reduce(a.data(), a.data(), N, MPI_DOUBLE, MPI_SUM, Mpi::main_rank,
               MPI_COMM_WORLD);
#else
  (void)a;
#endif
}
template <size_t N> void all_sum_i(std::array<long, N>& a)
{
#if MPI_FOUND
  MPI_Allreduce(MPI_IN_PLACE, a.data(), N, MPI_LONG, MPI_SUM, MPI_COMM_WORLD);
#else
  (void)a;
#endif
}
template <size_t N> void sum_i(std::array<long, N>& a)
{
#if MPI_FOUND
  if (is_main_rank())
    MPI_Reduce(MPI_IN_PLACE, a.data(), N, MPI_LONG, MPI_SUM, Mpi::main_rank,
               MPI_COMM_WORLD);
  else
    MPI_Reduce(a.data(), a.data(), N, MPI_LONG, MPI_SUM, Mpi::main_rank,
               MPI_COMM_WORLD);
#else
  (void)a;
#endif
}

// gathering values
void all_gather_i(std::vector<int>&);

} // namespace Mpi

#endif
