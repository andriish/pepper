// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "colour_factor_form_file_generator.h"
#include "flavour_process.h"
#include "melia_dyck_permutator.h"
#include "melia_dyck_words.h"
#include "print.h"

#include "blockgen_legacy/form_string.h"
using namespace Color;

#include <fstream>
#include <iostream>
#include <numeric>

Colour_factor_form_file_generator::Colour_factor_form_file_generator(
    const Flavour_process& proc)
    : colour_facs {create_colour_factors(proc)},
      n_colour_facs {colour_facs.size()},
      process_str {proc.qcd_only_uid()},
      melia_perm_size {proc.n_qcd()}
{
}

Full_colour_factor_form_file_generator::Full_colour_factor_form_file_generator(
    const Flavour_process& proc)
    : Colour_factor_form_file_generator(proc)
{
  // Generate usual full colour matrix
  file.open("colorfactor_" + process_str + ".frm");
  file << Form::form_start << std::endl;
  int norm = 1;
  if (melia_perm_size % 2 == 1 && proc.n_quark_pairs() != 0) {
    norm = -1;
  }
  for (size_t i(0); i < n_colour_facs; ++i) {
    for (size_t j(i); j < n_colour_facs; ++j) {
      file << "l C" << i << "d" << j << " = " << norm << " * "
           << FormString(colour_facs[i], colour_facs[j],
                         proc.n_quark_pairs() == 0)
           << ";" << std::endl;
    }
  }

  file << "$NC=" << QCD_NC << ";\n";
  file << Form::form_end1;
  for (size_t i(0); i < n_colour_facs; ++i) {
    for (size_t j(i); j < n_colour_facs; ++j) {
      file << "#write <"
           << "colorfactor_" << process_str + ".dat> \"%E\", C" << i << "d" << j
           << std::endl;
    }
  }

  file << Form::form_end2;
}

Leading_colour_factor_form_file_generator::
    Leading_colour_factor_form_file_generator(const Flavour_process& proc)
    : Colour_factor_form_file_generator(proc)
{
  // Generate diagonal terms @ leading colour
  file.open("colorfactor_lc_" + process_str + ".frm");
  file << Form::form_start_leading_colour << std::endl;
  int norm = 1;
  if (melia_perm_size % 2 == 1 && proc.n_quark_pairs() != 0) {
    norm = -1;
  }
  for (size_t i(0); i < n_colour_facs; ++i) {
    file << "l C" << i << "d" << i << " = " << norm << " * "
	    << FormString(colour_facs[i], colour_facs[i],
			  proc.n_quark_pairs() == 0)
	    << ";" << std::endl;
  }

  file << "$NC=" << QCD_NC << ";\n";
  file << Form::form_end1;
  for (size_t i(0); i < n_colour_facs; ++i) {
    file << "#write <"
	    << "colorfactor_lc_" << process_str + ".dat> \"%E\", C" << i << "d" << i
	    << std::endl;
  }

  file << Form::form_end2;
}

std::vector<ColorFactor>
Colour_factor_form_file_generator::create_colour_factors(
    const Flavour_process& proc)
{
  Melia_dyck_words dyck_words {proc};

  // number of elements for a given Melia permutation (= number of QCD
  // particles)
  const int melia_perm_size {proc.n_qcd()};

  // generate colour factors
  std::vector<ColorFactor> colour_facs;
  for (const auto& dyck_array : dyck_words()) {
    auto base_to_current_dyck_perm = dyck_words.from_base_perm(dyck_array);
    // permute flavoured dyck word accordingly
    std::vector<int> flavour_dyck_array(melia_perm_size, -1);
    for (int i {0}; i < melia_perm_size; ++i) {
      flavour_dyck_array[base_to_current_dyck_perm[i]] =
          dyck_words.flavoured_base()[i];
    }

    Melia_dyck_permutator dyck_permutator {
        melia_perm_size, proc.n_quark_pairs(), dyck_array, flavour_dyck_array};

    ColorFactor base_color_factor = generate_color_factor(
        dyck_array, proc.n_quark_pairs(), melia_perm_size);
    if (Msg::verbosity >= Msg::Verbosity::debug) {
      std::cerr << "  Base color factor (in convential notation):\n";
      for (const auto& term : base_color_factor.terms) {
        std::cerr << "    ";
        printColorTerm(term);
      }
    }

    DEBUG("  Permuted color factors:");
    do {
      auto quark_pair_and_gluon_perm = dyck_permutator.current();
      const auto permuted_indizes =
          concat_perms(dyck_words.proc_to_base_perm(),
                       base_to_current_dyck_perm, quark_pair_and_gluon_perm);

      auto gluon_colors = get_gluon_colors(proc, permuted_indizes);
      auto fermion_colors =
          get_fermion_colors(proc, permuted_indizes, dyck_array);
      auto permuted_cf =
          cf_from_base(base_color_factor, gluon_colors, fermion_colors);
      colour_facs.push_back(permuted_cf);
    } while (dyck_permutator.advance());
  }

  return colour_facs;
}
