// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "colour_factors.h"
#include "colour_factor_form_file_generator.h"
#include "flavour_process.h"
#include "mpi.h"
#include "paths.h"
#include "print.h"
#include "settings.h"

#include <filesystem>
#include <fstream>
#include <iostream>

namespace fs = std::filesystem;

Colour_factors::Colour_factors(const Flavour_process& flav_proc, int n_perm,
                               const Settings& s):
                               d_factors(Kokkos::ViewAllocateWithoutInitializing("colour_factors"),0),
                               d_leading_factors_mask(Kokkos::ViewAllocateWithoutInitializing("leading_colour_factors"),0)
{
  // construct colour factor file path
  const std::string stem {"colorfactor_" + flav_proc.qcd_only_uid()};

  const fs::path data_path {s.data_path};
  const fs::path file_name {stem + ".dat"};
  const fs::path file_path {data_path / file_name};

  // try to read colour factor file
  std::ifstream ifstream {file_path};
  if (!ifstream.is_open()) {

    // the colour factor file does not exist; perhaps it is okay to fall back on
    // dummy factors ...
    if (!s.dummy_colour_factors_allowed) {

      // ... it is not, so let's generate the colour factor file using FORM
      VERBOSE("");
      STATUS("Generate colour matrix file for " + flav_proc.uid() +
             " using FORM ...");

      if (Mpi::is_main_rank()) {

        // cd to our temporary directory, remember where we were
        const auto cwd {fs::current_path()};
        fs::current_path(Paths::temp);

        // generate the FORM scripts
        Full_colour_factor_form_file_generator gen {flav_proc};

        // run FORM; using "-F" ensures that all output is piped to a log file
        const int stat {std::system(("form -F " + stem + ".frm").c_str())};
        if (WIFEXITED(stat) && WEXITSTATUS(stat) != 0) {
          ERROR("The call to FORM exited with status \"", WEXITSTATUS(stat),
                "\". Abort.");
          abort();
        }

        // cd to the previous directory
        fs::current_path(cwd);

        // move generated colour factor files and FORM log files into respective
        // target directories
        const fs::path temp_dir {Paths::temp};
        const fs::path logs_dir {Paths::logs};
        const fs::path log_file_name {stem + ".log"};
        fs::copy(temp_dir / file_name, file_path);
        fs::copy(temp_dir / log_file_name, logs_dir / log_file_name,
                 fs::copy_options::update_existing);
      }

      // now try to read colour factor file again
      Mpi::barrier();
      ifstream.open(file_path);
      assert(ifstream.is_open());

      VERBOSE("Done");
    }
  }

  // TODO: The full- and leading-colour parts are nearly identical, so remove
  // code duplication. This refactoring should also make sure that we only use
  // one MPI barrier, instead of two. Even better, we should just broadcast the
  // data after reading it on the main rank, instead of reading it in on all
  // ranks.
  const std::string stem_lc {"colorfactor_lc_" + flav_proc.qcd_only_uid()};
  const fs::path file_name_lc {stem_lc + ".dat"};
  const fs::path file_path_lc {data_path / file_name_lc};

  // try to read leading colour factor file
  std::ifstream ifstream_lc {file_path_lc};
  if (!ifstream_lc.is_open()) {

    // the colour factor file does not exist; perhaps it is okay to fall back on
    // dummy factors ...
    if (!s.dummy_colour_factors_allowed) {

      // ... it is not, so let's generate the colour factor file using FORM
      VERBOSE("");
      STATUS("Generate leading colour matrix file for " + flav_proc.uid() +
             " using FORM ...");

      // wait so there there can be no race condition in the file I/O
      Mpi::barrier();

      if (Mpi::is_main_rank()) {

        // cd to our temporary directory, remember where we were
        const auto cwd {fs::current_path()};
        fs::current_path(Paths::temp);

        // generate the FORM scripts
        Leading_colour_factor_form_file_generator gen_lc {flav_proc};

        // run FORM; using "-F" ensures that all output is piped to a log file
        const int stat {std::system(("form -F " + stem_lc + ".frm").c_str())};
        if (WIFEXITED(stat) && WEXITSTATUS(stat) != 0) {
          ERROR("The call to FORM exited with status \"", WEXITSTATUS(stat),
                "\". Abort.");
          abort();
        }

        // cd to the previous directory
        fs::current_path(cwd);

        // move generated colour factor files and FORM log files into respective
        // target directories
        const fs::path temp_dir {Paths::temp};
        const fs::path logs_dir {Paths::logs};
        const fs::path log_file_name_lc {stem_lc + ".log"};
        fs::copy(temp_dir / file_name_lc, file_path_lc);
        fs::copy(temp_dir / log_file_name_lc, logs_dir / log_file_name_lc,
                 fs::copy_options::update_existing);
      }

      // now try to read colour factor file again
      Mpi::barrier();
      ifstream_lc.open(file_path_lc);
      assert(ifstream_lc.is_open());

      VERBOSE("Done");
    }
  }

  // Prepare memory
  const int n_entries = (n_perm * (n_perm + 1)) / 2;
  if (!ifstream.is_open() && s.dummy_colour_factors_allowed) {
    // If no file is found, but dummy colour factors are allowed (an option
    // only useful for development), set all factors to 1.0.
    factors.resize(n_entries, 1.0);
    Kokkos::resize(d_factors,n_entries);
    Kokkos::View<double*,Kokkos::HostSpace> h_factors(&factors[0],factors.size());
    Kokkos::deep_copy(d_factors,h_factors);

    leading_factors_mask.resize(n_perm, 1);
    Kokkos::resize(d_leading_factors_mask,n_perm);
    Kokkos::View<int*,Kokkos::HostSpace> h_leading_factors_mask(&leading_factors_mask[0],leading_factors_mask.size());
    Kokkos::deep_copy(d_leading_factors_mask,h_leading_factors_mask);
    return;
  }

  // Read in
  factors.resize(n_entries);
  std::string line;
  for (int i = 0; i < n_entries; i++) {
    getline(ifstream, line);
    line.erase(std::remove(line.begin(), line.end(), ' '), line.end());
    const size_t pos {line.find('/')};
    if (pos != std::string::npos) {
      factors[i] = std::stod(line.substr(0, pos)) /
                   std::stod(line.substr(pos + 1, line.length()));
    }
    else {
      factors[i] = std::stod(line);
    }
  }
  // copy factors to device
  Kokkos::resize(d_factors,n_entries);
  Kokkos::View<double*,Kokkos::HostSpace> h_factors(&factors[0],factors.size());
  Kokkos::deep_copy(d_factors,h_factors);
  factors.clear();

  // Read in leading-colour factors
  leading_factors_mask.resize(n_perm);
  for (int i = 0; i < n_perm; i++) {
    getline(ifstream_lc, line);
    line.erase(std::remove(line.begin(), line.end(), ' '), line.end());
    const size_t pos {line.find('/')};
    double fac {0.0};
    if (pos != std::string::npos) {
      fac = std::stod(line.substr(0, pos)) /
            std::stod(line.substr(pos + 1, line.length()));
    }
    else {
      fac = std::stod(line);
    }
    leading_factors_mask[i] = (fac == 0.0) ? 0 : 1;
  }
  // copy leading_factors_mask to device
  Kokkos::resize(d_leading_factors_mask,n_perm);
  Kokkos::View<int*,Kokkos::HostSpace> h_leading_factors_mask(&leading_factors_mask[0],leading_factors_mask.size());
  Kokkos::deep_copy(d_leading_factors_mask,h_leading_factors_mask);
  leading_factors_mask.clear();
}

const Kokkos::View<double*>& Colour_factors::data() const {
  return d_factors;
}

const Kokkos::View<int*>& Colour_factors::leading_factors_mask_data() const {
  return d_leading_factors_mask;
}
