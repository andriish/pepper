// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_EVENT_DATA_H
#define PEPPER_EVENT_DATA_H

#include "pepper/config.h"

#include "compiler_warning_macros.h"
PEPPER_SUPPRESS_COMMON_WARNINGS_PUSH
#include "bitmask/bitmask.h"
PEPPER_SUPPRESS_COMMON_WARNINGS_POP
#include "cvec4.h"
#include "helicity_configurations.h"
#include "event_data.h"
#include "vec4.h"
#include "Kokkos_Core.hpp"
#include <algorithm>
#include <memory>

#define HELICITY_BLOCK_SIZE 32

class Process;

enum class Event_data_component { leading_colour_configuration = 1 };
BITMASK_DEFINE_MAX_ELEMENT(Event_data_component, leading_colour_configuration)
using Event_data_components = bitmask::bitmask<Event_data_component>;

namespace detail{
  template <typename T>
  void configure_momentum_storage(T&);
  
  template <typename T>
  void configure_me_storage(T&);
}

class Event_handle {

public:
  Event_handle(const Process&, const Helicity_settings&, long batch_size,
             int n_phase_space_random_numbers);
  ~Event_handle();

  Event_handle(const Event_handle& rhs){device = rhs.device; host = rhs.host; };
  Event_handle& operator=(const Event_handle& rhs) {device= rhs.device;host=rhs.host; return (*this); };

  friend std::ostream& operator<<(std::ostream&, const Event_handle&);

  double xs_sum() const;
  double xs2_sum() const;

  int n_nonzero() const;
  void apply_weight_prefactor(double);

  int flav_channel(const int& evt) const { return host.flav_channels[evt]; }
  void set_flav_channel(const int& evt, const int& i) { host.flav_channels[evt] = i; }

  void reset_me(int perm);
  void reset_alt_me(int perm);
  void apply_me2_prefactor(const double& fac)
  {
    const auto& me2 = device.me2;
    Kokkos::parallel_for("apply_me2_prefactor",device.me2.extent(0),KOKKOS_LAMBDA(const int& i){
      me2[i] = me2[i] * fac;
    });
  }

  void set_fixed_alpha_s(double val){
    Kokkos::deep_copy(host.alpha_s,val);
    Kokkos::deep_copy(device.alpha_s,val);
  }

  void debug_w(const int& evt) const;
  void debug_r(const int& evt) const;
  void debug_p(const int& evt) const;
  void debug_c(const int& evt) const;
  void debug_internal_c(const int& evt) const;
  void debug_me(const int& evt) const;
  
  void add_required_nonzero_event_data_components(Event_data_component c)
  {
    required_nonzero_event_data_components |= c;
  }

  void pull_nonzero_events_from_device();
  
  template<typename T, typename buffer_type,typename host_type>
  void copy_buffer_2d(const buffer_type& dev_buffer,const host_type& host_obj,const int& num_nonzero) const;
  template<typename T, typename buffer_type,typename host_type>
  void copy_buffer_1d(const buffer_type& dev_buffer,const host_type& host_obj,const int& num_nonzero) const;

  void pull_random_numbers_from_device() const
  {
    Kokkos::deep_copy(host._r, device._r);
  };
  void pull_weights_from_device() const
  {
    Kokkos::deep_copy(host.w, device.w);
  };
  void pull_x12_from_device() const
  {
    Kokkos::deep_copy(host.x1, device.x1);
    Kokkos::deep_copy(host.x2, device.x2);
  }
  void pull_scales_from_device() const
  {
    Kokkos::deep_copy(host.mu2, device.mu2);
  }
  void pull_biases_from_device() const
  {
    Kokkos::deep_copy(host.ps_bias, device.ps_bias);
  }
  void pull_alpha_s_from_device() const
  {
    Kokkos::deep_copy(host.alpha_s, device.alpha_s);
  }
  void pull_momenta_from_device() const
  {
    Kokkos::deep_copy(host.e, device.e);
    Kokkos::deep_copy(host.px, device.px);
    Kokkos::deep_copy(host.py, device.py);
    Kokkos::deep_copy(host.pz, device.pz);
  }
  void pull_internal_momenta_from_device() const
  {
    Kokkos::deep_copy(host.e, device.e);
    Kokkos::deep_copy(host.px, device.px);
    Kokkos::deep_copy(host.py, device.py);
    Kokkos::deep_copy(host.pz, device.pz);
  }
  void pull_currents_from_device() const
  {
    Kokkos::deep_copy(host._c0, device._c0);
    Kokkos::deep_copy(host._c1, device._c1);
    Kokkos::deep_copy(host._c2, device._c2);
    Kokkos::deep_copy(host._c3, device._c3);
  }
  void pull_me2_from_device() const { Kokkos::deep_copy(host.me2, device.me2); }
  void pull_internal_particles_from_device() const
  {
    Kokkos::deep_copy(host.ptcl, device.ptcl);
  }
  void pull_helicities_from_device() const
  {
    Kokkos::deep_copy(host.hels, device.hels);
  }
  void pull_active_helicity_configuration_indexes_from_device() const
  {
    Kokkos::deep_copy(host.active_hel_conf_idx, device.active_hel_conf_idx);
  }
  void pull_flavour_channels() const
  {
    Kokkos::deep_copy(host.flav_channels, device.flav_channels);
  }
  void pull_active_lc_conf_from_device() const
  {
    Kokkos::deep_copy(host.active_lc_conf_idx, device.active_lc_conf_idx);
  }

  void push_random_numbers_to_device() {Kokkos::deep_copy(device._r,host._r);};
  void push_weights_to_device() {Kokkos::deep_copy(device.w,host.w);};
  void push_x12_to_device(){
    Kokkos::deep_copy(device.x1,host.x1);
    Kokkos::deep_copy(device.x2,host.x2);
  };
  void push_alpha_s_to_device(){
    Kokkos::deep_copy(device.alpha_s,host.alpha_s);
  }
  void push_momenta_to_device(){
    Kokkos::deep_copy(device.e,host.e);
    Kokkos::deep_copy(device.px,host.px);
    Kokkos::deep_copy(device.py,host.py);
    Kokkos::deep_copy(device.pz,host.pz);
  };
  void push_currents_to_device() {
    Kokkos::deep_copy(device._c0,host._c0);
    Kokkos::deep_copy(device._c1,host._c1);
    Kokkos::deep_copy(device._c2,host._c2);
    Kokkos::deep_copy(device._c3,host._c3);
  };
  void push_particles_to_device() {
    Kokkos::deep_copy(device.ptcl,host.ptcl);
  };
  void push_internal_particles_to_device() {
    Kokkos::deep_copy(device.ptcl,host.ptcl);
  };
  void push_helicities_to_device() {
    Kokkos::deep_copy(device.hels,host.hels);
  };
  void push_helicity_configurations_to_device() {
    Kokkos::deep_copy(device.hel_conf_idx,host.hel_conf_idx);
  };
  void push_active_helicities_to_device() {
    Kokkos::deep_copy(device.active_hels,host.active_hels);
    Kokkos::deep_copy(device.n_active_helicities,host.n_active_helicities);
  };
  void push_helicity_selection_weights_to_device() {
    Kokkos::deep_copy(device.hel_selection_weights,host.hel_selection_weights);
  };

  int proc_idx {0};

  Event_data<Kokkos::DefaultExecutionSpace::memory_space> device;
  Event_data<Kokkos::HostSpace> host;
  
  // The data we need for the downstream processing of nonzero events. At the
  // moment, this is only needed to steer what data is copied when calling
  // pull_nonzero_events_from_device. Note that momenta, weights and squared
  // matrix elements are always considered to be required.
  Event_data_components required_nonzero_event_data_components;

private:
  mutable double filter_nonzero_events_duration {0.0};
  mutable double pull_nonzero_events_duration {0.0};
};

namespace detail {

template<typename view_type>
void resize_and_init_1d(view_type& view,const int& size,const double init){
  Kokkos::resize(view,size);
  Kokkos::deep_copy(view,init);
}

template <typename T>
void configure_helicity_state_counts(T& data,
                                     const Helicity_treatment hel_treatment)
{
  // For helicity sampling, we need to hold the corresponding external
  // states only.  For helicity summing, we want to precalculate and hold
  // external states in both their "+" and "-" helicities.
  // Strictly speaking, we do not need one of 2*n_ptcl states when using
  // parity-symmetric summing, but its calculation time is negligible.
  data.n_external_helicity_states = data.n_ptcl;
  if (hel_treatment == Helicity_treatment::summed)
    data.n_external_helicity_states *= 2;
}

template <typename T>
void configure_weight_and_me2_storage(T& data)
{
  resize_and_init_1d(data.w,data.batch_size,1.0);
  resize_and_init_1d(data.me2,data.batch_size,-1.0);
  resize_and_init_1d(data.alpha_s,data.batch_size,-1.0);
}

template <typename T>
void configure_flavour_channel_storage(T& data)
{
  resize_and_init_1d(data.flav_channels,data.batch_size,0.0);
}

template <typename T>
void configure_active_lc_configuration_storage(T& data)
{
  resize_and_init_1d(data.active_lc_conf_idx,data.batch_size,-1);
}

template <typename T>
void configure_scale_storage(T& data)
{
  resize_and_init_1d(data.mu2,data.batch_size,1.0);
}

template <typename T>
void configure_biasing_storage(T& data)
{
  resize_and_init_1d(data.ps_bias,data.batch_size,1.0);
}

template <typename T>
void configure_coupling_storage(T& data)
{
  resize_and_init_1d(data.alpha_s,data.batch_size,1.0);
}

template <typename T>
void configure_momentum_storage(T& data)
{
  Kokkos::resize(data.e,
      data.batch_size, (data.n_internal_states + data.n_external_states));
  Kokkos::deep_copy(data.e,0.0);
  Kokkos::resize(data.px,
      data.batch_size, (data.n_internal_states + data.n_external_states));
  Kokkos::deep_copy(data.px,0.0);
  Kokkos::resize(data.py,
      data.batch_size, (data.n_internal_states + data.n_external_states));
  Kokkos::deep_copy(data.py,0.0);
  Kokkos::resize(data.pz,
      data.batch_size, (data.n_internal_states + data.n_external_states));
  Kokkos::deep_copy(data.pz,0.0);
}

template <typename T>
void configure_x12_storage(T& data)
{
  // Longitudinal momentum fractions
  resize_and_init_1d(data.x1,data.batch_size,1.0);
  resize_and_init_1d(data.x2,data.batch_size,1.0);
}

template <typename T>
void configure_xfx12_storage(T& data)
{
  resize_and_init_1d(data.xfx1,data.batch_size,1.0);
  resize_and_init_1d(data.xfx2,data.batch_size,1.0);
}

template <typename T> void configure_current_storage(T& data)
{
  Kokkos::resize(data._c0,data.batch_size,
                 (data.n_internal_states + data.n_external_helicity_states));
  Kokkos::resize(data._c1,data.batch_size,
                 (data.n_internal_states + data.n_external_helicity_states));
  Kokkos::resize(data._c2,data.batch_size,
                 (data.n_internal_states + data.n_external_helicity_states));
  Kokkos::resize(data._c3,data.batch_size,
                 (data.n_internal_states + data.n_external_helicity_states));
}

template <typename T> void configure_me_storage(T& data)
{
  Kokkos::resize(data.me_real,data.batch_size,data.n_me);
  Kokkos::deep_copy(data.me_real,0.0);
  Kokkos::resize(data.me_imag,data.batch_size,data.n_me);
  Kokkos::deep_copy(data.me_imag,0.0);
}

template <typename T>
void configure_storage(T& data, const Helicity_treatment hel_treatment)
{
  // Random numbers for sampling momenta
  int n_random_numbers {data.batch_size * data.n_phase_space_random_numbers};
  // Random numbers for unweighting
  // TODO: Make this dependent on the unweighting_disabled setting.
  n_random_numbers += data.batch_size;
  // Random numbers for flavour process channel selection
  n_random_numbers += data.batch_size;
  // Random numbers for leading colour configuration selection
  // TODO: Make this dependent on whether we actually select this
  n_random_numbers += data.batch_size;
  // Optionally add one random number for helicity sampling
  if (hel_treatment == Helicity_treatment::sampled)
    n_random_numbers += data.n_helicity_blocks;
  resize_and_init_1d(data._r, n_random_numbers, -1.0);

  configure_momentum_storage(data);
  configure_x12_storage(data);
  configure_biasing_storage(data);

  // Kinematic information (redundant, but we need some scratch storage for
  // calculations)
  Kokkos::resize(data.y,data.batch_size,(data.n_ptcl - 2));
  Kokkos::resize(data.phi,data.batch_size,(data.n_ptcl - 2));

  configure_current_storage(data);
  configure_me_storage(data);
}

template <typename T>
void configure_particle_and_helicity_storage(
    T& data, const Helicity_treatment hel_treatment)
{
  if (hel_treatment == Helicity_treatment::sampled) {
    // note that in the current context, n_active_helicities is set to the
    // maximum number of active helicities, see
    // Event_handle::initialise_host_data(), hence we reserve enough storage to
    // hold the required data for any of the flavour process groups
    Kokkos::resize(data.hels,data.n_internal_states * data.n_active_helicities());
    resize_and_init_1d(data.hel_conf_idx,data.n_helicity_blocks,-1);
    Kokkos::resize(data.active_hel_conf_idx,data.n_helicity_blocks);
    Kokkos::resize(data.ptcl,data.n_external_states +
                     data.n_internal_states * data.n_active_helicities());
    resize_and_init_1d(data.hel_selection_weights,data.n_active_helicities(), 1.0 / data.n_active_helicities());
  }
  else {
    Kokkos::resize(data.hels,data.n_internal_states);
    resize_and_init_1d(data.hel_conf_idx,1,-1);
    Kokkos::resize(data.active_hel_conf_idx,1);
    Kokkos::resize(data.ptcl,data.n_external_states + data.n_internal_states);

    // TODO: delete this after making sure that the helicity selection weights
    // are never accessed during summed event generation
    resize_and_init_1d(data.hel_selection_weights,data.n_active_helicities(), 1.0 / data.n_active_helicities());
  }
}

template <typename T>
void configure_internal_state_count(T& data, int n_lepton_pairs)
{
  assert(n_lepton_pairs > -1 && n_lepton_pairs < 2);
  data.n_internal_states = (data.n_ptcl * (data.n_ptcl - 1)) / 2;
  // add one additional current for the simultaneous computation
  // of Z/photon currents
  if (n_lepton_pairs == 1)
    data.n_internal_states++;
}

template <typename T>
void configure_matrix_element_counts(T& data,
                                     const Helicity_settings& hel_settings)
{
  // For cached helicity summing, we need to store twice the number of
  // matrix elements, both for the "+" and the "-" helicity of the final
  // current.
  data.n_me = data.n_perm;
  if (hel_settings.cached_summing())
    data.n_me *= 2;
}

} // namespace detail

#endif
