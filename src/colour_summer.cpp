// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "pepper/config.h"

#include "colour_summer.h"
#include "colour_summer_kernel.h"
#include "helicity_configurations.h"

Colour_summer::Colour_summer(const Flavour_process& flav_proc, int _n_perm,
                             const Settings& s)
    : n_perm {_n_perm},
      cf {flav_proc, n_perm, s}
{
}

void Colour_summer::reset_me2(const Event_handle& evt) const
{
  Kokkos::deep_copy(evt.device.me2,0.0);
}

void Colour_summer::update_me2(const Event_handle& evt,
                               const Helicity_settings& hel_settings,
                               double prefactor) const
{
  const auto& cf_data = cf.data();
  const bool cached_summing = hel_settings.cached_summing();
  const auto& _n_perm = n_perm;
  RUN_KERNEL(update_me2_kernel, evt, _n_perm, cf_data, cached_summing,
             prefactor);
  Kokkos::fence(__func__);
}

void Colour_summer::select_leading_colour_configuration(
    const Event_handle& evt, const Helicity_settings& hel_settings) const
{
  const auto& cf_leading_factors_mask = cf.leading_factors_mask_data();
  const bool cached_summing = hel_settings.cached_summing();
  const auto& _n_perm = n_perm;
  RUN_KERNEL(select_leading_colour_configuration_kernel, evt, _n_perm,
             cf_leading_factors_mask, cached_summing);
  Kokkos::fence(__func__);
}
