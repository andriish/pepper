// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_PERMUTATION_H
#define PEPPER_PERMUTATION_H

#include "pepper/config.h"

#include <numeric>
#include <vector>
#include "Kokkos_Core.hpp"

class Flavour_process;

class Permutations {

public:
  Permutations(const Flavour_process&);
  int get(int perm_idx, int inner_idx) const
  {
    return permutations[_perm_size * perm_idx + inner_idx];
  }
  int get_qcd(int qcd_perm_idx, int inner_idx) const
  {
    return get(get_index_for_qcd_permutation_group(qcd_perm_idx), inner_idx);
  }
  int get_final() const
  {
    // With our conventions, the index of the "final" current is not affected
    // by permutations.
    return get(0, 0);
  }
  double sign(int perm_idx) const { return signs[perm_idx]; }
  const std::vector<int>& get() const { return permutations; }
  int n() const { return n_perm; }
  int size() const { return n_perm; }
  int n_qcd() const { return n_qcdperm; }
  int n_qed(int qcd_idx) const { return qed_permutation_counts[qcd_idx]; }
  int perm_size() const { return _perm_size; }

  // return QCD permutation for a mapped flavour channel, swapping permutation
  // indizes for flavour lines that are conjugated in the mapped channel
  std::vector<int> mapped_qcd_permutation_for_flavour_process(
      const Flavour_process&, int flav_channel, int perm_idx) const;

  const Kokkos::View<int*>& h_or_d() const { return d_permutations; };

private:
  void append_full_permutations_for_melia_permuation(
      const Flavour_process&, const std::vector<int>& melia_perm);
  int get_index_for_qcd_permutation_group(int qcd_perm_idx) const
  {
    // find first permutation index of the permutations
    // associated with the requested QCD permutation
    return std::accumulate(qed_permutation_counts.begin(),
                           qed_permutation_counts.begin() + qcd_perm_idx, 0);
  }
  int n_perm {0};
  int n_qcdperm {0};
  int _perm_size {0};
  std::vector<int> qed_permutation_counts;
  std::vector<int> permutations;
  std::vector<double> signs;

  Kokkos::View<int*> d_permutations;
};

std::vector<int> flavour_base_dyck(const Flavour_process&,
                                   const std::vector<int>& perm_from_base_dyck);

#endif
