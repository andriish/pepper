// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "lhef_writer.h"
#include "citations.h"
#include "event_data.h"
#include "fill_leading_colour_information.h"
#include "flavour_process.h"
#include "paths.h"
#include "pdf_and_alpha_s.h"
#include "process.h"
#include "utilities.h"

#include "HepMC3/LHEFAttributes.h"
#include <random>

enum class Lhef_particle_status {
  incoming = -1,
  outgoing = 1,
  intermediate_space_like_propagator_with_preserved_x_and_q2 = -2,
  intermediate_resonance_with_preserved_mass = 2,
  intermediate_resonance_for_documentation_only = 3,
  incoming_beam = -9
};
// SPINUP = 9 denotes unknown/unpolarized spin, see hep-ph/0109068
constexpr double lhef_spin_unknown_or_unpolarized {9.0};

using namespace Paths;
using namespace HepMC3;

Lhef_writer::Lhef_writer(const std::string& _filepath, const Settings& s,
                         const SM_parameters& sm)
    : unweighting_disabled {s.unweighting_disabled},
      leading_colour_flow_output_enabled {s.leading_colour_flow_output_enabled},
      e_cms {s.e_cms},
      ew_alpha {sm.ew.alpha()},
      filepath {path_with_rank_info(_filepath)}
{
  INFO("Events will be written in LHEF v3 format to \"",
       path_with_rank_pattern(_filepath), "\"");
}

Lhef_writer::Lhef_writer(std::ostream& _stream, const Settings& s,
                         const SM_parameters& sm)
    : unweighting_disabled {s.unweighting_disabled},
      leading_colour_flow_output_enabled {s.leading_colour_flow_output_enabled},
      e_cms {s.e_cms},
      ew_alpha {sm.ew.alpha()},
      stream {&_stream}
{
  INFO("Events will be written in LHEF v3 format to stdout");
}

void Lhef_writer::initialise(const Process& proc,
                             const Pdf_and_alpha_s& pdf_and_alphas)
{
  Citations::register_citation("HepMC v3", "Buckley:2019xhk");
  Citations::register_citation("LHEF v3", "Alwall:2006yp,Andersen:2014efa");

  colours.resize(proc.n_ptcl());
  anticolours.resize(proc.n_ptcl());

  assert(stream != nullptr || !filepath.empty());
  if (stream != nullptr) {
    initialise_lhef_writer(*stream);
  }
  else if (!filepath.empty()) {
    if (case_insensitive_ends_with(filepath, ".gz")) {
      zstream = std::unique_ptr<std::ostream>(new zstr::ofstream(filepath));
      initialise_lhef_writer(*zstream);
    }
    else {
      initialise_lhef_writer(filepath);
    }
  }
  initialised = true;

  // determine beam particles via a heuristic approach
  if (pdf_and_alphas.pdf_enabled()) {
    // PDF use is enabled, for now we assume that this means that we are doing
    // proton-proton collisions
    beams[0] = 2212;
    beams[1] = 2212;
  }
  else if (proc.n_procs() == 1) {
    // no PDF is used, but there is only a single process, so let's use its
    // initial-state particles as pure partonic beams in this case
    beams[0] = proc[0][0];
    beams[1] = proc[0][1];
  }

  const int n_procs {proc.n_procs()};
  lhef_writer->heprup.IDBMUP = std::make_pair(beams[0], beams[1]);
  lhef_writer->heprup.EBMUP = std::make_pair(e_cms / 2.0, e_cms / 2.0);
  lhef_writer->heprup.PDFGUP = std::make_pair(0, 0);
  lhef_writer->heprup.PDFSUP =
      std::make_pair(pdf_and_alphas.lhapdf_id(), pdf_and_alphas.lhapdf_id());
  lhef_writer->heprup.IDWTUP = unweighting_disabled ? -1 : -3;
  lhef_writer->heprup.resize(n_procs);
  for (int i {0}; i < n_procs; i++) {
    if (proc.has_partial_results()) {
      lhef_writer->heprup.XSECUP[i] = proc.partial_results[i].mean();
      lhef_writer->heprup.XERRUP[i] = proc.partial_results[i].stddev();
      if (!unweighting_disabled) {
        lhef_writer->heprup.XMAXUP[i] = proc.partial_max_weights[i];
      }
    }
    else {
      lhef_writer->heprup.XSECUP[i] = NAN;
      lhef_writer->heprup.XERRUP[i] = NAN;
      if (!unweighting_disabled) {
        lhef_writer->heprup.XMAXUP[i] = NAN;
      }
    }
  }
  lhef_writer->init();
}

void Lhef_writer::finalise(const MC_result&)
{
  lhef_writer.reset();
}

void Lhef_writer::write_n(const Event_handle& evt, int n, const MC_result&,
                          long n_nonzero, long n_trials, const Process& proc)
{
  // To ensure proper normalization of the total cross section, the "trials"
  // needs to be evenly distributed over all the events in the batch.
  // Otherwise, when events are rejected the total cross section will be modified.
  size_t trials_add = n_trials / n_nonzero;
  std::vector<size_t> trials_remainder(n_nonzero, 0);
  for(size_t i = 0; i < n_trials-trials_add*n_nonzero; ++i) {
    trials_remainder[i] = 1;
  }
  std::random_device device;
  std::mt19937 gen(device());
  std::shuffle(trials_remainder.begin(), trials_remainder.end(), gen);

  size_t remainder_idx = 0;
  for (int i {0}; i < n; ++i) {

    if (evt.host.w(i) == 0.0)
      continue;

    const auto n_evt_trials = trials_add + trials_remainder[remainder_idx++];
    lhef_writer->hepeup.ntries = n_evt_trials;

    lhef_writer->hepeup.resize(evt.host.n_ptcl);
    lhef_writer->hepeup.IDPRUP = evt.proc_idx;
    if (unweighting_disabled)
      lhef_writer->hepeup.XWGTUP = evt.host.w(i) * evt.host.me2(i);
    else
      lhef_writer->hepeup.XWGTUP = (evt.host.w(i) < 0.0) ? -1.0 : 1.0;
    lhef_writer->hepeup.SCALUP = std::sqrt(evt.host.mu2(i));
    lhef_writer->hepeup.AQEDUP = ew_alpha;
    lhef_writer->hepeup.AQCDUP = evt.host.alpha_s(i);

    if (leading_colour_flow_output_enabled) {
      fill_leading_colour_information(evt, i, proc, colours, anticolours);
    }

    for (int p {0}; p < evt.host.n_ptcl; ++p) {
      const bool is_incoming {p < 2};
      const Ptcl_num ptcl_num {proc.current_flavour_process().unmapped_ptcl(evt.flav_channel(i), p)};
      lhef_writer->hepeup.IDUP[p] = ptcl_num;
      int mother_p1 {0};
      int mother_p2 {0};
      Vec4 mom {evt.host.p(i, p)};
      Lhef_particle_status status {Lhef_particle_status::outgoing};
      if (is_incoming) {
        mom *= -1;
        status = Lhef_particle_status::incoming;
      }
      else {
        // outgoing particles have the incoming ones as mothers
        mother_p1 = 1;
        mother_p2 = 2;
      }
      lhef_writer->hepeup.ISTUP[p] = static_cast<int>(status);
      lhef_writer->hepeup.MOTHUP[p] = std::make_pair(mother_p1, mother_p2);
      lhef_writer->hepeup.PUP[p][0] = mom[1];
      lhef_writer->hepeup.PUP[p][1] = mom[2];
      lhef_writer->hepeup.PUP[p][2] = mom[3];
      lhef_writer->hepeup.PUP[p][3] = mom[0];
      lhef_writer->hepeup.PUP[p][4] = sm_properties::getInstance().hmass(ptcl_num);
      lhef_writer->hepeup.VTIMUP[p] = 0.0;
      lhef_writer->hepeup.SPINUP[p] = lhef_spin_unknown_or_unpolarized;
      if (leading_colour_flow_output_enabled) {
        int colour {colours[p]};
        int anticolour {anticolours[p]};
        if (is_incoming)
          std::swap(colour, anticolour);
        lhef_writer->hepeup.ICOLUP[p] = std::make_pair(colour, anticolour);
      }
    }

    lhef_writer->hepeup.heprup =  &(lhef_writer->heprup);
    lhef_writer->writeEvent();

    n_written++;
  }
}
