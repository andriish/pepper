// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_MELIA_DYCK_WORDS_H
#define PEPPER_MELIA_DYCK_WORDS_H

#include "math.h"
#include "flavour_process.h"
#include <vector>

class Melia_dyck_words {

public:
  Melia_dyck_words(const Flavour_process&);

  // access words, base word, flavoured base word
  const std::vector<std::vector<int>>& operator()() const { return words; }
  const std::vector<int>& base() const { return _base; }
  const std::vector<Ptcl_num>& flavoured_base() const
  {
    return _flavoured_base;
  }

  // Return the permutation to get from the process to the base dyck word.
  // Since the latter is only concerned with QCD particle content, it might
  // be shorter. Therefore, -1 is used to denote (non-QCD) particles that are
  // permuted nowhere (i.e. do not occur in the base dyck word).
  const std::vector<int>& proc_to_base_perm() const
  {
    return _proc_to_base_perm;
  }

  // Calculate the permutation to get from the base dyck word to the given "to"
  // dyck word.
  std::vector<int> from_base_perm(std::vector<int> to);

private:
  void generate_words();
  void generate_base();
  void generate_flavoured_base();
  void fill_base_to_proc_perm();
  void fill_proc_to_base_perm();

  Flavour_process proc;
  std::vector<int> base_to_proc_perm;
  std::vector<int> _proc_to_base_perm;
  std::vector<std::vector<int>> words;
  std::vector<int> _base;
  std::vector<Ptcl_num> _flavoured_base;
};

#endif
