// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef VCL_VEC4_H
#define VCL_VEC4_H

#include "compiler_warning_macros.h"
PEPPER_SUPPRESS_COMMON_WARNINGS_PUSH
#define VCL_NAMESPACE Vcl
#include <vectorclass.h>
PEPPER_SUPPRESS_COMMON_WARNINGS_POP

#include "math.h"
#include "vec3.h"

static const Vcl::Vec4d mu {1.0, -1.0, -1.0, -1.0};

class Vcl_cvec4;
struct Scl_cvec4;
struct Weyl;

class Vcl_vec4 {

public:
  Vcl_vec4(double e, double px, double py, double pz) : data {e, px, py, pz} {};

  Vec3 vec3() const { return {(*this)[1], (*this)[2], (*this)[3]}; }

  double signed_abs() const
  {
    const double _abs2 {abs2()};
    return (_abs2 < 0.0 ? -1.0 : 1.0) * std::sqrt(std::abs(_abs2));
  };

  double abs() const { return std::sqrt(abs2()); };

  double abs2() const { return (*this) * (*this); };

  double p_plus() const { return (*this)[0] + (*this)[3]; }

  double p_minus() const { return (*this)[0] - (*this)[3]; }

  double p_perp() const { return std::sqrt(p_perp2()); }
  double p_perp2() const {
    return (*this)[1] * (*this)[1] + (*this)[2] * (*this)[2];
  }

  double m_perp() const { return std::sqrt(m_perp2()); }
  double m_perp2() const {
    return p_plus() * p_minus();
  }
  double y() const {
    return 0.5*log(p_plus()/p_minus());
  }
  KOKKOS_INLINE_FUNCTION
  double vec3_abs() const {
    return sqrt(vec3_abs2());
  }
  KOKKOS_INLINE_FUNCTION
  double vec3_abs2() const {
    return (*this)[1]*(*this)[1]+(*this)[2]*(*this)[2]+(*this)[3]*(*this)[3];
  }
  KOKKOS_INLINE_FUNCTION
  double cosPhi() const {
    if(p_perp()==0) return 0.;
    return std::max(std::min((*this)[1]/p_perp(),1.0),-1.0);
  }
  KOKKOS_INLINE_FUNCTION
  double phi() const {
    if((*this)[2]>0.) return acos(cosPhi());
    else return -acos(cosPhi());
  }

  double operator[](int i) const
  {
    return data[i];
  }

  Vcl_vec4& operator+=(const Vcl_vec4& rhs)
  {
    data += rhs.data;
    return *this;
  }

  Vcl_vec4& operator-=(const Vcl_vec4& rhs)
  {
    data -= rhs.data;
    return *this;
  }

  Vcl_vec4& operator*=(double rhs)
  {
    data *= rhs;
    return *this;
  }

  friend Vcl_vec4 operator+(Vcl_vec4 lhs, const Vcl_vec4& rhs)
  {
    return lhs += rhs;
  }

  friend Vcl_vec4 operator-(Vcl_vec4 lhs, const Vcl_vec4& rhs)
  {
    return lhs -= rhs;
  }

  friend Vcl_vec4 operator*(Vcl_vec4 lhs, double rhs)
  {
    return lhs *= rhs;
  }

  friend Vcl_vec4 operator*(double lhs, Vcl_vec4 rhs)
  {
    return rhs *= lhs;
  }

  friend double operator*(const Vcl_vec4& lhs, const Vcl_vec4& rhs)
  {
    return horizontal_add(mu*lhs.data*rhs.data);
  }

  friend C operator*(const Vcl_cvec4& lhs, const Vcl_vec4& rhs);

  friend std::ostream& operator<<(std::ostream& o, const Vcl_vec4& v)
  {
    return o << "(" << v[0] << ", " << v[1] << ", " << v[2] << ", " << v[3] << ")";
  }

  friend Vcl_cvec4;

private:
  Vcl::Vec4d data;
};

class Vcl_cvec4 {

public:
  Vcl_cvec4(const C& c1, const C& c2, const C& c3, const C& c4)
      : data {c1.real(), c2.real(), c3.real(), c4.real(),
              c1.imag(), c2.imag(), c3.imag(), c4.imag()} {}

  Vcl_cvec4(const Vcl_vec4& reals, const Vcl_vec4& imags)
      : data {reals.data, imags.data} {}

  Vcl_cvec4(const Scl_cvec4&);

  Vcl_cvec4(const Weyl&, const Weyl&);

  C abs() const { return Complex::sqrt(abs2()); };

  C abs2() const { return (*this) * (*this); };

  C p_plus() const { return (*this)[0] + (*this)[3]; }

  C p_minus() const { return (*this)[0] - (*this)[3]; }

  C operator[](int i) const
  {
    return {data[i], data[i + 4]};
  }

  Vcl_cvec4& operator+=(const Vcl_cvec4& rhs)
  {
    data += rhs.data;
    return *this;
  }

  Vcl_cvec4& operator-=(const Vcl_cvec4& rhs)
  {
    data -= rhs.data;
    return *this;
  }

  friend Vcl_cvec4 operator+(Vcl_cvec4 lhs, const Vcl_cvec4& rhs)
  {
    return lhs += rhs;
  }

  friend Vcl_cvec4 operator-(Vcl_cvec4 lhs, const Vcl_cvec4& rhs)
  {
    return lhs -= rhs;
  }

  friend C operator*(const Vcl_cvec4& lhs, const Vcl_vec4& rhs);

  friend C operator*(const Vcl_cvec4& lhs, const Vcl_cvec4& rhs)
  {
    return {horizontal_add(mu * (lhs.data.get_low() * rhs.data.get_low() -
                                 lhs.data.get_high() * rhs.data.get_high())),
            horizontal_add(mu * (lhs.data.get_low() * rhs.data.get_high() +
                                 lhs.data.get_high() * rhs.data.get_low()))};
  }

  Vcl_cvec4& operator/=(const C& rhs)
  {
    return *this *= 1.0/rhs;
  }

  Vcl_cvec4& operator*=(const C& rhs)
  {
    Vcl::Vec4d reals {data.get_low() * rhs.real() - data.get_high() * rhs.imag()};
    Vcl::Vec4d imags {data.get_low() * rhs.imag() + data.get_high() * rhs.real()};
    data =  Vcl::Vec8d{reals, imags};
    return *this;
  }

  friend Vcl_cvec4 operator*(Vcl_cvec4 lhs, const C& rhs)
  {
    return lhs *= rhs;
  }

  friend Vcl_cvec4 operator*(const C& lhs, Vcl_cvec4 rhs)
  {
    return rhs *= lhs;
  }

  friend Vcl_cvec4 operator/(Vcl_cvec4 lhs, const C& rhs)
  {
    return lhs /= rhs;
  }

  friend Vcl_cvec4 operator/(const C& lhs, Vcl_cvec4 rhs)
  {
    return rhs /= lhs;
  }

  friend std::ostream& operator<<(std::ostream& o, const Vcl_cvec4& v)
  {
    return o << "(" << v[0] << ", " << v[1] << ", " << v[2] << ", " << v[3] << ")";
  }
  
private:
  Vcl::Vec8d data;
};

class Vcl_cvec2 {

public:
  Vcl_cvec2(const C& c1, const C& c2)
      : data {c1.real(), c2.real(), c1.imag(), c2.imag()}
  {
  }

  C operator[](int i) const
  {
    return {data[i], data[i + 2]};
  }

  Vcl_cvec2& operator*=(const Vcl_cvec2& rhs)
  {
    data = Vcl::Vec4d {data.get_low() * rhs.data.get_low() -
                           data.get_high() * rhs.data.get_high(),
                       data.get_low() * rhs.data.get_high() +
                           data.get_high() * rhs.data.get_low()};
    return *this;
  }

  Vcl_cvec2& operator+=(const Vcl_cvec2& rhs)
  {
    data += rhs.data;
    return *this;
  }

  Vcl_cvec2& operator-=(const Vcl_cvec2& rhs)
  {
    data -= rhs.data;
    return *this;
  }

  friend Vcl_cvec2 operator*(Vcl_cvec2 lhs, const Vcl_cvec2& rhs)
  {
    return lhs *= rhs;
  }

  friend Vcl_cvec2 operator+(Vcl_cvec2 lhs, const Vcl_cvec2& rhs)
  {
    return lhs += rhs;
  }

  friend Vcl_cvec2 operator-(Vcl_cvec2 lhs, const Vcl_cvec2& rhs)
  {
    return lhs -= rhs;
  }

  /*
  friend C operator*(const Vcl_cvec2& lhs, const Vcl_cvec2& rhs)
  {
    return {horizontal_add(lhs.data.get_low() * rhs.data.get_low() -
                           lhs.data.get_high() * rhs.data.get_high()),
            horizontal_add(lhs.data.get_low() * rhs.data.get_high() +
                           lhs.data.get_high() * rhs.data.get_low())};
  }
  */

  Vcl_cvec2& operator/=(const C& rhs)
  {
    return *this *= 1.0/rhs;
  }

  Vcl_cvec2& operator*=(const C& rhs)
  {
    Vcl::Vec2d reals {data.get_low() * rhs.real() - data.get_high() * rhs.imag()};
    Vcl::Vec2d imags {data.get_low() * rhs.imag() + data.get_high() * rhs.real()};
    data = Vcl::Vec4d{reals, imags};
    return *this;
  }

  friend Vcl_cvec2 operator*(Vcl_cvec2 lhs, const C& rhs)
  {
    return lhs *= rhs;
  }

  friend Vcl_cvec2 operator*(const C& lhs, Vcl_cvec2 rhs)
  {
    return rhs *= lhs;
  }

  friend Vcl_cvec2 operator/(Vcl_cvec2 lhs, const C& rhs)
  {
    return lhs /= rhs;
  }

  friend Vcl_cvec2 operator/(const C& lhs, Vcl_cvec2 rhs)
  {
    return rhs /= lhs;
  }

  friend std::ostream& operator<<(std::ostream& o, const Vcl_cvec2& v)
  {
    return o << "(" << v[0] << ", " << v[1] << ")";
  }
  
private:
  Vcl::Vec4d data;
};

inline C operator*(const Vcl_cvec4& lhs, const Vcl_vec4& rhs)
{
  return {
    horizontal_add(mu * lhs.data.get_low() * rhs.data),
    horizontal_add(mu * lhs.data.get_high() * rhs.data)
  };
}

inline Vcl_cvec4 operator*(const C& lhs, const Vcl_vec4& rhs)
{
  return {lhs.real() * rhs, lhs.imag() * rhs};
}

#endif
