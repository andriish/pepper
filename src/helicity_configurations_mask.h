// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_HELICITY_CONFIGURATIONS_MASK_H
#define PEPPER_HELICITY_CONFIGURATIONS_MASK_H

#include <cstddef>
#include <vector>

class Event_handle;

class Helicity_configurations_mask {
public:
  Helicity_configurations_mask(int n) : mask(n, 1), n_disabled {0} {}
  bool enabled(int idx) const { return mask[idx]; }
  void set_enabled(int idx, bool enabled)
  {
    if (mask[idx] != enabled) {
      mask[idx] = enabled;
      if (enabled)
        n_disabled--;
      else
        n_disabled++;
    }
  }
  size_t n() const { return mask.size(); }
  size_t size() const { return n(); }
  int n_enabled() const { return mask.size() - n_disabled; }
  void update(Event_handle& evt) const;
  void debug_enabled_helicities(const Event_handle& evt) const;

private:
  std::vector<int> mask;
  int n_disabled;
};

#endif
