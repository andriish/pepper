// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_RNG_H
#define PEPPER_RNG_H

#include "pepper/config.h"

#include "Kokkos_Core.hpp"

#include "print.h"
#include "timing.h"
#include <random>
#include <vector>

#if !FORCE_HOST_RNG
#include "Kokkos_Random.hpp"
#endif

class Event_handle;

// Transform a given random number to an integer between min and max,
// inclusively.
KOKKOS_FUNCTION int ri(double r, int min, int max);

struct Rng {
public:
  Rng(int _seed_offset);
  ~Rng();

  // fill random number storage on the host or device
  void fill(Event_handle&);

  // generate random number in [0.0, 1.0) on the host
  double d();

  void reset();

  void reset_timing() const { fill_d_duration = 0.0; }

  // helper function for fill()
  template <typename T> void fill_d(T& v)
  {
    Timer timer;
    const size_t n = v.extent(0);
#if !FORCE_HOST_RNG
    const auto& rp = random_pool;
    Kokkos::parallel_for(__func__,n,KOKKOS_LAMBDA(const int& i){
      // acquire the state of the random number generator engine
      auto generator = rp.get_state();
      v[i] = generator.drand();
      // do not forget to release the state of the engine
      rp.free_state(generator);
    });
    Kokkos::fence("Rng::fill_d");
#else
    for (size_t i {0}; i < n; ++i) {
      v[i] = d();
    }
#endif
    fill_d_duration += timer.elapsed();
  };


private:

#if !FORCE_HOST_RNG
  // Kokkos Random needs
  Kokkos::Random_XorShift64_Pool<> random_pool;
#endif

  // C++ random needs
  std::mt19937_64 gen;
  std::uniform_real_distribution<double> dis {0.0, 1.0};

  int seed_offset {0};
  mutable double fill_d_duration {0.0};
};

#endif
