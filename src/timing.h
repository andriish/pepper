// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_TIMING_H
#define PEPPER_TIMING_H

#include <chrono>
#include <map>
#include <string>
#include <iostream>
#include <vector>

#define PEPPER_TIMING_ENABLED 1

#if PEPPER_TIMING_ENABLED

namespace Timing {

  // tasks for which durations are measured; the indentation follows the intended
  // nesting of the tasks in the code
  enum class Task {
    Pepper = 0,
      Initialisation,
      Optimisation,
      Unweighting_setup,
      Event_generation,
        Phase_space,
          Cuts,
          Momenta_and_weights,
          Update_weights,
        Output,
          Filter_nonzero_events,
          Pull_nonzero_events,
          Hdf5_write,
        Recursion,
          Momenta_preparation,
          Currents_preparation,
          Internal_currents_reset,
          Internal_particle_information_reset,
          Calculate_currents,
          ME_reset,
          ME_update,
          ME2_update,
        External_states_construction,
        Pdf_and_alpha_s,
          Pdf,
          Alpha_s,
        Rng,
      Hdf5_open,
      Hdf5_init,
      Hdf5_close,
      Hdf5_phase_space_points_open,
  };

void initialise();
void finalise(double duration);
void register_duration(const std::vector<Task>& labels, double duration);
void print_durations(std::ostream&);
}; // namespace Timing

class Timer {
private:
  // Type aliases to make accessing nested type easier
  using Clock = std::chrono::steady_clock;
  using Second = std::chrono::duration<double, std::ratio<1>>;

  std::chrono::time_point<Clock> m_beg {Clock::now()};

public:
  void reset() { m_beg = Clock::now(); }

  double elapsed() const
  {
    return std::chrono::duration_cast<Second>(Clock::now() - m_beg).count();
  }

  void register_elapsed(const std::vector<Timing::Task>& duration_labels) const
  {
    Timing::register_duration(duration_labels, elapsed());
  }
};

#else

// add dummy implementation if timing is disabled

namespace Timing {
inline void initialise() { };
inline void finalise(double) { };
inline void register_duration(const std::vector<Task>&, double) { };
inline void print_durations(std::ostream&) { };
}; // namespace Timing

class Timer {
public:
  void reset() { }
  double elapsed() const { return 0.0; }
  void register_elapsed(const std::vector<Timing::Task>&) const { }
};

#endif

#endif
