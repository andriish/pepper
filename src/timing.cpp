// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "timing.h"

#if PEPPER_TIMING_ENABLED

#include "print.h"
#include "tree/tree.h"
#include <iomanip>

using namespace Timing;

namespace Timing {
namespace detail {

using Duration_tree = tree<std::pair<Task, double>>;
Duration_tree durations;
bool initialised {false};

std::string to_string(Task t)
{
  switch (t) {
  case Task::Pepper:
    return "Total";
  case Task::Initialisation:
    return "Initialisation";
  case Task::Optimisation:
    return "Optimisation";
  case Task::Unweighting_setup:
    return "Unweighting setup";
  case Task::Event_generation:
    return "Event generation";
  case Task::Phase_space:
    return "Phase space";
  case Task::Cuts:
    return "Cuts";
  case Task::Momenta_and_weights:
    return "Fill momenta and weights";
  case Task::Update_weights:
    return "Update weights";
  case Task::Output:
    return "Output";
  case Task::Filter_nonzero_events:
    return "Filter non-zero events";
  case Task::Pull_nonzero_events:
    return "Pull non-zero events";
  case Task::Hdf5_write:
    return "HDF5 write";
  case Task::Recursion:
    return "Recursion";
  case Task::Momenta_preparation:
    return "Momenta preparation";
  case Task::Currents_preparation:
    return "Currents preparation";
  case Task::Internal_currents_reset:
    return "Internal currents reset";
  case Task::Internal_particle_information_reset:
    return "Internal particle information reset";
  case Task::Calculate_currents:
    return "Calculate currents";
  case Task::ME_reset:
    return "ME reset";
  case Task::ME_update:
    return "ME update";
  case Task::ME2_update:
    return "ME2 update";
  case Task::External_states_construction:
    return "External state construction";
  case Task::Pdf_and_alpha_s:
    return "PDF and AlphaS evaluation";
  case Task::Pdf:
    return "PDF";
  case Task::Alpha_s:
    return "AlphaS";
  case Task::Rng:
    return "Random number generation";
  case Task::Hdf5_open:
    return "HDF5 open";
  case Task::Hdf5_init:
    return "HDF5 init";
  case Task::Hdf5_close:
    return "HDF5 close";
  case Task::Hdf5_phase_space_points_open:
    return "HDF5 open phase-space points";
  }
  assert(false);
  return "Unknown";
}

}; // namespace detail
}; // namespace Timing

using namespace Timing::detail;

void Timing::initialise()
{
  // insert root node
  durations.insert(durations.begin(), std::make_pair(Task::Pepper, 0.0));
  initialised = true;
}

void Timing::finalise(double duration)
{
  if (!initialised)
    return;
  // insert total time into root node
  durations.begin()->second = duration;
}

void Timing::register_duration(const std::vector<Task>& labels, double duration)
{
  if (!initialised)
    return;
  Duration_tree::iterator parent {durations.begin()};
  for (const auto& label : labels) {
    // find child with this label
    auto child_it = std::find_if(parent.begin(), parent.end(),
                                 [&label](std::pair<Task, double> const& elem) {
                                   return elem.first == label;
                                 });
    // the (new) child is the parent for the next iteration
    if (child_it == parent.end()) {
      parent = durations.append_child(parent, std::make_pair(label, 0.0));
    }
    else {
      parent = child_it;
    }
  }
  // insert duration in leaf node
  parent->second = duration;
}

void Timing::print_durations(std::ostream& out)
{
  if (!initialised)
    return;
  // determine maximum label column width
  int label_column_width {0};
  for (auto it = detail::durations.begin(); it != detail::durations.end();
       it++) {
    int width {static_cast<int>(to_string(it->first).size() +
                                detail::Duration_tree::depth(it))};
    label_column_width = std::max(label_column_width, width);
  }
  // add one for the seperator (",")
  label_column_width += 1;

  // print header
  out << std::left << std::setw(label_column_width) << "Task,"
      << " Duration [s],"
      << " Ratio of Total [%],"
      << " Self ID, Parent ID\n";

  auto it = detail::durations.begin();

  // determine total duration
  const auto total = it->second;

  // sort entire tree by duration
  detail::durations.sort(
      it.begin(), it.end(),
      [](const auto& left, const auto& right) {
        return left.second >= right.second;
      },
      true);

  // now print all durations, iterating through the tree
  for (; it != detail::durations.end(); it++) {
    std::string label;
    // add label indentation denoting the nesting level
    for (int i {0}; i < detail::Duration_tree::depth(it); ++i)
      label += " ";
    // add separator and print
    label += to_string(it->first) + ",";
    out << std::left << std::setw(label_column_width) << label << " ";
    // print duration
    out << std::setprecision(3) << std::scientific
        << it->second << std::setw(4) << "," << " ";
    // print relative duration as a percentage of the total
    out << std::fixed << std::setprecision(2) << std::setw(18) << std::internal
        << it->second / total * 100 << ", ";
    // print this task's and parent task's IDs
    out << std::setw(7) << std::right << static_cast<int>(it->first) << ", ";
    if (it == detail::durations.begin()) {
      out << std::setw(9) << std::right << static_cast<int>(it->first) << '\n';
    }
    else {
      out << std::setw(9) << std::right
          << static_cast<int>(detail::Duration_tree::parent(it)->first) << '\n';
    }
  }
}

#endif
