// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_BLOCKGEN_LEGACY_FORM_STRING_H
#define PEPPER_BLOCKGEN_LEGACY_FORM_STRING_H

#include <string>

namespace Form{
  std::string form_start =
    "*********************************\n"
    "* FORM version 3.2(Apr 16 2007) *\n"
    "*********************************\n"
    "\n"
    "#procedure colorbar(cf,ct,nc)\n"
    ".sort\n"
    "c `ct', `cf'; s `nc';\n"
    "i ctmpa, ctmpf1, ctmpf2;\n"
    "id `ct'(ctmpa?,ctmpf1?,ctmpf2?) = `ct'(ctmpa,ctmpf2,ctmpf1);\n"
    ".sort\n"
    "#endprocedure\n"
    "\n"
    "#procedure colortrace(cf,ct,nc)\n"
    ".sort\n"
    "c `ct', `cf', ctmpf(c); s `nc';\n"
    "i coli1, ..., coli3, colf1, ..., colf4, cola1, ..., cola3;\n"
    "#do j = 1,1\n"
    "  #$j = 1;\n"
    "  id once   `cf'(cola1?,cola2?,cola3?)\n"
    "          = -2*i_*`ct'(cola1,coli1,coli2)*\n"
    "            ( `ct'(cola2,coli2,coli3)*`ct'(cola3,coli3,coli1)\n"
    "             -`ct'(cola3,coli2,coli3)*`ct'(cola2,coli3,coli1));\n"
    "  sum coli1, coli2, coli3;\n"
    "  id   `ct'(cola1?,colf1?,colf2?)*`ct'(cola1?,colf3?,colf4?)\n"
    "     = 1/2*( ctmpf(colf1,colf4)*ctmpf(colf2,colf3)\n"
    "            -1/`nc'*ctmpf(colf1,colf2)*ctmpf(colf3,colf4));\n"
    "  id ctmpf(colf1?,colf3?)*ctmpf(colf3?,colf2?) = ctmpf(colf1,colf2);\n"
    "  id ctmpf(colf1?,colf3?)*ctmpf(colf2?,colf3?) = ctmpf(colf1,colf2);\n"
    "  id ctmpf(colf3?,colf1?)*ctmpf(colf3?,colf2?) = ctmpf(colf1,colf2);\n"
    "  id ctmpf(colf3?,colf1?)*ctmpf(colf2?,colf3?) = ctmpf(colf1,colf2);\n"
    "  id ctmpf(colf1?,colf2?)^2 = ctmpf(colf1,colf1);\n"
    "  id ctmpf(colf1?,colf1?) = `nc';\n"
    "  if ( count(`cf',1)>0 ) $j = 0;\n"
    "  ModuleOption minimum $j;\n"
    "  .sort\n"
    "  #redefine j \"`$j'\"\n"
    "#enddo\n"
    "#endprocedure\n"
    "\n"
    "* color structures\n"
    "\n"
    "c T, F(c);\n"
    "i i1, ..., i200, j1, ..., j200, a1, ..., a200, b1, ..., b200;\n";

  std::string form_start_leading_colour =
    "*********************************\n"
    "* FORM version 3.2(Apr 16 2007) *\n"
    "*********************************\n"
    "\n"
    "#procedure colorbar(cf,ct,nc)\n"
    ".sort\n"
    "c `ct', `cf'; s `nc';\n"
    "i ctmpa, ctmpf1, ctmpf2;\n"
    "id `ct'(ctmpa?,ctmpf1?,ctmpf2?) = `ct'(ctmpa,ctmpf2,ctmpf1);\n"
    ".sort\n"
    "#endprocedure\n"
    "\n"
    "#procedure colortrace(cf,ct,nc)\n"
    ".sort\n"
    "c `ct', `cf', ctmpf(c); s `nc';\n"
    "i coli1, ..., coli3, colf1, ..., colf4, cola1, ..., cola3;\n"
    "#do j = 1,1\n"
    "  #$j = 1;\n"
    "  id once   `cf'(cola1?,cola2?,cola3?)\n"
    "          = -2*i_*`ct'(cola1,coli1,coli2)*\n"
    "            ( `ct'(cola2,coli2,coli3)*`ct'(cola3,coli3,coli1)\n"
    "             -`ct'(cola3,coli2,coli3)*`ct'(cola2,coli3,coli1));\n"
    "  sum coli1, coli2, coli3;\n"
    "  id   `ct'(cola1?,colf1?,colf2?)*`ct'(cola1?,colf3?,colf4?)\n"
    "     = 1/2*( ctmpf(colf1,colf4)*ctmpf(colf2,colf3));\n"
    "  id ctmpf(colf1?,colf3?)*ctmpf(colf3?,colf2?) = ctmpf(colf1,colf2);\n"
    "  id ctmpf(colf1?,colf3?)*ctmpf(colf2?,colf3?) = ctmpf(colf1,colf2);\n"
    "  id ctmpf(colf3?,colf1?)*ctmpf(colf3?,colf2?) = ctmpf(colf1,colf2);\n"
    "  id ctmpf(colf3?,colf1?)*ctmpf(colf2?,colf3?) = ctmpf(colf1,colf2);\n"
    "  id ctmpf(colf1?,colf1?) = `nc';\n"
    "  if ( count(`cf',1)>0 ) $j = 0;\n"
    "  ModuleOption minimum $j;\n"
    "  .sort\n"
    "  #redefine j \"`$j'\"\n"
    "#enddo\n"
    "#endprocedure\n"
    "\n"
    "* color structures\n"
    "\n"
    "c T, F(c);\n"
    "i i1, ..., i200, j1, ..., j200, a1, ..., a200, b1, ..., b200;\n";

  std::string form_set_nc = "$NC=3;\n";

  std::string form_end1 =
    "#call colortrace(F,T,$NC)\n"
    ".sort\n"
    "print +f;\n"
    "Format C;\n";
  std::string form_end2 =
    "\n"
    ".end\n";
}

#endif
