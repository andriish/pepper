// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "color.h"
#include "../flavour_process.h"
#include "../print.h"

#include <stack>
#include <algorithm>

namespace Color {

  void printColorTerm(const ColorTerm& term){
    if (term.sign < 0)
      cout << "-";
    else
      cout << "+";
    for(unsigned int j(0); j<term.gens.size(); ++j){
      auto cg = term.gens[j];
      if(cg.a == 0) {
	cout << "Id(" << cg.i << "," << cg.j << ")";
      } else {
        if (cg.a > 0)
          cout << "T(a" << cg.a << ",";
        else
          cout << "T(i" << -cg.a << ",";
        if (cg.i != 0)
          cout << "i" << cg.i << ",";
        else
          cout << "_,";
        if (cg.j != 0)
          cout << "i" << cg.j << ")";
        else
          cout << "_)";
      }
      if(j<term.gens.size()-1) cout << "*";
    }
    cout <<"\n";
  }

  ColorFactor cf_from_base(ColorFactor base_cf,
			   std::vector<int> gluon_colors,
			   std::map<int,int> fermion_color) {
    ColorFactor cf;
    for(auto term : base_cf.terms){
      ColorTerm cp_new;
      for(auto cg : term.gens){
	ColorGen cg_new;
	if(cg.a < 0) cg_new.a = -(gluon_colors[-cg.a - 1] + 1);
	else cg_new.a = cg.a;
        if (fermion_color.empty()) {
          // gluon-only case
          if(cg.i != 0) cg_new.i = (gluon_colors[cg.i - 1] + 1);
          if(cg.j != 0) cg_new.j = (gluon_colors[cg.j - 1] + 1);
        } else {
          if(cg.i != 0) cg_new.i = fermion_color[ cg.i] + 1;
          if(cg.j != 0) cg_new.j = fermion_color[-cg.j] + 1;
        }
	cp_new.gens.push_back(cg_new);
      }
      cp_new.sign = term.sign;
      cf.terms.push_back(cp_new);
    }
    return cf;
  }

  std::vector<int> get_gluon_colors(const Flavour_process& proc, const std::vector<int>& permutation){
    std::vector<int> ret;
    for(int index : permutation)
      if(proc.particle_name(index) == "g") ret.push_back(index);
    return ret;
  }

  std::map<int, int> get_fermion_colors(const Flavour_process& proc, const std::vector<int>& permutation,
				  dyck_plus_gluons_array base_dyck){
    // ensures that that the fermions always get the same colour
    // independant of the dyck permutations
    // also ensure, that particles that *do not* have colour don't mess up the factors
    std::map<int, int> ret;
    int x {0};
    std::stack<int> letters;
    for(int i(0); i<proc.n_qcd(); ++i) {
      int letter = base_dyck[i];
      if (letter == GLUON)
        continue;
      if (letter > 0) {
        letter -= x;
        letters.push(letter);
      } else {
        letter = -letters.top();
        letters.pop();
      }
      ret.insert(std::pair<int,int>(letter,permutation[i]));
    }
    return ret;
  }


  ColorFactor cf_from_term(
      const std::vector<std::vector<std::vector<int>>>& terms,
      const std::vector<int>& term_signs,
      int k){
    ColorFactor ret;
    const int number_of_terms = terms.size();

    for (int j {0}; j < number_of_terms; ++j) {
      ColorTerm term;
      for (int i {0}; i < std::max(k, 1); ++i) {
	if (terms[j][i].empty()) {
	  // there is no insertion at all, so we emit an identity operator
	  ColorGen gen;
	  gen.a = 0;
	  gen.i = i+1;
	  gen.j = i+1;
	  term.gens.push_back(gen);
	} else {
	  for (unsigned int l {0}; l < terms[j][i].size(); ++l) {
	    ColorGen gen;
	    if (terms[j][i][l] < 0){ // gluon operator
	      if (k == 0)
		gen.a = terms[j][i][l]-1;
	      else
		gen.a = terms[j][i][l];
	    } else if (terms[j][i][l] > 0) { // quark operator
	      gen.a = terms[j][i][l] + 1;
	    } else { // identity operator
	      gen.a = 0;
	    }
	    if(terms[j][i].size() == 1){
	      gen.i = i+1;
	      gen.j = i+1;
	      term.gens.push_back(gen);
	      continue;
	    }
	    if(l == 0){
	      gen.i = i+1;
	    } else if(l == terms[j][i].size() - 1){
	      if (k == 0)
		gen.j = terms[j][i].size()+2;
	      else
		gen.j = i+1;
	    } else{
	      gen.i = 0;
	      gen.j = 0;
	    }
	    term.gens.push_back(gen);
	  }
	}
      }
      term.sign = term_signs[j];
      ret.terms.push_back(term);
    }
    return ret;
  }

  ColorFactor generate_color_factor(const dyck_plus_gluons_array& dyck_array,
                                    const int k_quarks, const int n){
    // first calculate levels and nestings for quarks and gluons (not the anti-quarks)
    // note that levels will be equal to the size of each nesting, so it's redundant
    const int n_inner_quark_pairs {std::max(0, k_quarks - 1)};
    const int n_inner_gluons {n - 2*std::max(1, k_quarks)};
    std::vector<int> levels(n_inner_quark_pairs + n_inner_gluons);
    std::vector< std::vector<int> > nestings(n_inner_quark_pairs + n_inner_gluons);
    for(int i(0); i<n_inner_quark_pairs + n_inner_gluons; ++i){
      nestings[i].resize(1);
      nestings[i][0] = 1;
    }

    int quark_or_gluon_index {0};
    int level {1};

    for(unsigned int ii(2); ii<dyck_array.size(); ++ii){
      const auto& letter = dyck_array[ii];
      if (letter == NO_PTCL)
	continue;
      if (letter != GLUON) {
        if (letter < 0) {
          --level;
          for (int i {quark_or_gluon_index};
               i < (k_quarks - 1) + n - 2 * k_quarks; ++i)
            nestings[i].pop_back();
          continue;
        }
        if (letter > 0) {
          for (int i {quark_or_gluon_index};
               i < (k_quarks - 1) + n - 2 * k_quarks; ++i)
            nestings[i].push_back(letter);
        }
      }
      levels[quark_or_gluon_index] = level;
      ++quark_or_gluon_index;
      if (letter == GLUON)
	continue;
      ++level;
    }

    // the number of terms will be equal to the product of the levels
    const int number_of_terms = std::accumulate(levels.begin(), levels.end(), 1, std::multiplies<int>());

    // populate terms with operators from quarks and gluons
    std::vector<std::vector<std::vector<int>>> terms(number_of_terms);
    int overall_sign = 1;
    if (k_quarks > 0 && (k_quarks - 1) % 2 == 1)
      overall_sign = -1;
    std::vector<int> term_signs(number_of_terms, overall_sign);
    for(int j(0); j<number_of_terms; ++j) terms[j].resize(std::max(k_quarks, 1));
    int block_size {number_of_terms};
    for (int i {2}, quark_counter {1}, gluon_counter {0}; i < n; ++i) {
      if (dyck_array[i] <= 0) {
	// anti-quarks and No_ptcl do not contribute operators
	continue;
      }
      if (dyck_array[i] <= TOP_QUARK) {
	// quark
	for (int j {0}; j < number_of_terms; ++j) {
          // first the quark itself gets its own T^i
	  terms[j][quark_counter].push_back(quark_counter);
	}
	const int number_of_subterms = nestings[quark_counter-1 + gluon_counter].size()-1;
	if (number_of_subterms == 0) {
	  quark_counter++;
	  continue;
	}
	int stride {block_size / number_of_subterms};
	int current_subterm {0};
	for (int j {0}; j < number_of_terms; ++j) {
	  const int target_quark = nestings[quark_counter-1 + gluon_counter][current_subterm]-1;
	  terms[j][target_quark].push_back(quark_counter);
	  if ((j + 1) % stride == 0) {
	    ++current_subterm;
	    current_subterm %= number_of_subterms;
	  }
	}
	block_size /= number_of_subterms;
	quark_counter++;
      } else {
	// gluon
	const int number_of_subterms = nestings[quark_counter-1 + gluon_counter].size();
	if (number_of_subterms == 0) {
	  gluon_counter++;
	  continue;
	}
	int stride {block_size / number_of_subterms};
	int current_subterm {0};
	for (int j {0}; j < number_of_terms; ++j) {
	  const int target_quark = nestings[quark_counter-1 + gluon_counter][current_subterm]-1;
	  terms[j][target_quark].push_back(-(gluon_counter + 1));
	  if ((j + 1) % stride == 0) {
	    ++current_subterm;
	    current_subterm %= number_of_subterms;
	  }
	}
	block_size /= number_of_subterms;
	gluon_counter++;
      }
    }

    if (k_quarks > 0) {
      // apply special treatment for primary quark line
      // - the ordering of the operators must be reverted
      // - a factor of (-1)^(number of operators) is applied
      for (int i {0}; i < number_of_terms; ++i) {
        std::reverse(terms[i][0].begin(), terms[i][0].end());
        if (terms[i][0].size() % 2 == 1)
          term_signs[i] *= -1;
          //term_signs[i] *= 1;
      }
    }

    return cf_from_term(terms,term_signs,k_quarks);
  }

  string FormString(const ColorFactor& cf1, const ColorFactor& cf2, bool gluon_only){
    // create string to be used in FORM to compute color coefficients
    //
    // every external color gets an i-index
    // internal gluon colors get an a-index (if !gluon_only)
    // internal fermion indices get j-index (if !gluon_only)
    // internal indices get a j-index (if gluon_only)
    //

    string ret = "";
    const int nf1 = cf1.terms.size();
    const int nf2 = cf2.terms.size();

    ret += "(";
    int amax = 0;
    int jct = 0;
    for(int i(0); i<nf1; ++i){
      if (cf1.terms[i].sign < 0)
        ret += "(-1)*";
      for(unsigned int j(0); j < cf1.terms[i].gens.size(); ++j){

	// new color factor
	ColorGen cg = cf1.terms[i].gens[j];

        if (cg.a == 0) {

	  // first check for a trivial factor (i.e. <q|Id|q>), and sum them out
	  // here (an alternative would be to add an Identity matrix to the
	  // FORM script, but I (EB) do not know how to do that)
          ret += "3";

        } else {

        if (gluon_only)
          ret += "F(";
        else
          ret += "T(";

        // Gluon color index
        if (cg.a < 0)
          ret += "i" + to_string(-cg.a) + ","; // external gluon
        else if (cg.a > 0)
          ret += "a" + to_string(cg.a) + ","; // internal gluon
        else
          std::cerr << "Something went wrong in the FormString generation\n";
        amax = (cg.a > amax) ? cg.a : amax;

        // 1st Fermion indices
        if (cg.i == 0)
          ret += "j" + to_string(jct) + ",";
        else if (cg.i > 0)
          ret += "i" + to_string(cg.i) + ",";
        else
          std::cerr << "Something went wrong in the FormString generattion\n";

        // 2nd Fermion indices
        if (cg.j == 0)
          ret += "j" + to_string(++jct) + ")";
        else if (cg.j > 0)
          ret += "i" + to_string(cg.j) + ")";
        else
          std::cerr << "Something went wrong in the FormString generattion\n";
        }

	// end factor and multiply
        if (j < cf1.terms[i].gens.size() - 1)
          ret += '*';
      }
      if(i<nf1-1) ret += " + ";
    }
    ret += ") * (";
    jct++;
    for(int i(0); i<nf2; ++i){
      if (cf2.terms[i].sign < 0)
        ret += "(-1)*";
      for(unsigned int j(0); j < cf2.terms[i].gens.size(); ++j){

	// new color factor
	ColorGen cg = cf2.terms[i].gens[j];

        if (cg.a == 0) {

	  // we have already evaluated trivial factors as a 3 above
          ret += "1";

        } else {
	  // h.c. of T and F matrices gives a minus sign
          ret += "(-1)*";

          // new color factor
          if (gluon_only)
            ret += "F(";
          else
            ret += "T(";

          // Gluon color index
          if (cg.a < 0)
            ret += "i" + to_string(-cg.a) + ","; // external gluon
          else if (cg.a > 0)
            ret += "a" + to_string(cg.a + amax) + ","; // internal gluon
          else
            std::cerr << "Something went wrong in the FormString generation\n";

          std::string ij1, ij2;

          // 1st Fermion indices
          if (cg.i == 0)
            ij1 = "j" + to_string(jct) + ")";
          else if (cg.i > 0)
            ij1 = "i" + to_string(cg.i) + ")";
          else
            std::cerr << "Something went wrong in the FormString generattion\n";

          // 2nd Fermion indices
          if (cg.j == 0)
            ij2 = "j" + to_string(++jct) + ",";
          else if (cg.j > 0)
            ij2 = "i" + to_string(cg.j) + ",";
          else
            std::cerr << "Something went wrong in the FormString generattion\n";

          ret += ij2;
          ret += ij1;
        }

        // end factor and multiply
        if (j < cf2.terms[i].gens.size() - 1)
          ret += '*';
      }
      if(i<nf2-1) ret += " + ";
    }
    ret += ")";
    return ret;
  }


}
