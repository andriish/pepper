// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_BLOCKGEN_LEGACY_COLOR_H
#define PEPPER_BLOCKGEN_LEGACY_COLOR_H

#include <iostream>
#include <fstream>
#include <numeric>
#include <map>
#include <string>
#include <vector>

//#include "process.h"
//#include "dyck.h"
//#include "settings.h"
//#include "combinatorics.h"
//#include "message.h"
//using namespace Dyck;

using std::string;
using std::to_string;
using std::cout;

class Flavour_process;

namespace Color{

  using dyck_plus_gluons_array = std::vector<int>;

  struct ColorGen{
    int a = 0;
    int i = 0;
    int j = 0;
  };

  struct ColorTerm{
    std::vector<ColorGen> gens;
    int sign = 1;
  };

  struct ColorFactor {
    std::vector<ColorTerm> terms;
  };

  // mostly helper functions to compute color-factors and write out to form
  void printColorTerm(const ColorTerm& term);
  ColorFactor cf_from_base(ColorFactor base_cf,
			   std::vector<int> gluon_colors,
			   std::map<int,int> fermion_color);
  std::vector<int> get_gluon_colors(const Flavour_process& proc, const std::vector<int>& permutation);
  std::map<int, int> get_fermion_colors(const Flavour_process& proc, const std::vector<int>& permutation,
                                        dyck_plus_gluons_array base_dyck);
  ColorFactor cf_from_term(const std::vector<std::vector<std::vector<int>>>& terms,
                           const std::vector<int>& term_signs,
                           int k);
  ColorFactor generate_color_factor(const dyck_plus_gluons_array& dyck_array,
                                    const int k_quarks, const int n);
  string FormString(const ColorFactor& cf1, const ColorFactor& cf2, bool gluon_only);

}

#endif
