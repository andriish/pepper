// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "progress.h"

using namespace Progress;

std::tuple<Time_unit, Time_unit>
Estimator::elapsed_and_left(int n_steps_done) const
{
  const Time_unit _elapsed {elapsed()};
  if (n_steps_done > n_steps_total)
    return {_elapsed, Unknown_duration};
  double remaining_ms {static_cast<double>(_elapsed.count())};
  if (growth_factor == 1.0) {
    remaining_ms *= double(n_steps_total) / n_steps_done - 1.0;
  }
  else {
    double numerator {0.0};
    for (int i {n_steps_done}; i < n_steps_total; i++)
      numerator += std::pow(growth_factor, i);
    double denominator {1.0};
    for (int i {1}; i < n_steps_done; i++)
      denominator += std::pow(growth_factor, i);
    remaining_ms *= numerator / denominator;
  }
  return {_elapsed, Time_unit {std::llround(remaining_ms)}};
}
