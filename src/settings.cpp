// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023-2024 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "settings.h"

#include "pepper/config.h"

#include "argument_parser.h"
#include "paths.h"
#include "system.h"
#include "utilities.h"
#include <cassert>
#include <iostream>
#include <mini/ini.h>
#include <string>

namespace fs = std::filesystem;

void print_help_and_exit_with_error(const Argument_parser& parser)
{
  std::cerr << parser;
  std::exit(1);
}

bool stob(const std::string& s)
{
  if (case_insensitive_equal(s, "false") || case_insensitive_equal(s, "no") ||
      case_insensitive_equal(s, "none") || s == "0") {
    return false;
  }
  return true;
}

Settings::Settings()
{
  data_path = fs::path {PEPPER_INSTALL_DATADIR} / "pepper_data";
  if (!fs::exists(fs::path {PEPPER_INSTALL_DATADIR} / "pepper_data")) {
    data_path = fs::path {PEPPER_BUILD_DATADIR};
  }
  data_path = get_environment_variable("PEPPER_DATA_PATH", data_path);
  device_id = std::stoi(get_environment_variable("PEPPER_DEVICE_ID", "0"));
}

void Settings::read(int argc, char** argv, const std::string& argprefix)
{
  // The parsing logic is as follows:
  // 1. Read positional cmd line argument to possibly override the ini file path.
  // 2. Write ini file contents into the ini structure.
  // 3. Read optional cmd line arguments and if present, override the
  //    corresponding ini structure fields.
  // 4. Finally we can parse the string-typed ini structure fields into typed
  //    Setting data members.
  // Note that we ignore ArgumentParser's typed parsing capabilities on
  // purpose, because we anyway need to parse string-typed ini structure
  // fields.

  // To add a command line argument, do the following steps:
  // 1. Call `parser.add_argument(...)`.
  // 2. Check if it's available and if yes, write its value into
  //    `ini_structure`, choosing an appropriate section and key within
  //    that section.
  // 3. Extend the parsing logic for `ini_structure` to treat the new key,
  //    probably writing it into a member variable of Settings for later
  //    usage.
  // 4. Update documentation (manpages, manual, ...)

  const std::string program_name {fs::path(argv[0]).filename()};
  Argument_parser parser(program_name, argprefix);

  parser.add_runcard_path_argument(ini_path);

  // declare main options
  parser.add_argument("-p", "--process")
      .help("select process");
  parser.add_argument("-b", "--batch-size")
      .help("set event batch size");
  parser.add_argument("-n", "--n-batches")
      .help("set number of event batches");
  parser.add_argument("-q", "--quiet")
      .help("use minimum output verbosity")
      .implicit_value(true);
  parser.add_argument("-V", "--verbose")
      .help("use high output verbosity")
      .implicit_value(true);
  parser.add_argument("-d", "--debug")
      .help("use maximum output verbosity")
      .implicit_value(true);
  parser.add_argument("--no-color")
      .help("never use colored output")
      .implicit_value(true);
  parser.add_argument("--color")
      .help("always use colored output")
      .implicit_value(true);
  parser.add_argument("-D", "--device")
      .help("select GPU device ID");
  parser.add_argument("-s", "--seed")
      .help("select random generator seed (offset)");
  parser.add_argument("-e", "--collision-energy")
      .help("set collision energy (GeV)");
  parser.add_argument("-S", "--helicity-summing")
      .help("enable helicity summing")
      .implicit_value(true);
  parser.add_argument("-O", "--helicity-summing-options")
      .help("configure helicity summing");

  // declare events options
  parser.add_argument("-Q", "--output-disabled")
      .help("disable event output")
      .implicit_value(true);
  parser.add_argument("-l", "--lhef")
      .help("use LHEF instead of HepMC3 output")
      .implicit_value(true);
  parser.add_argument("-o", "--output")
      .help("set event output path");
  parser.add_argument("-z", "--zero-weight-output-disabled")
      .help("disable zero-weight event output")
      .implicit_value(true);
  parser.add_argument("-w", "--unweighting-disabled")
      .help("disable event unweighting")
      .implicit_value(true);
  parser.add_argument("-c", "--leading-colour-flow-output-disabled")
      .help("disable leading colour flow output")
      .implicit_value(true);

  // declare phase space options
  parser.add_argument("-P", "--psi")
      .help("select phase space integrator");

  // declare dev options
  parser.add_argument("-g", "--diagnostic-output-enabled")
      .help("enable diagnostic output")
      .implicit_value(true);
  parser.add_argument("--me2-only")
      .help("integrate over |ME|^2 only")
      .implicit_value(true);
  parser.add_argument("--dummy-color-factors-allowed")
      .help("set colour factors to `1' if no colour factor file is found");
  parser.add_argument("--dummy-averaging-factors-enabled")
      .help("set initial state averaging factors to `1'");
  parser.add_argument("--dummy-symmetry-factors-enabled")
      .help("set initial and final state symmetry factors to `1'");
  parser.add_argument("--fixed-helicities")
      .help("select a fixed helicity configuration");

  // parse command line arguments
  try {
    parser.parse_args(argc, argv);
  }
  catch (const std::runtime_error& err) {
    ERROR(err.what());
    print_help_and_exit_with_error(parser);
    std::cerr << parser;
    std::exit(1);
  }

  ini_path = parser.runcard_path();
  if (ini_path == "-")
    ini_path = "";
  if (ini_path != "") {
    if (fs::exists(ini_path)) {
      ini_file = std::make_shared<mINI::INIFile>(ini_path);
    }
    else if (parser.runcard_path_is_used()) {
      ERROR("There is not file at the given runcard path `", ini_path, "'.");
      std::exit(1);
    }
    else {
      ini_path = "";
    }
  }

  // load runcard
  if (ini_file)
    ini_file->read(ini_structure);

  // write main options to ini structure
  if (auto fn = parser.present("--process")) {
    ini_structure["main"]["process"] = *fn;
  }
  if (auto fn = parser.present("--batch-size")) {
    ini_structure["main"]["batch_size"] = *fn;
    ini_structure["main.cpu"].remove("batch_size");
    ini_structure["main.gpu"].remove("batch_size");
  }
  if (auto fn = parser.present("--n-batches")) {
    ini_structure["main"]["n_batches"] = *fn;
  }
  int n_verbosity_flags {0};
  if (parser.is_used("--quiet")) {
    n_verbosity_flags++;
    ini_structure["main"]["verbosity"] = "warn";
  }
  if (parser.is_used("--verbose")) {
    n_verbosity_flags++;
    ini_structure["main"]["verbosity"] = "verbose";
  }
  if (parser.is_used("--debug")) {
    n_verbosity_flags++;
    ini_structure["main"]["verbosity"] = "debug";
  }
  if (n_verbosity_flags > 1) {
    ERROR("Only one of --quiet|-q, --verbose|-v, or --debug|-d may be used.");
    print_help_and_exit_with_error(parser);
  }
  int n_colour_flags {0};
  if (parser.is_used("--no-color")) {
    n_colour_flags++;
    ini_structure["main"]["color"] = "never";
  }
  if (parser.is_used("--color")) {
    n_colour_flags++;
    ini_structure["main"]["color"] = "always";
  }
  if (n_colour_flags > 1) {
    ERROR("Only one of --color or --no-color may be used.");
    print_help_and_exit_with_error(parser);
  }
  if (auto fn = parser.present("--device")) {
    ini_structure["main"]["device"] = *fn;
  }
  if (auto fn = parser.present("--seed")) {
    ini_structure["main"]["seed"] = *fn;
  }
  if (auto fn = parser.present("--collision-energy")) {
    ini_structure["main"]["collision_energy"] = *fn;
  }
  if (parser.is_used("--helicity-summing")) {
    ini_structure["main"]["helicity_summing"] = "true";
  }
  if (auto fn = parser.present("--helicity-summing-options")) {
    ini_structure["main"]["helicity_summing_options"] = *fn;
  }
  if (auto fn = parser.present("--fixed-helicities")) {
    ini_structure["dev"]["fixed_helicities"] = *fn;
  };

  // write events options to ini structure
  if (auto fn = parser.present("--output")) {
    ini_structure["events"]["output_path"] = *fn;
  }
  if (parser.is_used("--zero-weight-output-disabled")) {
    ini_structure["events"]["zero_weight_output_disabled"] = "true";
  }
  if (parser.is_used("--unweighting-disabled")) {
    ini_structure["events"]["unweighting_disabled"] = "true";
  }
  if (parser.is_used("--output-disabled")) {
    ini_structure["events"]["output_disabled"] = "true";
  }
  if (parser.is_used("--lhef")) {
    ini_structure["events"]["lhef"] = "true";
  }
  if (parser.is_used("--leading-colour-flow-output-disabled")) {
    ini_structure["events"]["leading_colour_flow_output_enabled"] = "false";
  }

  // write phase space options to ini structure
  if (auto fn = parser.present("--psi")) {
    ini_structure["phase_space"]["integrator"] = *fn;
  };

  // write dev options to ini structure
  if (parser.is_used("--diagnostic-output-enabled")) {
    ini_structure["dev"]["diagnostic_output_enabled"] = "true";
  };
  if (parser.is_used("--me2-only")) {
    ini_structure["dev"]["me2_only"] = "true";
  };
  if (parser.is_used("--dummy-color-factors-allowed")) {
    ini_structure["dev"]["dummy_color_factors_allowed"] = "true";
  };
  if (parser.is_used("--dummy-averaging-factors-enabled")) {
    ini_structure["dev"]["dummy_averaging_factors_enabled"] = "true";
  };
  if (parser.is_used("--dummy-symmetry-factors-enabled")) {
    ini_structure["dev"]["dummy_symmetry_factors_enabled"] = "true";
  };

  // Before iterating over the sections in the runcard, we sort them. The
  // reason is that we want to handle a subsection a.b after handling its main
  // section a. That way, settings in the subsection always take precedence,
  // regardless of the order in the file.
  std::vector<std::string> sections;
  for (const auto& [section, collection] : ini_structure) {
    sections.push_back(section);
  }
  std::sort(sections.begin(), sections.end());

  // now iterate over the sorted runcard sections
  for (const auto& section : sections) {
    //println("[", section, "]");

    for (auto const& [key, value] : ini_structure[section]) {
    // println(key, " = ", value);

      if (section == "main") {
        if (key == "process" || key == "process_file") {
          const auto specs = split(value, ',');
          std::copy(specs.begin(), specs.end(),
                    std::back_inserter(process_specs));
        }
        else if (key == "batch_size")
          batch_size = std::stoi(value);
        else if (key == "n_batches")
          n_batches = std::stoi(value);
        else if (key == "verbosity")
          msg_verbosity = Msg::to_verbosity(value);
        else if (key == "color")
          msg_colourfulness = Msg::to_colourfulness(value);
        else if (key == "device")
          device_id = std::stoi(value);
        else if (key == "seed")
          seed_offset = std::stoi(value);
        else if (key == "collision_energy") {
          e_cms = std::stod(value);
          if (e_cms <= 0.0) {
            ERROR("The collision energy must be a positive number.");
            std::exit(1);
          }
          cuts.e_cms = e_cms;
        }
        else if (key == "helicity_summing") {
          if(stob(value))
            helicity_settings.treatment = Helicity_treatment::summed;
          else
            helicity_settings.treatment = Helicity_treatment::sampled;
        }
        else if (key == "helicity_integrator") {
          // TODO: should probably move this to helicity settings
          phase_space_settings.helicity_integrator_enabled = stob(value);
        }
        else if (key == "helicity_summing_options") {
          helicity_settings.summing_options =
              to_helicity_summing_options(value);
        }
        else if (key == "fixed_helicities" && stob(value)) {
          helicity_settings.fixed_helicities = value;
          helicity_settings.treatment = Helicity_treatment::fixed;
        }
        else if (key == "mu2") {
          scale2_spec = value;
        }
        else if (key == "fixed_alpha_s") {
          fixed_alpha_s = std::stod(value);
        }
        else if (key == "max_overweight_xs_contribution") {
          max_eps = std::stod(value);
        }
      }

      else if (section == "events") {
        if (key == "output_disabled")
          event_output_disabled = stob(value);
        else if (key == "lhef")
          lhef_event_output_enabled = stob(value);
        else if (key == "output_path" && stob(value))
          event_output_path = value;
        else if (key == "zero_weight_output_disabled")
          zero_weight_event_output_enabled = !stob(value);
        else if (key == "unweighting_disabled")
          unweighting_disabled = stob(value);
        else if (key == "leading_colour_flow_output_enabled")
          leading_colour_flow_output_enabled = stob(value);
      }

      else if (section == "pdf") {
        if (key == "set") {
          if (!stob(value))
            pdf_disabled = true;
          else
            pdfset = value;
        }
        if (key == "member")
          pdfmember = std::stoi(value);
      }

      else if (section == "cuts") {
        if (key == "jets.pt_min" || key == "jets.pT_min")
          cuts.pT_min = std::stod(value);
        else if (key == "jets.nu_max")
          cuts.nu_max = std::stod(value);
        else if (key == "jets.dR_min" || key == "jets.dr_min")
          cuts.dR_min = std::stod(value);
        else if (key == "ll.m_min")
          cuts.ll_m2_min = square(std::stod(value));
        else if (key == "ll.m_max")
          cuts.ll_m2_max = square(std::stod(value));
      }

      else if (section == "phase_space") {
        if (key == "integrator" || key == "points_file")
          phase_space_settings.generator = value;
        else if (key == "write_cached_results")
          phase_space_settings.write_cached_results = stob(value);
        else if (key == "cache_path")
          phase_space_settings.cache_path = value;
        else if (key == "use_cached_results")
          phase_space_settings.use_cached_results_if_possible = stob(value);
        else if (key == "integration_info_output_enabled")
          phase_space_settings.integration_info_output_enabled = stob(value);
	else if (key == "enhance_function")
          phase_space_settings.bias = value;
      }

      else if (section == "phase_space.chili") {
        if (key == "n_s_channels")
          phase_space_settings.generator_settings.n_s_channels =
              std::stoi(value);
        else if (key == "start_vegas_bins")
          phase_space_settings.generator_settings.start_vegas_bins =
              std::stoi(value);
        else if (key == "max_vegas_bins")
          phase_space_settings.generator_settings.max_vegas_bins =
              std::stoi(value);
	else if (key == "beta")
          phase_space_settings.generator_settings.beta = std::stod(value);
	else if (key == "should_optimize")
	  phase_space_settings.generator_settings.should_optimize
	    = stob(value);

      }

      else if (section == "phase_space.optimisation") {
        if (key == "n_iter")
          phase_space_settings.optimisation.n_iter = std::stoi(value);
        else if (key == "n_nonzero_min_growth_factor")
          phase_space_settings.optimisation.n_nonzero_min_growth_factor =
              std::stod(value);
        else if (key == "n_nonzero_min")
          phase_space_settings.optimisation.n_nonzero_min = std::stoi(value);
      }

      else if (section == "phase_space.integration") {
        if (key == "n_iter")
          phase_space_settings.integration.n_iter = std::stoi(value);
        else if (key == "n_nonzero_min_growth_factor")
          phase_space_settings.integration.n_nonzero_min_growth_factor =
              std::stod(value);
        else if (key == "n_nonzero_min")
          phase_space_settings.integration.n_nonzero_min = std::stoi(value);
      }

      else if (section == "dev") {
        if (key == "me2_only")
          me2_only = stob(value);
        else if (key == "dummy_color_factors_allowed")
          dummy_colour_factors_allowed = stob(value);
        else if (key == "dummy_averaging_factors_enabled")
          dummy_averaging_factors_enabled = stob(value);
        else if (key == "dummy_symmetry_factors_enabled")
          dummy_symmetry_factors_enabled = stob(value);
        else if (key == "diagnostic_output_enabled")
          diagnostic_output_enabled = stob(value);
        else if (key == "diagnostic_path")
          diagnostic_path = value;
      }
    }
  }

  if (process_specs.empty()) {
    std::stringstream epilog;
    epilog << "A process must either be specified using `"
           << parser.prefixed_argument("--process")
           << " <process>` or using the\n"
           << "`process = <process>` setting in the `[main]` "
           << "section of a runcard.";
    if (argprefix.empty()) {
      epilog << "\n\nDijet production example:\n\n"
             << "    " << program_name << " -p ppjj\n\n"
             << "For a listing of options, use " << program_name
             << " --help.";
    }
    // generate a dummy parser to print only the most basic usage
    Argument_parser dummy_parser {program_name, argprefix};
    dummy_parser.add_runcard_path_argument(ini_path);
    std::stringstream desc;
    std::string current_type {argprefix.empty() ? "program" : "library"};
    desc << PROJECT_NAME
         << " is a portable " + current_type +
                " that generates parton-level events\n"
                "for a process at a high-energy physics collider such as "
                "the LHC.";
    dummy_parser.add_description(desc.str());
    dummy_parser.add_epilog(epilog.str());
    std::cerr << dummy_parser;
    std::exit(1);
  }
}

void Settings::write_resolved_settings()
{
  // write ini file with all resolved settings (i.e. with overrides from the cmd
  // line applied)
  if (ini_file) {
    fs::path ini_filename {ini_path.filename()};

    fs::path tmp_path {fs::path{Paths::diagnostics}};
    tmp_path /= ini_filename;
    tmp_path += ".original";

    fs::path storage_path {fs::path{Paths::diagnostics}};
    storage_path /= "resolved.ini";

    fs::copy(ini_path, tmp_path, fs::copy_options::update_existing);

    ini_file->write(ini_structure, true);

    fs::copy(ini_path, storage_path, fs::copy_options::update_existing);
    // fs::rename(tmp_path, ini_path);
  }
}
