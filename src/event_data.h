// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_HOST_EVENT_DATA_H
#define PEPPER_HOST_EVENT_DATA_H

#include "pepper/config.h"

#include "helicity.h"
#include "math.h"
#include "sm.h"
#include "vec4.h"
#include "cvec4.h"
#include "Kokkos_Vector.hpp"
#include <vector>
#include <type_traits>

template <typename MemSpace> 
class Event_data {

public:
  Event_data():
      n_active_helicities("n_active_helicities"),
      w(Kokkos::ViewAllocateWithoutInitializing("w"),0),
      n_overweight("n_overweight"),
      max_overweight(Kokkos::ViewAllocateWithoutInitializing("max_overweight")),
      _r(Kokkos::ViewAllocateWithoutInitializing("r"),0),

      e(Kokkos::ViewAllocateWithoutInitializing("e"),0,0),
      px(Kokkos::ViewAllocateWithoutInitializing("px"),0,0),
      py(Kokkos::ViewAllocateWithoutInitializing("py"),0,0),
      pz(Kokkos::ViewAllocateWithoutInitializing("pz"),0,0),

      x1(Kokkos::ViewAllocateWithoutInitializing("x1"),0),
      x2(Kokkos::ViewAllocateWithoutInitializing("x2"),0),

      mu2(Kokkos::ViewAllocateWithoutInitializing("mu2"),0),
      ps_bias(Kokkos::ViewAllocateWithoutInitializing("ps_bias"),0),
      alpha_s(Kokkos::ViewAllocateWithoutInitializing("alpha_s"),0),

      y(Kokkos::ViewAllocateWithoutInitializing("y"),0,0),
      phi(Kokkos::ViewAllocateWithoutInitializing("phi"),0,0),

      _c0(Kokkos::ViewAllocateWithoutInitializing("c0"),0,0),
      _c1(Kokkos::ViewAllocateWithoutInitializing("c1"),0,0),
      _c2(Kokkos::ViewAllocateWithoutInitializing("c2"),0,0),
      _c3(Kokkos::ViewAllocateWithoutInitializing("c3"),0,0),

      ptcl(Kokkos::ViewAllocateWithoutInitializing("ptcl"),0),
      hels(Kokkos::ViewAllocateWithoutInitializing("hels"),0),
      hel_conf_idx(Kokkos::ViewAllocateWithoutInitializing("hel_conf_idx"),0),
      active_hel_conf_idx(Kokkos::ViewAllocateWithoutInitializing("active_hel_conf_idx"),0),
      active_hels(Kokkos::ViewAllocateWithoutInitializing("active_hels"),0),
      hel_selection_weights(Kokkos::ViewAllocateWithoutInitializing("hel_selection_weights"),0),

      me_real(Kokkos::ViewAllocateWithoutInitializing("me_real"),0,0),
      me_imag(Kokkos::ViewAllocateWithoutInitializing("me_imag"),0,0),
      me2(Kokkos::ViewAllocateWithoutInitializing("me2"),0),
      flav_channels(Kokkos::ViewAllocateWithoutInitializing("flav_channels"),0),
      active_lc_conf_idx(Kokkos::ViewAllocateWithoutInitializing("active_lc_conf_idx"),0)

  {
    Kokkos::deep_copy(max_overweight,1.0);
  };

  // copy constructor for like-memory spaces
  template<typename T,
          typename std::enable_if<std::is_same<MemSpace,T>::value,T>::type* = nullptr>
  Event_data(const Event_data<T>& lhs):
      batch_size(lhs.batch_size),
      n_phase_space_random_numbers(lhs.n_phase_space_random_numbers),
      n_helicity_blocks(lhs.n_helicity_blocks),
      n_active_helicities(Kokkos::create_mirror_view(lhs.n_active_helicities)),
      n_ptcl(lhs.n_ptcl),
      n_perm(lhs.n_perm),
      n_internal_states(lhs.n_internal_states),
      n_external_states(lhs.n_external_states),
      n_external_helicity_states(lhs.n_external_helicity_states),
      n_me(lhs.n_me),
      
      w(Kokkos::create_mirror_view(lhs.w)),
      n_overweight(Kokkos::create_mirror_view(lhs.n_overweight)),
      max_overweight(Kokkos::create_mirror_view(lhs.max_overweight)),
      _r(Kokkos::create_mirror_view(lhs._r)),

      e(Kokkos::create_mirror_view(lhs.e)),
      px(Kokkos::create_mirror_view(lhs.px)),
      py(Kokkos::create_mirror_view(lhs.py)),
      pz(Kokkos::create_mirror_view(lhs.pz)),

      x1(Kokkos::create_mirror_view(lhs.x1)),
      x2(Kokkos::create_mirror_view(lhs.x2)),

      mu2(Kokkos::create_mirror_view(lhs.mu2)),
      ps_bias(Kokkos::create_mirror_view(lhs.ps_bias)),
      alpha_s(Kokkos::create_mirror_view(alpha_s)),

      y(Kokkos::create_mirror_view(lhs.y)),
      phi(Kokkos::create_mirror_view(lhs.phi)),

      _c0(Kokkos::create_mirror_view(lhs.c0)),
      _c1(Kokkos::create_mirror_view(lhs.c1)),
      _c2(Kokkos::create_mirror_view(lhs.c2)),
      _c3(Kokkos::create_mirror_view(lhs.c3)),

      ptcl(Kokkos::create_mirror_view(lhs.ptcl)),
      hels(Kokkos::create_mirror_view(lhs.hels)),
      hel_conf_idx(Kokkos::create_mirror_view(lhs.hel_conf_idx)),
      active_hel_conf_idx(Kokkos::create_mirror_view(lhs.active_hel_conf_idx)),
      active_hels(Kokkos::create_mirror_view(lhs.active_hels)),
      hel_selection_weights(Kokkos::create_mirror_view(lhs.hel_selection_weights)),

      me_real(Kokkos::create_mirror_view(lhs.me_real)),
      me_imag(Kokkos::create_mirror_view(lhs.me_imag)),
      me2(Kokkos::create_mirror_view(lhs.me2)),
      flav_channels(Kokkos::create_mirror_view(lhs.flav_channels)),
      active_lc_conf_idx(Kokkos::create_mirror_view(lhs.active_lc_conf_idx))
      {};

  // copy constructor for unlike-memory spaces
  template<typename T,
          typename std::enable_if<!std::is_same<MemSpace,T>::value,T>::type* = nullptr>
  Event_data(const Event_data<T>& lhs):
      batch_size(lhs.batch_size),
      n_phase_space_random_numbers(lhs.n_phase_space_random_numbers),
      n_helicity_blocks(lhs.n_helicity_blocks),
      n_active_helicities(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.n_active_helicities)),
      n_ptcl(lhs.n_ptcl),
      n_perm(lhs.n_perm),
      n_internal_states(lhs.n_internal_states),
      n_external_states(lhs.n_external_states),
      n_external_helicity_states(lhs.n_external_helicity_states),
      n_me(lhs.n_me),
      w(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.w)),
      n_overweight("n_overweight"),
      max_overweight(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.max_overweight)),
      _r(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs._r)),

      e(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.e)),
      px(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.px)),
      py(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.py)),
      pz(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.pz)),

      x1(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.x1)),
      x2(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.x2)),

      xfx1(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.xfx1)),
      xfx2(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.xfx2)),

      mu2(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.mu2)),
      ps_bias(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.ps_bias)),
      alpha_s(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.alpha_s)),

      y(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.y)),
      phi(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.phi)),

      _c0(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs._c0)),
      _c1(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs._c1)),
      _c2(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs._c2)),
      _c3(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs._c3)),

      ptcl(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.ptcl)),
      hels(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.hels)),
      hel_conf_idx(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.hel_conf_idx)),
      active_hel_conf_idx(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.active_hel_conf_idx)),
      active_hels(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.active_hels)),
      hel_selection_weights(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.hel_selection_weights)),

      me_real(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.me_real)),
      me_imag(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.me_imag)),
      me2(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.me2)),
      flav_channels(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.flav_channels)),
      active_lc_conf_idx(Kokkos::create_mirror_view_and_copy(MemSpace(),lhs.active_lc_conf_idx)) {};

  int batch_size;
  int n_phase_space_random_numbers;
  int n_helicity_blocks;
  Kokkos::View<int,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> n_active_helicities;
  int n_ptcl;
  int n_perm;
  int n_internal_states;
  int n_external_states;
  int n_external_helicity_states;
  int n_me;

  KOKKOS_INLINE_FUNCTION int auxiliary_photon_current_idx() const { return n_internal_states - 1; }

  // Weights
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> w;
  Kokkos::View<int,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> n_overweight;
  Kokkos::View<double,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> max_overweight;

  // Random numbers
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> _r;

  // Momenta
  Kokkos::View<double**,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> e;
  Kokkos::View<double**,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> px;
  Kokkos::View<double**,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> py;
  Kokkos::View<double**,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> pz;

  // Longitudinal momentum fractions and x_i f(x_i, Q^2) products
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> x1;
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> x2;
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> xfx1;
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> xfx2;

  // Scale factors
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> mu2;

  // Phase space biasing factors
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> ps_bias;

  // Dynamic coupling factors
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> alpha_s;
  // Rapidities and azimuthal angles of final-state particles
  Kokkos::View<double**,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> y;
  Kokkos::View<double**,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> phi;

  // Currents
  Kokkos::View<C**,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> _c0;
  Kokkos::View<C**,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> _c1;
  Kokkos::View<C**,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> _c2;
  Kokkos::View<C**,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> _c3;

  // Particle types and helicities
  Kokkos::View<Ptcl_num*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> ptcl;
  Kokkos::View<Helicity*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> hels;
  Kokkos::View<int*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> hel_conf_idx;
  Kokkos::View<int*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> active_hel_conf_idx;
  Kokkos::View<int*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> active_hels;
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> hel_selection_weights;

  // Matrix elements
  Kokkos::View<double**,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> me_real;
  Kokkos::View<double**,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> me_imag;
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> me2;

  // Flavour channels
  Kokkos::View<int*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> flav_channels;

  // Active leading colour configurations
  Kokkos::View<int*,Kokkos::DefaultExecutionSpace::array_layout,MemSpace> active_lc_conf_idx;

  KOKKOS_INLINE_FUNCTION void set_r(const int& evt,const int& i,const double& v) const { _r[i * batch_size + evt] = v; }
  KOKKOS_INLINE_FUNCTION double r(const int& evt, const int& i) const { return _r[i * batch_size + evt]; }

  // Convenience methods to pick the appropriate random number for each purpose.
  KOKKOS_INLINE_FUNCTION double r_rambo(const int& evt, const int& p, const int& c) const 
  {
    return r(evt, (p - 2) * 4 + c);
  }
  KOKKOS_INLINE_FUNCTION double r_unweighting(const int& evt) const
  {
    return r(evt, n_phase_space_random_numbers);
  }
  KOKKOS_INLINE_FUNCTION double r_flav_proc(const int& evt) const
  {
    return r(evt, n_phase_space_random_numbers + 1);
  }
  KOKKOS_INLINE_FUNCTION double r_lc_perm(const int& evt) const
  {
    return r(evt, n_phase_space_random_numbers + 2);
  }
  KOKKOS_INLINE_FUNCTION double r_hel(const int& block) const {
    return r(block, n_phase_space_random_numbers + 3);
  }

  KOKKOS_INLINE_FUNCTION Vec4 p(const int& evt, const int& _ptcl) const
  {
    Vec4 x = {e(evt, _ptcl), px(evt, _ptcl), py(evt, _ptcl), pz(evt, _ptcl)};
    return x;
  }

  template <typename T>
  KOKKOS_INLINE_FUNCTION void set_p(int evt, int _ptcl, const T& v)
  {
    e(evt, _ptcl) = v[0];
    px(evt, _ptcl) = v[1];
    py(evt, _ptcl) = v[2];
    pz(evt, _ptcl) = v[3];
  }

  KOKKOS_INLINE_FUNCTION Vec4 internal_p(const int& evt, const int& i) const
  {
    return {internal_e(evt, i), internal_px(evt, i), internal_py(evt, i),
            internal_pz(evt, i)};
  }

  KOKKOS_INLINE_FUNCTION void internal_e(const int& evt, const int& i,const double& val) const
  {
    e(evt,n_external_states + i) = val;
  }
  KOKKOS_INLINE_FUNCTION double internal_e(const int& evt, const int& i) const
  {
    return e(evt,n_external_states + i);
  }

  KOKKOS_INLINE_FUNCTION void internal_px(const int& evt, const int& i,const double& val) const
  {
    px(evt,n_external_states + i) = val;
  }
  KOKKOS_INLINE_FUNCTION double internal_px(const int& evt, const int& i) const
  {
    return px(evt,n_external_states + i);
  }

  KOKKOS_INLINE_FUNCTION void internal_py(const int& evt, const int& i,const double& val) const
  {
    py(evt,n_external_states + i) = val;
  }
  KOKKOS_INLINE_FUNCTION double internal_py(const int& evt, const int& i) const
  {
    return py(evt,n_external_states + i);
  }

  KOKKOS_INLINE_FUNCTION void internal_pz(const int& evt, const int& i,const double& val) const
  {
    pz(evt,n_external_states + i) = val;
  }
  KOKKOS_INLINE_FUNCTION double internal_pz(const int& evt, const int& i) const
  {
    return pz(evt,n_external_states + i);
  }

  KOKKOS_INLINE_FUNCTION CVec4 c(const int& evt, const int& i, const Helicity helicity = Helicity::unset) const
  {
    CVec4 ret;
    ret[0] = c0(evt, i, helicity);
    ret[1] = c1(evt, i, helicity);
    ret[2] = c2(evt, i, helicity);
    ret[3] = c3(evt, i, helicity);
    return ret;
  }
  KOKKOS_INLINE_FUNCTION CVec4 internal_c(const int& evt, const int& i) const
  {
    CVec4 ret;
    ret[0] = internal_c0(evt, i);
    ret[1] = internal_c1(evt, i);
    ret[2] = internal_c2(evt, i);
    ret[3] = internal_c3(evt, i);
    return ret;
  }

  KOKKOS_INLINE_FUNCTION C c0(const int& evt, const int& i, Helicity helicity = Helicity::unset) const
  {
    return _c0(evt,i + ((int(helicity) + 1) % 2) * n_ptcl);
  }
  KOKKOS_INLINE_FUNCTION void c0(const int& evt, const int& i, Helicity helicity, const C& val) const
  {
    _c0(evt,i + ((int(helicity) + 1) % 2) * n_ptcl) = val;
  }

  KOKKOS_INLINE_FUNCTION C c1(const int& evt, const int& i, Helicity helicity = Helicity::unset) const
  {
    return _c1(evt,i + ((int(helicity) + 1) % 2) * n_ptcl);
  }
  KOKKOS_INLINE_FUNCTION void c1(const int& evt, const int& i, Helicity helicity, const C& val) const
  {
    _c1(evt,i + ((int(helicity) + 1) % 2) * n_ptcl) = val;
  }

  KOKKOS_INLINE_FUNCTION C c2(const int& evt, const int& i, Helicity helicity = Helicity::unset) const
  {
    return _c2(evt,i + ((int(helicity) + 1) % 2) * n_ptcl);
  }
  KOKKOS_INLINE_FUNCTION void c2(const int& evt, const int& i, Helicity helicity, const C& val) const
  {
    _c2(evt,i + ((int(helicity) + 1) % 2) * n_ptcl) = val;
  }

  KOKKOS_INLINE_FUNCTION C c3(const int& evt, const int& i, Helicity helicity = Helicity::unset) const
  {
    return _c3(evt,i + ((int(helicity) + 1) % 2) * n_ptcl);
  }
  KOKKOS_INLINE_FUNCTION void c3(const int& evt, const int& i, Helicity helicity, const C& val) const
  {
    _c3(evt,i + ((int(helicity) + 1) % 2) * n_ptcl) = val;
  }
  KOKKOS_INLINE_FUNCTION void set_c(const int& evt, const int& i, const CVec4& c,
             Helicity helicity = Helicity::unset) const
  {
    c0(evt, i, helicity, c[0]);
    c1(evt, i, helicity, c[1]);
    c2(evt, i, helicity, c[2]);
    c3(evt, i, helicity, c[3]);
  }
  KOKKOS_INLINE_FUNCTION void set_internal_c(const int& evt, const int& i, const CVec4& c) const
  {
    internal_c0(evt, i, c[0]);
    internal_c1(evt, i, c[1]);
    internal_c2(evt, i, c[2]);
    internal_c3(evt, i, c[3]);
  }

  KOKKOS_INLINE_FUNCTION void internal_c0(const int& evt, const int& c,const C& val) const
  {
    _c0(evt,n_external_helicity_states + c) = val;
  }
  KOKKOS_INLINE_FUNCTION C internal_c0(const int& evt, const int& c) const
  {
    return _c0(evt,n_external_helicity_states + c);
  }
  
  KOKKOS_INLINE_FUNCTION void internal_c1(const int& evt, const int& c,const C& val) const
  {
    _c1(evt,n_external_helicity_states + c) = val;
  }
  KOKKOS_INLINE_FUNCTION C internal_c1(const int& evt, const int& c) const
  {
    return _c1(evt,n_external_helicity_states + c);
  }
  
  KOKKOS_INLINE_FUNCTION void internal_c2(const int& evt, const int& c,const C& val) const
  {
    _c2(evt,n_external_helicity_states + c) = val;
  }
  KOKKOS_INLINE_FUNCTION C internal_c2(const int& evt, const int& c) const
  {
    return _c2(evt,n_external_helicity_states + c);
  }
  
  KOKKOS_INLINE_FUNCTION void internal_c3(const int& evt, const int& c,const C& val) const
  {
    _c3(evt,n_external_helicity_states + c) = val;
  }
  KOKKOS_INLINE_FUNCTION C internal_c3(const int& evt, const int& c) const
  {
    return _c3(evt,n_external_helicity_states + c);
  }

  KOKKOS_INLINE_FUNCTION void internal_ptcl(const int& block, const int& p,const Ptcl_num& val) const
  {
    const int local_block = Kokkos::min(block, n_helicity_blocks - 1);
    ptcl[n_external_states + n_internal_states * active_hel_conf_idx[local_block] + p] = val;
  }
  KOKKOS_INLINE_FUNCTION Ptcl_num internal_ptcl(const int& block, const int& p) const
  {
    const int local_block = Kokkos::min(block, n_helicity_blocks - 1);
    return ptcl[n_external_states + n_internal_states * active_hel_conf_idx[local_block] + p];
  }
  KOKKOS_INLINE_FUNCTION Ptcl_num internal_ptcl(const int& p) const
  {
    return internal_ptcl(0, p);
  }
  
  KOKKOS_INLINE_FUNCTION Helicity helicity(const int& block, const int& p) const
  {
    const int local_block = Kokkos::min(block, n_helicity_blocks - 1);
    return calculate_helicity(hel_conf_idx[local_block], p);
  }
  KOKKOS_INLINE_FUNCTION void internal_helicity(const int& block, const int& p,const Helicity& hel) const
  {
    const int local_block = Kokkos::min(block, n_helicity_blocks - 1);
    hels[n_internal_states * active_hel_conf_idx[local_block] + p] = hel;
  }
  KOKKOS_INLINE_FUNCTION Helicity internal_helicity(const int& block, const int& p) const
  {
    const int local_block = Kokkos::min(block, n_helicity_blocks - 1);
    return hels[n_internal_states * active_hel_conf_idx[local_block] + p];
  }
  KOKKOS_INLINE_FUNCTION double alt_me_real(const int& evt, const int& perm) const
  {
    return me_real(evt,n_perm + perm);
  }
  KOKKOS_INLINE_FUNCTION void alt_me_real(const int& evt, const int& perm,const double& val) const
  {
    me_real(evt,n_perm + perm) = val;
  }

  KOKKOS_INLINE_FUNCTION double alt_me_imag(const int& evt, const int& perm) const
  {
    return me_imag(evt,n_perm + perm);
  }
  KOKKOS_INLINE_FUNCTION void alt_me_imag(const int& evt, const int& perm,const double& val) const
  {
    me_imag(evt,n_perm + perm) = val;
  }

  KOKKOS_INLINE_FUNCTION void record_overweight(const double abs_rel_w) const
  {
    n_overweight() = n_overweight() + 1;
    max_overweight() = Kokkos::max(max_overweight(), abs_rel_w);
  }
};

#endif
