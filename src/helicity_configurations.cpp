// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "helicity_configurations.h"
#include "event_handle.h"
#include "helicity_configurations_kernel.h"
#include "helicity_configurations_mask.h"
#include "permutations.h"
#include "print.h"
#include "flavour_process.h"
#include "timing.h"
#include "weyl.h"
#include <array>
#include <cassert>
#include <iomanip>
#include <string>

std::string to_string(Helicity_treatment h)
{
  switch (h) {
  case Helicity_treatment::sampled:
    return "Sampling";
  case Helicity_treatment::summed:
    return "Summing";
  case Helicity_treatment::fixed:
    return "Constant";
  }
  assert(false);
  return "Unknown";
}

Helicity_summing_options to_helicity_summing_options(const std::string& s)
{
  Helicity_summing_options opts;
  for (char c : s) {
    switch (c) {
    case 'c':
      opts |= Helicity_summing_option::cached;
      break;
    case 'p':
      opts |= Helicity_summing_option::parity_symmetric;
      break;
    default:
      throw std::invalid_argument {
          "`" + s + "' can not be translated to a helicity mode."};
    }
  }
  return opts;
}

Helicity_configurations::Helicity_configurations(const Helicity_settings& s,
                                                 int n_ptcl)
    : _settings {s}
{
  if (s.treatment == Helicity_treatment::fixed) {
    assert((int)s.fixed_helicities.size() == n_ptcl);
    fixed_current = 0;
    for (int i {0}; i < n_ptcl; ++i) {
      const char helicity_spec {s.fixed_helicities[i]};
      assert(helicity_spec == '+' || helicity_spec == '-');
      Helicity helicity {helicity_spec == '+' ? Helicity::plus
                                              : Helicity::minus};
      if (i < 2)
        helicity = flip_helicity(helicity);
      const int h {static_cast<int>(helicity)};
      if (h == 1)
        fixed_current |= (1 << i);
    }
    _n_helicities = 1;
  }
  else {
    _n_helicities = 1 << n_ptcl;
  }
}

Helicity_configurations::~Helicity_configurations()
{
  using namespace Timing;
  register_duration(
      {Task::Event_generation, Task::External_states_construction},
      kernel_duration);
}

void Helicity_configurations::reset(Event_handle& evt)
{
  evt.host.hel_conf_idx(0) = -1;
  needs_reset = false;
}

bool Helicity_configurations::advance(Event_handle& evt,
                                      const Helicity_configurations_mask& mask)
{
  if (needs_reset)
    return false;
  // We use the first helicity configuration index to decide if we are in a
  // state after being reset or not, see reset(Event_handle&). Also, for
  // non-sampled helicity treatments, we only have one entry anyway.
  int& main_idx {evt.host.hel_conf_idx(0)};
  switch (_settings.treatment) {
  case Helicity_treatment::summed: {
    do {
      main_idx++;
    } while (main_idx < _n_helicities && !mask.enabled(main_idx));
    if (main_idx < _n_helicities) {
      evt.push_helicity_configurations_to_device();
      return true;
    }
    else {
      needs_reset = true;
      return false;
    }
  }
  case Helicity_treatment::sampled: {
    RUN_HELICITY_BLOCK_KERNEL(set_random_helicities_kernel, evt);
    needs_reset = true;
    return true;
  }
  case Helicity_treatment::fixed: {
    main_idx = fixed_current;
    evt.push_helicity_configurations_to_device();
    needs_reset = true;
    return true;
  }
  }
  assert(false);
  return false;
}

double Helicity_configurations::sampling_factor(int n_active) const
{
  if (_settings.treatment == Helicity_treatment::sampled) {
    return n_active / (double)_n_helicities;
  }
  return 1.0;
}

void Helicity_configurations::precalculate_all_external_states(
    Event_handle& evt) const
{
  for (int h {0}; h < 2; ++h) {
    construct_external_state(evt, static_cast<Helicity>(h));
  }
}

void Helicity_configurations::precalculate_external_states(
    Event_handle& evt) const
{
  construct_external_state(evt);
}

void Helicity_configurations::construct_external_state(Event_handle& evt,
                                                       Helicity helicity) const
{
  Timer timer;
  const int n_ptcl = evt.host.n_ptcl;
  const auto& smprop = sm_properties::getInstance();
  for (int p {0}; p < n_ptcl; ++p) {
    const Ptcl_num ptcl {evt.host.ptcl(p)};
    RUN_KERNEL(construct_external_state_kernel, evt, p, ptcl, helicity, smprop);
  }
  kernel_duration += timer.elapsed();
}

void Helicity_configurations::debug_current(const Event_handle& evt) const
{
  if (Msg::verbosity < Msg::Verbosity::debug)
    return;
  for (int i {0}; i < evt.host.n_helicity_blocks; ++i) {
    print(MARK_DEBUG, "Helicity configuration for block ", std::setw(3), i,
          ": ", std::setw(3), evt.host.hel_conf_idx(i), " = (");
    for (int p {0}; p < evt.host.n_ptcl; ++p) {
      print(evt.host.helicity(i, p));
    }
    println(")");
  }
}
