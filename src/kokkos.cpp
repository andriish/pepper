// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2024 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "kokkos.h"
#include <iostream>

namespace Pepper_kokkos {

void initialise_and_register_finalise(int& argc, char* argv[])
{
  bool help_or_version_requested {false};
  bool kokkos_help_requested {false};
  for (int i {0}; i < argc; ++i) {
    if (strcmp(argv[i], "--help") == 0 || strcmp(argv[i], "-h") == 0 ||
        strcmp(argv[i], "--version") == 0 || strcmp(argv[i], "-v") == 0) {
      help_or_version_requested = true;
    }
    if (strcmp(argv[i], "--kokkos-help") == 0)
      kokkos_help_requested = true;
  }
  if (help_or_version_requested && !kokkos_help_requested)
    // the user has requested Pepper's help or version (and does not ask
    // explicitly for Kokkos' help), so let's make Kokkos as silent as possible,
    // and do not pass argv to it, so it won't display its own help (the user
    // can use --kokkos-help for that)
    Kokkos::initialize(
        Kokkos::InitializationSettings {}.set_disable_warnings(true));
  else
    Kokkos::initialize(argc, argv);
  std::atexit(Kokkos::finalize);
  if (kokkos_help_requested && !help_or_version_requested) {
    std::cerr << "Will exit after displaying Kokkos' help. Use `--help` to "
                 "display Pepper's help.\n";
    std::exit(0);
  }
}

} // namespace Pepper_kokkos
