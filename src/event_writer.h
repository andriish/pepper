// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef EVENT_WRITER_H
#define EVENT_WRITER_H

#include <memory>
#include <string>

class Event_handle;
class Flavour_process;
class Pdf_and_alpha_s;
class Process;
struct MC_result;
struct Settings;
struct SM_parameters;

enum class Event_output_format { none, debug, hepmc3, lhef, hdf5 };

class Event_writer {

public:
  virtual ~Event_writer() = default;

  // This is called right before and after event generation (i.e. any calls to
  // write_n are guaranteed to happen between these calls), and can be
  // optionally overridden by derived classes.
  virtual void initialise(const Process&, const Pdf_and_alpha_s&) {};
  virtual void finalise(const MC_result&) {};

  // Write the first n events from Event_handle, taking into account the full
  // event generation's MC_result, the number of non-zero events in the batch,
  // and the number of trials of this batch (potentially including previous
  // all-zero batches).
  virtual void write_n(const Event_handle&, int n, const MC_result&,
                       long n_nonzero, long n_trials, const Process&) = 0;

  // Some writers might prefer to print zero-weight events. An example would be
  // a writer used for debugging purposes.
  virtual bool prints_zero_events() const { return false; };

  // Some writers might prefer that write_n is called even if all events in a
  // batch are zero, independently of whether zero-weight events are printed or
  // not. An example would be writers that need to synchronise via MPI, or
  // writers that can account for n_trials independently of non-zero events.
  virtual bool wants_zero_batches() const { return false; };

  // Some writers will want event-wise leading-colour information.
  virtual bool prints_leading_colour_information() const { return false; };
};

std::unique_ptr<Event_writer> create_event_writer(const Settings&,
                                                  const SM_parameters&);

#endif
