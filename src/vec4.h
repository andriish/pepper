// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_VEC4_H
#define PEPPER_VEC4_H

#include "pepper/config.h"
#include "pepper/vec4.h"

#if VCL_ENABLED

#include "vcl_vec4.h"
using Vec4 = Vcl_vec4;

#else

struct Scl_vec4;
using Vec4 = Scl_vec4;

#endif

#include "vec3.h"
#include <array>
#include <cmath>
#include <ostream>

struct Scl_vec4 : std::array<double, 4> {
  KOKKOS_INLINE_FUNCTION 
  Vec3 vec3() const { return {(*this)[1], (*this)[2], (*this)[3]}; }

  KOKKOS_INLINE_FUNCTION Scl_vec4(const Pepper::Vec4& v)
      : std::array<double, 4> {v[0], v[1], v[2], v[3]} {};
  KOKKOS_INLINE_FUNCTION Scl_vec4() : std::array<double, 4> {} {};
  KOKKOS_INLINE_FUNCTION Scl_vec4(double e, double px, double py, double pz) {
    (*this)[0] = e;(*this)[1]=px;(*this)[2]=py;(*this)[3]=pz;
  };
  // NOTE: The following signed_abs member function is not physical, but can be
  // used to print the invariant mass of four vectors. Negative values then
  // mean that the invariant mass squared is negative (which is usually due to
  // numerics).
  KOKKOS_FUNCTION
  double signed_abs() const
  {
    const double _abs2 {abs2()};
    return (_abs2 < 0.0 ? -1.0 : 1.0) * Kokkos::sqrt(Kokkos::abs(_abs2));
  };
  KOKKOS_INLINE_FUNCTION 
  double abs() const { return Kokkos::sqrt(abs2()); };
  KOKKOS_INLINE_FUNCTION 
  double abs2() const { return (*this) * (*this); };
  KOKKOS_INLINE_FUNCTION 
  double p_plus() const { return (*this)[0] + (*this)[3]; }
  KOKKOS_INLINE_FUNCTION 
  double p_minus() const { return (*this)[0] - (*this)[3]; }
  KOKKOS_INLINE_FUNCTION 
  double p_perp() const { return Kokkos::sqrt(p_perp2()); };
  KOKKOS_INLINE_FUNCTION 
  double p_perp2() const {
    return (*this)[1] * (*this)[1] + (*this)[2] * (*this)[2];
  }
  KOKKOS_INLINE_FUNCTION 
  double m_perp() const { return Kokkos::sqrt(m_perp2()); };
  KOKKOS_INLINE_FUNCTION 
  double m_perp2() const {
    return p_plus() * p_minus();
  }
  KOKKOS_INLINE_FUNCTION
  double y() const {
    return 0.5*Kokkos::log(p_plus()/p_minus());
  }
  KOKKOS_INLINE_FUNCTION
  double cosPhi() const {
    if(p_perp()==0) return 0.;
    return Kokkos::max(Kokkos::min((*this)[1]/p_perp(),1.0),-1.0);
  }
  KOKKOS_INLINE_FUNCTION
  double phi() const {
    if((*this)[2]>0.) return Kokkos::acos(cosPhi());
    else return -Kokkos::acos(cosPhi());
  }
  KOKKOS_INLINE_FUNCTION
  double vec3_abs() const {
    return Kokkos::sqrt(vec3_abs2());
  }
  KOKKOS_INLINE_FUNCTION
  double vec3_abs2() const {
    return (*this)[1]*(*this)[1]+(*this)[2]*(*this)[2]+(*this)[3]*(*this)[3];
  }

  KOKKOS_INLINE_FUNCTION
  Scl_vec4 operator-()
  {
    return {-(*this)[0], -(*this)[1], -(*this)[2], -(*this)[3]};
  }
  KOKKOS_INLINE_FUNCTION 
  Scl_vec4& operator+=(const Scl_vec4& rhs)
  {
    for (int i {0}; i < 4; ++i)
      (*this)[i] += rhs[i];
    return *this;
  }
  KOKKOS_INLINE_FUNCTION 
  Scl_vec4& operator-=(const Scl_vec4& rhs)
  {
    for (int i {0}; i < 4; ++i)
      (*this)[i] -= rhs[i];
    return *this;
  }
  KOKKOS_INLINE_FUNCTION 
  Scl_vec4& operator*=(double rhs)
  {
    for (int i {0}; i < 4; ++i)
      (*this)[i] *= rhs;
    return *this;
  }
  KOKKOS_INLINE_FUNCTION 
  friend Scl_vec4 operator*(Scl_vec4 lhs, double rhs) { return lhs *= rhs; }
  KOKKOS_INLINE_FUNCTION 
  friend Scl_vec4 operator*(double lhs, Scl_vec4 rhs) { return rhs *= lhs; }
  KOKKOS_INLINE_FUNCTION 
  friend Scl_vec4 operator+(Scl_vec4 lhs, const Scl_vec4& rhs) { return lhs += rhs; }
  KOKKOS_INLINE_FUNCTION 
  friend Scl_vec4 operator-(Scl_vec4 lhs, const Scl_vec4& rhs) { return lhs -= rhs; }
  KOKKOS_INLINE_FUNCTION 
  friend double operator*(const Scl_vec4& lhs, const Scl_vec4& rhs)
  {
    return lhs[0] * rhs[0] - lhs[1] * rhs[1] - lhs[2] * rhs[2] -
           lhs[3] * rhs[3];
  };

  friend std::ostream& operator<<(std::ostream& o, const Scl_vec4& v)
  {
    return o << "(" << v[0] << ", " << v[1] << ", " << v[2] << ", " << v[3] << ")";
  }

};

#endif
