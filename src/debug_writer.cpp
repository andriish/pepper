// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "debug_writer.h"

#include "pepper/config.h"

#include "event_handle.h"
#include "fill_leading_colour_information.h"
#include "paths.h"
#include "settings.h"

using namespace Paths;

Debug_writer::Debug_writer(const std::string& filepath, const Settings& s)
    : out {path_with_rank_info(filepath)},
      hel_treatment {s.helicity_settings.treatment},
      zero_weight_event_output_enabled {s.zero_weight_event_output_enabled},
      leading_colour_flow_output_enabled {s.leading_colour_flow_output_enabled}
{
  INFO("Events will be written in debug format to \"",
       path_with_rank_pattern(filepath), "\"");
}

void Debug_writer::initialise(const Process& proc, const Pdf_and_alpha_s&)
{
  colours.resize(proc.n_ptcl());
  anticolours.resize(proc.n_ptcl());
}

void Debug_writer::write_n(const Event_handle& evt, int n,
                           const MC_result& mc_result, long n_nonzero,
                           long n_trials, const Process& proc)
{
  (void)mc_result;
  (void)n_nonzero;
  (void)n_trials;

  bool prints_random_numbers {true};
  bool prints_internal_momenta {true};
  bool prints_currents {true};
  if (!zero_weight_event_output_enabled) {
    prints_random_numbers = false;
    prints_internal_momenta = false;
    prints_currents = false;
  }
  else {
    evt.pull_random_numbers_from_device();
    evt.pull_internal_momenta_from_device();
    evt.pull_currents_from_device();
  }

  for (int i {0}; i < n; ++i) {

    // Helicity block information
    if (hel_treatment == Helicity_treatment::sampled) {
      if (i % HELICITY_BLOCK_SIZE == 0) {
        const int block {i / HELICITY_BLOCK_SIZE};
        out << "Helicity block " << block << '\n';
        out << "Random number (helicity sampling) " << evt.host.r_hel(block);
        out << "\n\n";
      }
    }

    const double weight {evt.host.w(i) * evt.host.me2(i)};

    if (!zero_weight_event_output_enabled && weight == 0.0)
      continue;

    out << "Event " << i << '\n';
    if (weight != 0.0)
      out << "Nonzero event " << n_nonzero_written << '\n';
    out << "W " << evt.host.w(i) << '\n';
    out << "ME2 " << evt.host.me2(i) << '\n';
    out << "Weight " << weight << '\n';

    if (prints_random_numbers) {
      out << "Random numbers (phase space)";
      for (int p {2}; p < evt.host.n_ptcl; ++p) {
        for (int c {0}; c < 4; ++c) {
          out << ' ' << evt.host.r_rambo(i, p, c);
        }
      }
      out << '\n';
    }

    out << "External momenta:\n";
    for (int p {0}; p < evt.host.n_ptcl; ++p) {
      out << p << ' ' << evt.host.p(i, p) << '\n';
    }

    if (prints_internal_momenta) {
      out << "Internal momenta:\n";
      for (int p {0}; p < evt.host.n_internal_states; ++p) {
        out << p << ' ' << evt.host.internal_p(i, p) << '\n';
      }
    }

    if (prints_currents) {
      out << "External currents:\n";
      for (int p {0}; p < evt.host.n_external_helicity_states; ++p) {
        if (hel_treatment == Helicity_treatment::sampled) {
          out << p << ' ' << evt.host.c(i, p) << '\n';
        }
        else {
          out << p << ' ' << evt.host.c(i, p, Helicity::plus) << '\n';
          out << p << ' ' << evt.host.c(i, p, Helicity::minus) << '\n';
        }
      }
      out << "Internal currents:\n";
      for (int p {0}; p < evt.host.n_internal_states; ++p) {
        out << p << ' ' << evt.host.internal_c(i, p) << '\n';
      }
    }

    if (leading_colour_flow_output_enabled) {
      fill_leading_colour_information(evt, i, proc, colours, anticolours);
      out << "Leading colour assignments:\n";
      for (int p {0}; p < evt.host.n_ptcl; ++p) {
        out << p << ' ' << colours[p] << ", " << anticolours[p] << '\n';
      }
    }

    out << '\n';

    if (weight != 0.0)
      n_nonzero_written++;
  }
}
