// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_COLOUR_FACTOR_FORM_FILE_GENERATOR_H
#define PEPPER_COLOUR_FACTOR_FORM_FILE_GENERATOR_H

#include "blockgen_legacy/color.h"

#include <string>
#include <vector>

class Flavour_process;

class Colour_factor_form_file_generator {
protected:
  Colour_factor_form_file_generator(const Flavour_process&);
  std::ofstream file;
  const std::vector<Color::ColorFactor> colour_facs;
  const size_t n_colour_facs;
  const std::string process_str;
  // number of elements for a given Melia permutation (= number of QCD
  // particles)
  const int melia_perm_size;

private:
  static std::vector<Color::ColorFactor> create_colour_factors(const Flavour_process&);
};

class Full_colour_factor_form_file_generator : Colour_factor_form_file_generator {
  public:
    Full_colour_factor_form_file_generator(const Flavour_process&);
};

class Leading_colour_factor_form_file_generator : Colour_factor_form_file_generator {
  public:
    Leading_colour_factor_form_file_generator(const Flavour_process&);
};

#endif
