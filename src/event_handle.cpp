// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "event_handle.h"
#include "floating_point_comparisons.h"
#include "helicity_configurations.h"
#include "print.h"
#include "process.h"
#include "timing.h"

#include <array>
#include <iomanip>

// Event_handle::Event_handle(const Event_handle& evt)
// {
//   h = evt.h;
//   copy_device_data(evt);
// }

// Event_handle& Event_handle::operator=(const Event_handle& evt)
// {
//   h = evt.h;
//   copy_device_data(evt);
//   return *this;
// }

Event_handle::Event_handle(const Process& proc_group,
                       const Helicity_settings& hel_settings,
                       long _batch_size,
                       int n_phase_space_random_numbers)
{
  host.batch_size = _batch_size;
  host.n_phase_space_random_numbers = n_phase_space_random_numbers;
  host.n_active_helicities() = proc_group.max_n_active_helicities();
  if (hel_settings.treatment == Helicity_treatment::sampled) {
    host.n_helicity_blocks = (_batch_size - 1) / HELICITY_BLOCK_SIZE + 1;
  }
  else {
    host.n_helicity_blocks = 1;
  }
  host.n_ptcl = proc_group.n_ptcl();
  host.n_perm = proc_group.max_n_perm_qcd();

  detail::configure_internal_state_count(host, proc_group.n_lepton_pairs());
  host.n_external_states = host.n_ptcl;

  detail::configure_helicity_state_counts(host, hel_settings.treatment);

  detail::configure_matrix_element_counts(host, hel_settings);

  // takes care of RNG, X12, Momenta, Currents, ME-real/imag
  detail::configure_storage(host, hel_settings.treatment);

  detail::configure_particle_and_helicity_storage(host, hel_settings.treatment);
  detail::configure_weight_and_me2_storage(host);
  detail::configure_scale_storage(host);
  detail::configure_coupling_storage(host);
  detail::configure_xfx12_storage(host);
  detail::configure_flavour_channel_storage(host);
  detail::configure_active_lc_configuration_storage(host);

  device = host;
}

Event_handle::~Event_handle()
{
  using namespace Timing;
  register_duration(
      {Task::Event_generation, Task::Output, Task::Filter_nonzero_events},
      filter_nonzero_events_duration);
  register_duration(
      {Task::Event_generation, Task::Output, Task::Pull_nonzero_events},
      pull_nonzero_events_duration);
}

void Event_handle::pull_nonzero_events_from_device() {
  Kokkos::Profiling::pushRegion(__func__);
  // zero out host side arrays
  detail::configure_momentum_storage(host);

  Timer timer;

  // 1. count number of non-zero MEs
  int num_nonzero = n_nonzero();

  // 2. Create Device-side buffers that are sized to the number of non-zero MEs.
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout> d_buffer_me2(Kokkos::ViewAllocateWithoutInitializing("buffer_me2"),num_nonzero);
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout> d_buffer_mu2(Kokkos::ViewAllocateWithoutInitializing("buffer_mu2"),num_nonzero);
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout> d_buffer_w(Kokkos::ViewAllocateWithoutInitializing("buffer_w"),num_nonzero);
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout> d_buffer_alpha_s(Kokkos::ViewAllocateWithoutInitializing("buffer_alpha_s"),num_nonzero);
  Kokkos::View<int*,Kokkos::DefaultExecutionSpace::array_layout> d_buffer_active_lc_conf_idx(Kokkos::ViewAllocateWithoutInitializing("buffer_active_lc_conf_idx"),num_nonzero);
  Kokkos::View<int*,Kokkos::DefaultExecutionSpace::array_layout> d_buffer_flav_channels(Kokkos::ViewAllocateWithoutInitializing("buffer_flav_channels"),num_nonzero);

  Kokkos::View<double**,Kokkos::DefaultExecutionSpace::array_layout> d_buffer_e(Kokkos::ViewAllocateWithoutInitializing("buffer_e"),num_nonzero,host.e.extent(1));
  Kokkos::View<double**,Kokkos::DefaultExecutionSpace::array_layout> d_buffer_px(Kokkos::ViewAllocateWithoutInitializing("buffer_px"),num_nonzero,host.px.extent(1));
  Kokkos::View<double**,Kokkos::DefaultExecutionSpace::array_layout> d_buffer_py(Kokkos::ViewAllocateWithoutInitializing("buffer_py"),num_nonzero,host.py.extent(1));
  Kokkos::View<double**,Kokkos::DefaultExecutionSpace::array_layout> d_buffer_pz(Kokkos::ViewAllocateWithoutInitializing("buffer_pz"),num_nonzero,host.pz.extent(1));

  // 3. loop over all events, only copy non-zero into device buffer
  // counter for inside parallel reduce
  Kokkos::View<int> counter("counter");
  int num_elements = 0;
  const auto& devdata = device;
  // shared counter that returns sum
  Kokkos::parallel_reduce("copy_nonzero_events_on_device",devdata.w.extent(0), KOKKOS_LAMBDA(const int& i, int& sum) {
    if(devdata.w(i) != 0 && devdata.me2(i) != 0) {
      // non-zero index is same for all threads
      unsigned int idx = Kokkos::atomic_fetch_add(&counter(),1);
      if(idx<d_buffer_w.extent(0)) { // just a sanity check
        d_buffer_w(idx) = devdata.w(i);
        d_buffer_me2(idx) = devdata.me2(i);
        d_buffer_mu2(idx) = devdata.mu2(i);
        d_buffer_alpha_s(idx) = devdata.alpha_s(i);
        d_buffer_active_lc_conf_idx(idx) = devdata.active_lc_conf_idx(i);
        d_buffer_flav_channels(idx) = devdata.flav_channels(i);
        for(unsigned int j=0;j<d_buffer_e.extent(1);++j){
          d_buffer_e(idx,j)  = devdata.e(i,j);
          d_buffer_px(idx,j) = devdata.px(i,j);
          d_buffer_py(idx,j) = devdata.py(i,j);
          d_buffer_pz(idx,j) = devdata.pz(i,j);
        }

      }
      sum++;
    }
  }, num_elements);

  filter_nonzero_events_duration += timer.elapsed();
  timer.reset();

  // 4. create a subview of the host-side objects and copy the device-side buffers into them
  // const auto h_me2 = Kokkos::subview(host.me2,Kokkos::make_pair(0,num_nonzero));
  // Kokkos::deep_copy(h_me2,d_buffer_me2);
  copy_buffer_1d<double>(d_buffer_me2,host.me2,num_nonzero);
  copy_buffer_1d<double>(d_buffer_mu2,host.mu2,num_nonzero);
  copy_buffer_1d<double>(d_buffer_w,host.w,num_nonzero);
  copy_buffer_1d<double>(d_buffer_alpha_s,host.alpha_s,num_nonzero);
  copy_buffer_1d<int>(d_buffer_active_lc_conf_idx,host.active_lc_conf_idx,num_nonzero);
  copy_buffer_1d<int>(d_buffer_flav_channels,host.flav_channels,num_nonzero);

  copy_buffer_2d<double>(d_buffer_e, host.e, num_nonzero);
  copy_buffer_2d<double>(d_buffer_px,host.px,num_nonzero);
  copy_buffer_2d<double>(d_buffer_py,host.py,num_nonzero);
  copy_buffer_2d<double>(d_buffer_pz,host.pz,num_nonzero);

  pull_nonzero_events_duration += timer.elapsed();

  Kokkos::Profiling::popRegion();
}

template<typename T,typename buffer_type,typename host_type>
void Event_handle::copy_buffer_2d(const buffer_type& dev_buffer,const host_type& host_obj,const int& num_nonzero) const{
  // dev_buffer: this is the buffer where non-zero values have been copied
  // host_obj: this is the original host-side event data object

  Kokkos::deep_copy(host_obj,(T)0);
  // make subview of the host-side object in to which the reduced device side buffer will be copied
  const auto reduced_host = Kokkos::subview(host_obj,Kokkos::make_pair(0,num_nonzero),Kokkos::ALL);
  // create a host-side memory-space compatible view to which the device-side buffer will be copied
  Kokkos::View<T**,Kokkos::DefaultExecutionSpace::array_layout, Kokkos::HostSpace> tmp("host_buffer_copy",dev_buffer.extent(0),dev_buffer.extent(1));
  // copy the device buffer to the host-side memory-space compatible view
  Kokkos::deep_copy(Kokkos::DefaultExecutionSpace(), tmp,  dev_buffer);
  // copy on the host-side which maps the memory counting properly
  Kokkos::deep_copy(reduced_host,  tmp);
}

template<typename T, typename buffer_type,typename host_type>
void Event_handle::copy_buffer_1d(const buffer_type& dev_buffer,const host_type& host_obj,const int& num_nonzero) const{
  // dev_buffer: this is the buffer where non-zero values have been copied
  // host_obj: this is the original host-side event data object

  Kokkos::deep_copy(host_obj,(T)0);
  // make subview of the host-side object in to which the reduced device side buffer will be copied
  const auto reduced_host = Kokkos::subview(host_obj,Kokkos::make_pair(0,num_nonzero));
  // create a host-side memory-space compatible view to which the device-side buffer will be copied
  Kokkos::View<T*,Kokkos::DefaultExecutionSpace::array_layout, Kokkos::HostSpace> tmp("host_buffer_copy",dev_buffer.extent(0));
  // copy the device buffer to the host-side memory-space compatible view
  Kokkos::deep_copy(Kokkos::DefaultExecutionSpace(), tmp,  dev_buffer);
  // copy on the host-side which maps the memory counting properly
  Kokkos::deep_copy(reduced_host,  tmp);
}

double Event_handle::xs_sum() const
{
  double sum {0.0};
  const auto& devdata = device;
  Kokkos::parallel_reduce(__func__,devdata.w.extent(0), KOKKOS_LAMBDA(const int& i,double& local_sum){
    local_sum += devdata.w(i) * devdata.me2(i);
  },sum);
  return sum;
}

double Event_handle::xs2_sum() const
{
  double sum {0.0};
  const auto& devdata = device;
  Kokkos::parallel_reduce(__func__,devdata.w.extent(0), KOKKOS_LAMBDA(const int& i,double& local_sum){
    local_sum += devdata.w(i) * devdata.w(i) * devdata.me2(i) * devdata.me2(i);
  },sum);
  return sum;
}

int Event_handle::n_nonzero() const
{
  int num_nonzero = 0;
  const auto& devdata = device;
  Kokkos::parallel_reduce(__func__,devdata.w.extent(0), KOKKOS_LAMBDA(const int& evt_idx, int& sum) {
    if(devdata.w(evt_idx) != 0) sum++;
  },num_nonzero);
  return num_nonzero;
}

void Event_handle::apply_weight_prefactor(double fac)
{
  const auto& devdata = device;
  Kokkos::parallel_for(__func__,device.w.extent(0),KOKKOS_LAMBDA(const int& evt_idx){
    devdata.w(evt_idx) *= fac;
  });
}

void Event_handle::reset_me(int perm)
{ 
  const auto& devdata = device;
  Kokkos::parallel_for(__func__,device.me_real.extent(0),KOKKOS_LAMBDA(const int& evt_idx){
    if(devdata.w(evt_idx) != 0){
      devdata.me_real(evt_idx, perm) = 0.0;
      devdata.me_imag(evt_idx, perm) = 0.0;
    }
  });
}

void Event_handle::reset_alt_me(int perm)
{
  const auto& devdata = device;
  Kokkos::parallel_for(__func__,device.me_real.extent(0),KOKKOS_LAMBDA(const int& evt_idx){
    if(devdata.w(evt_idx) != 0){
      devdata.alt_me_real(evt_idx, perm, 0.0);
      devdata.alt_me_imag(evt_idx, perm, 0.0);
    }
  });
}

void Event_handle::debug_w(const int& evt) const
{
  if (Msg::verbosity < Msg::Verbosity::debug)
    return;
  DEBUG("Weight for evt ", evt + 1, "/", host.batch_size, " = ", host.w(evt));
}

void Event_handle::debug_r(const int& evt) const
{
  if (Msg::verbosity < Msg::Verbosity::debug)
    return;
  DEBUG("Random numbers for evt ", evt + 1, "/", host.batch_size, ':');
  for (int ptcl {2}; ptcl < host.n_ptcl; ++ptcl) {
    DEBUG("p #", ptcl, " = ", host.r_rambo(evt, ptcl, 0));
    DEBUG("p #", ptcl, " = ", host.r_rambo(evt, ptcl, 1));
    DEBUG("p #", ptcl, " = ", host.r_rambo(evt, ptcl, 2));
    DEBUG("p #", ptcl, " = ", host.r_rambo(evt, ptcl, 3));
  }
  for (int i {0}; i < host.n_helicity_blocks; ++i) {
    DEBUG("h #", i, " = ", host.r_hel(i));
  }
}

void Event_handle::debug_p(const int& evt) const
{
  if (Msg::verbosity < Msg::Verbosity::debug)
    return;
  DEBUG("External momenta for evt ", evt + 1, "/", host.batch_size, ':');
  for (int ptcl {0}; ptcl < host.n_ptcl; ++ptcl) {
    DEBUG("p #", ptcl, " = ", host.p(evt, ptcl), ", m = ", host.p(evt, ptcl).signed_abs());
  }
}

void Event_handle::debug_c(const int& evt) const
{
  if (Msg::verbosity < Msg::Verbosity::debug)
    return;
  if (host.n_external_helicity_states == host.n_ptcl) {
    // For each particle, only a single helicity has been stored. This means
    // that we are dealing with a random sample of helicities or a fixed
    // helicity.
    DEBUG("External currents for evt ", evt + 1, "/", host.batch_size, ':');
    for (int p {0}; p < host.n_ptcl; ++p) {
      CVec4 current {host.c(evt, p, Helicity::unset)};
      DEBUG("j #", p, " (", std::setw(2), host.ptcl(p), ") = ", current);
    }
  }
  else {
    // For each particle, several helicities have been stored. This means that
    // we are dealing with a full set of precalculated external states intended
    // for helicity summing.
    for (int hel {0}; hel < 2; ++hel) {
      DEBUG("External currents for evt ", evt + 1, "/", host.batch_size,
            ", helicity=", hel, ':');
      for (int p {0}; p < host.n_ptcl; ++p) {
        CVec4 current {host.c(evt, p, static_cast<Helicity>(hel))};
        DEBUG("j #", p, " (", std::setw(2), host.ptcl(p), ") = ", current);
      }
    }
  }
}

void Event_handle::debug_internal_c(const int& evt) const
{
  if (Msg::verbosity < Msg::Verbosity::debug)
    return;
  DEBUG("Internal currents for evt ", evt + 1, "/", host.batch_size, ':');
  const int block {evt / HELICITY_BLOCK_SIZE};
  pull_currents_from_device();
  for (int c {0}; c < host.n_internal_states; ++c) {
    CVec4 current {host.internal_c(evt, c)};
    DEBUG("j #", c, " (", std::setw(2), host.internal_ptcl(block, c), ' ',
          host.internal_helicity(block, c), ") = ", current);
  }
}

void Event_handle::debug_me(const int& evt) const
{
  if (Msg::verbosity < Msg::Verbosity::debug)
    return;
  DEBUG("Matrix elements:");
  for (int p {0}; p < host.n_perm; ++p) {
    C me {host.me_real(evt, p), host.me_imag(evt, p)};
    DEBUG("perm #", p, " = ", me);
  }
}

std::ostream& operator<<(std::ostream& os, const Event_handle& evt)
{
  os << "{\n";
  os << "  batch_size: " << evt.host.batch_size << '\n';
  os << "  n_helicity_blocks: " << evt.host.n_helicity_blocks << '\n';
  os << "  n_active_helicities: " << evt.host.n_active_helicities() << '\n';
  os << "  n_ptcl: " << evt.host.n_ptcl << '\n';
  os << "  n_perm: " << evt.host.n_perm << '\n';
  os << "  n_internal_states: " << evt.host.n_internal_states << '\n';
  os << "  n_external_states: " << evt.host.n_external_states << '\n';
  os << "  n_external_helicity_states: " << evt.host.n_external_helicity_states << '\n';
  os << "  n_me: " << evt.host.n_me << '\n';
  os << "  weights: " << evt.host.w[0] << '\n';
  os << "  random numbers: " << evt.host._r(0) << '\n';
  os << "   e: " << evt.host.e(0,2) << '\n';
  os << "   px: " << evt.host.px(0,2) << '\n';
  os << "   py: " << evt.host.py(0,2) << '\n';
  os << "   pz: " << evt.host.pz(0,2) << '\n';
  os << "   x1: " << evt.host.x1[0] << '\n';
  os << "   x2: " << evt.host.x2[0] << '\n';
  os << "    y: " << evt.host.y(0,0) << '\n';
  os << "  phi: " << evt.host.phi(0,0) << '\n';
  os << "   c0: " << evt.host._c0(0,2).real() << '\n';
  os << "   c1: " << evt.host._c1(0,2).real() << '\n';
  os << "   c2: " << evt.host._c2(0,2).real() << '\n';
  os << "   c2: " << evt.host._c3(0,2).real() << '\n';
  os << " ptcl: " << evt.host.ptcl[0] << '\n';
  os << " hels: " << evt.host.hels[0] << '\n';
  os << "  hel conf idx: " << evt.host.hel_conf_idx[0] << '\n';
  os << "  active hel conf idx: " << evt.host.active_hel_conf_idx[0] << '\n';
  os << "  active hels: " << evt.host.active_hels[0] << '\n';
  os << "  sel weights: " << evt.host.hel_selection_weights[0] << '\n';
  os << "  me_real: [";
  for(int i=0;i<(int)evt.host.me_real.extent(0);++i)
   for(int j=0;j<(int)evt.host.me_real.extent(1);++j)
      os << " " << evt.host.me_real(i,j);
  os << "]\n";
  os << "  me_imag: [";
  for(int i=0;i<(int)evt.host.me_imag.extent(0);++i)
   for(int j=0;j<(int)evt.host.me_imag.extent(1);++j)
      os << " " << evt.host.me_imag(i,j);
  os << "]\n";
  os << "  me2: " << evt.host.me2[0] << '\n';
  os << "}";
  return os;
}
