// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "helicity.h"

std::ostream& operator<<(std::ostream& os, Helicity h)
{
  switch (h) {
    case Helicity::unset:
      return os << "unset";
    case Helicity::plus:
      return os << "+";
    case Helicity::minus:
      return os << "-";
  }
  return os;
}

