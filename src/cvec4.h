// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_CVEC4_H
#define PEPPER_CVEC4_H

#include "pepper/config.h"

#if VCL_ENABLED

#include "vcl_vec4.h"
using CVec4 = Vcl_cvec4;

#else

struct Scl_cvec4;
using CVec4 = Scl_cvec4;

#endif


#include "math.h"
#include "vec4.h"
#include "weyl.h"
#include <array>
#include <cmath>
#include <ostream>


struct Scl_cvec4 : std::array<C, 4> {

  // Make base class constructors available
  using std::array<C, 4>::array;

  KOKKOS_INLINE_FUNCTION Scl_cvec4(const C& i0, const C& i1, const C& i2, const C& i3)
      : std::array<C, 4> {i0, i1, i2, i3}
  {
  }

  // Construct a complex (contravariant Lorentz) 4-vector from two Weyl spinors,
  // using the construction in
  // https://de.m.wikipedia.org/wiki/Spinor-Helizitäts-Formalismus .  This
  // corresponds to SHERPA::Polarization_Vector::VT.
  KOKKOS_INLINE_FUNCTION Scl_cvec4(const Weyl& a, const Weyl& b)
  {
    (*this)[0] = a[0] * b[0] + a[1] * b[1];
    (*this)[1] = a[0] * b[1] + a[1] * b[0];
    (*this)[2] = C(0,1) * (a[0] * b[1] - a[1] * b[0]);
    (*this)[3] = a[0] * b[0] - a[1] * b[1];
  }

  KOKKOS_INLINE_FUNCTION C abs() const { return Complex::sqrt(abs2()); };
  KOKKOS_INLINE_FUNCTION C abs2() const { return (*this) * (*this); };
  KOKKOS_INLINE_FUNCTION C p_plus() const { return (*this)[0] + (*this)[3]; }
  KOKKOS_INLINE_FUNCTION C p_minus() const { return (*this)[0] - (*this)[3]; }
  KOKKOS_INLINE_FUNCTION Scl_cvec4& operator+=(const Scl_cvec4& rhs)
  {
    for (int i {0}; i < 4; ++i)
      (*this)[i] += rhs[i];
    return *this;
  }
  KOKKOS_INLINE_FUNCTION Scl_cvec4& operator-=(const Scl_cvec4& rhs)
  {
    for (int i {0}; i < 4; ++i)
      (*this)[i] -= rhs[i];
    return *this;
  }
  KOKKOS_INLINE_FUNCTION Scl_cvec4& operator*=(C rhs)
  {
    for (int i {0}; i < 4; ++i)
      (*this)[i] *= rhs;
    return *this;
  }
  KOKKOS_INLINE_FUNCTION Scl_cvec4& operator/=(C rhs)
  {
    for (int i {0}; i < 4; ++i)
      (*this)[i] /= rhs;
    return *this;
  }
  KOKKOS_INLINE_FUNCTION void conjugate()
  {
    for (int i {0}; i < 4; ++i)
      (*this)[i] = Complex::conj((*this)[i]);
  }
  KOKKOS_INLINE_FUNCTION friend Scl_cvec4 operator*(Scl_cvec4 lhs, C rhs) { return lhs *= rhs; }
  KOKKOS_INLINE_FUNCTION friend Scl_cvec4 operator*(C lhs, Scl_cvec4 rhs) { return rhs *= lhs; }
  KOKKOS_INLINE_FUNCTION friend Scl_cvec4 operator/(Scl_cvec4 lhs, C rhs) { return lhs /= rhs; }
  KOKKOS_INLINE_FUNCTION friend Scl_cvec4 operator+(Scl_cvec4 lhs, const Scl_cvec4& rhs)
  {
    return lhs += rhs;
  }
  KOKKOS_INLINE_FUNCTION friend Scl_cvec4 operator-(Scl_cvec4 lhs, const Scl_cvec4& rhs)
  {
    return lhs -= rhs;
  }
  KOKKOS_INLINE_FUNCTION friend C operator*(const Scl_cvec4& lhs, const Scl_cvec4& rhs)
  {
    return lhs[0] * rhs[0] - lhs[1] * rhs[1] - lhs[2] * rhs[2] -
           lhs[3] * rhs[3];
  };
  KOKKOS_INLINE_FUNCTION friend C operator*(const Scl_cvec4& lhs, const Scl_vec4& rhs)
  {
    return lhs[0] * rhs[0] - lhs[1] * rhs[1] - lhs[2] * rhs[2] -
           lhs[3] * rhs[3];
  };
  KOKKOS_INLINE_FUNCTION friend C operator*(const Scl_vec4& lhs, const Scl_cvec4& rhs)
  {
    return lhs[0] * rhs[0] - lhs[1] * rhs[1] - lhs[2] * rhs[2] -
           lhs[3] * rhs[3];
  };

  friend std::ostream& operator<<(std::ostream& o, const Scl_cvec4& v)
  {
    return o << "(" << v[0] << ", " << v[1] << ", " << v[2] << ", " << v[3] << ")";
  }

  KOKKOS_INLINE_FUNCTION void print() const{
    printf("( {%10.3g,%10.3g}, {%10.3g,%10.3g}, {%10.3g,%10.3g}, {%10.3g,%10.3g} )",
      (*this)[0].real(),(*this)[0].imag(),
      (*this)[1].real(),(*this)[1].imag(),
      (*this)[2].real(),(*this)[2].imag(),
      (*this)[3].real(),(*this)[3].imag()
    );
  }
  
};

KOKKOS_INLINE_FUNCTION Scl_cvec4 operator*(const Scl_vec4& vec, const C& scalar)
{
  return {scalar * vec[0], scalar * vec[1], scalar * vec[2], scalar * vec[3]};
};

KOKKOS_INLINE_FUNCTION Scl_cvec4 operator*(const C& scalar, const Scl_vec4& vec)
{
  return {scalar * vec[0], scalar * vec[1], scalar * vec[2], scalar * vec[3]};
};

#endif
