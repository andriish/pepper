// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_HELICITY_CONFIGURATIONS_KERNEL_H
#define PEPPER_HELICITY_CONFIGURATIONS_KERNEL_H


#include "event_handle.h"
#include "external_state_builders.h"
#include "helicity.h"
#include "kernel_macros.h"
#include "rng.h"
#include "sm.h"
#include "weyl.h"

#include <cassert>

template<typename event_type>
KOKKOS_INLINE_FUNCTION void
set_random_helicities_kernel(event_type& evt, int b)
{
  int active_hel_conf_idx = -1;
  double sum = 0;
  for (int i {0}; i < evt.n_active_helicities(); ++i) {
    sum += evt.hel_selection_weights(i);
    if(evt.r_hel(b) < sum){
      active_hel_conf_idx = i;
      break;
    }
  }
  assert(active_hel_conf_idx >= 0);

  evt.hel_conf_idx(b) = evt.active_hels(active_hel_conf_idx);
  evt.active_hel_conf_idx(b) = active_hel_conf_idx;
}
DEFINE_CUDA_KERNEL_NARGS_0(set_random_helicities_kernel);

template<typename event_data>
KOKKOS_INLINE_FUNCTION void
construct_external_state_kernel(const event_data& evt, int i, int p,
                                Ptcl_num ptcl, Helicity storage_helicity,
                                const sm_properties& smprop)
{

  Helicity physics_helicity {storage_helicity};
  if (storage_helicity == Helicity::unset) {
    // Resolve randomly sampled helicity for the current helicity block
    const int block {i / HELICITY_BLOCK_SIZE};
    physics_helicity = evt.helicity(block, p);
  }
  if (ptcl == GLUON) {
    const CVec4 j {
        build_massless_polarisation_vector(evt.p(i, p), physics_helicity)};
    evt.set_c(i, p, j, storage_helicity);
  }
  else {
    if (smprop.mass(ptcl) > 0.0) {
      const CVec4 s {build_massive_spinor(evt.p(i, p), physics_helicity, ptcl)};
      evt.set_c(i, p, s, storage_helicity);
    }
    else {
      const Weyl s {build_massless_spinor(evt.p(i, p), physics_helicity, ptcl)};
      evt.c0(i, p, storage_helicity, s[0]);
      evt.c1(i, p, storage_helicity, s[1]);
    }
  }
}
DEFINE_CUDA_KERNEL_NARGS_4(construct_external_state_kernel,
                           int, p,
                           Ptcl_num, ptcl,
                           Helicity, helicity,
                           const sm_properties&, smprop);

#endif
