// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "hdf5_writer.h"

#include "citations.h"
#include "event_handle.h"
#include "fill_leading_colour_information.h"
#include "mpi.h"
#include "paths.h"
#include "pdf_and_alpha_s.h"
#include "process.h"
#include "settings.h"
#include "sm.h"
#include "timing.h"

#include <array>
#include <random>

// Sherpa might expect us to use a single process, which we emulate by labelling
// all processes with the same index ("1"), and adding a single proc to the
// "procInfo" data, with the summed cross section of all procs
#define SHERPA_READIN_COMPAT_MODE 1

using namespace Paths;
using namespace HighFive;

Hdf5_writer::Hdf5_writer(const std::string& filename, const Settings& s,
                         const SM_parameters& sm)
    : batch_size {s.batch_size},
      n_batches {s.n_batches},
      unweighting_disabled {s.unweighting_disabled},
      leading_colour_flow_output_enabled {s.leading_colour_flow_output_enabled},
      e_cms {s.e_cms},
      ew_alpha {sm.ew.alpha()}
{
  Timer timer;
#if MPI_FOUND && H5_HAVE_PARALLEL
  INFO("Events will be written collectively in LHEH5 format to \"", filename, "\"");
  HighFive::FileAccessProps props;
  props.add(MPIOFileAccess {MPI_COMM_WORLD, MPI_INFO_NULL});
  props.add(MPIOCollectiveMetadata {});
  file = std::make_unique<File>(filename, File::Overwrite, props);
  transfer_props.add(UseCollectiveIO {});
#else
  INFO("Events will be written in LHEH5 format to \"", filename, "\"");
  file = std::make_unique<File>(path_with_rank_info(filename), File::Overwrite);
#endif
  timer.register_elapsed({Timing::Task::Hdf5_open});
}

void Hdf5_writer::initialise(const Process& proc,
                             const Pdf_and_alpha_s& pdf_and_alphas)
{
  Timer timer;
  Citations::register_citation("HDF5-based LHE storage (LHEH5) v2",
                               "Hoeche:2019rti,Bothmann:2023ozs");

  colours.resize(proc.n_ptcl());
  anticolours.resize(proc.n_ptcl());
#if MPI_FOUND && H5_HAVE_PARALLEL
  set_expected_n_nonzero_per_batch(batch_size);
#else
  event_cache_size = 100;
  evt_cache.reserve(100);
  ptcl_cache.reserve(100);
#endif
  write_header(proc, pdf_and_alphas);
  initialised = true;
  timer.register_elapsed({Timing::Task::Hdf5_init});
}

void Hdf5_writer::finalise(const MC_result& result)
{
  // write generated cross section result
  const auto result_data =
      std::array<double, 2> {result.mean(), result.stddev()};
  auto result_data_set = file->createDataSet<double>(
      "generatedResult", DataSpace::From(result_data));
  result_data_set.write(result_data, transfer_props);
  const std::vector<std::string> result_keys {"xSection", "error"};
  result_data_set.createAttribute("properties", result_keys);

  // write run statistics
  const auto stats_data = std::array<long, 3> {
      result.n_trials, result.n_nonzero_after_cuts, result.n_nonzero};
  auto stats_data_set =
      file->createDataSet<long>("runStatistics", DataSpace::From(stats_data));
  stats_data_set.write(stats_data, transfer_props);
  const std::vector<std::string> stats_keys {"trials", "nonzeroAfterCuts",
                                             "nonzero"};
  stats_data_set.createAttribute("properties", stats_keys);
}

void Hdf5_writer::set_expected_n_nonzero_per_batch(double n_nonzero)
{
#if MPI_FOUND && H5_HAVE_PARALLEL
  batch_cache_size = int(std::max(1.0, 100 / n_nonzero));
  evt_cache.reserve(200);
  ptcl_cache.reserve(200);
#else
  (void)n_nonzero;
#endif
}

Hdf5_writer::~Hdf5_writer()
{
  Timer timer;
  // Account for n_trials which have not been attached to any event yet.
  write_cache();
  write_remaining_n_zero_events();
  write_events_duration += timer.elapsed();

  using namespace Timing;

  register_duration({Task::Event_generation, Task::Output, Task::Hdf5_write},
                    write_events_duration);

  // Explicitly delete the HDF5 file, such that we can measure the duration of
  // this operation This can be significant, depending on the number of events
  // in RAM and the file system.
  timer.reset();
  file.reset();
  timer.register_elapsed({Task::Hdf5_close});
}

void Hdf5_writer::write_header(const Process& proc,
                               const Pdf_and_alpha_s& pdf_and_alphas)
{
  const int n_procs {proc.n_procs()};

  // determine beam particles via a heuristic approach
  std::array<int, 2> beams {-1, -1};
  if (pdf_and_alphas.pdf_enabled()) {
    // PDF use is enabled, for now we assume that this means that we are doing
    // proton-proton collisions
    beams[0] = 2212;
    beams[1] = 2212;
  }
  else if (n_procs == 1) {
    // no PDF is used, but there is only a single process, so let's use its
    // initial-state particles as pure partonic beams in this case
    beams[0] = proc[0][0];
    beams[1] = proc[0][1];
  }

  // write version
  const auto version_data = std::array<int, 3> {2, 0, 1};
  auto version_data_set =
      file->createDataSet<int>("version", DataSpace::From(version_data));
  version_data_set.write(version_data, transfer_props);

  // write init data
  const std::vector<std::string> init_data_keys {
      "beamA",     "beamB",   "energyA", "energyB",           "PDFgroupA",
      "PDFgroupB", "PDFsetA", "PDFsetB", "weightingStrategy", "numProcesses"};
  const std::vector<double> init_data {static_cast<double>(beams[0]),
                                       static_cast<double>(beams[1]),
                                       e_cms / 2.0,
                                       e_cms / 2.0,
                                       0,
                                       0,
                                       -1,
                                       -1,
                                       unweighting_disabled ? 1.0 : 3.0,
                                       static_cast<double>(n_procs)};
  auto init_data_set =
      file->createDataSet<double>("init", DataSpace::From(init_data));
  init_data_set.write(init_data, transfer_props);
  init_data_set.createAttribute("properties", init_data_keys);

  // write process data and find n_ptcl_max
  const std::vector<std::string> proc_data_keys {
      "procId", "npLO", "npNLO", "xSection", "error", "unitWeight"};
#if SHERPA_READIN_COMPAT_MODE
  std::vector<std::vector<double>> proc_data(1, std::vector<double>(6, 0));
  proc_data[0][0] = 1;
  proc_data[0][1] = proc.n_ptcl();
  // We do not support NLO processes yet.
  proc_data[0][2] = -1;
  Value_with_error xs;
  if (proc.has_partial_results()) {
    for (int i {0}; i < n_procs; i++) {
      xs.value += proc.partial_results[i].mean();
      xs.error += square(proc.partial_results[i].stddev());
    }
    xs.error = std::sqrt(xs.error);
  }
  else {
    xs.value = xs.error = NAN;
  }
  proc_data[0][3] = xs.value;
  proc_data[0][4] = xs.error;
  if (unweighting_disabled) {
    proc_data[0][5] = -1.0;
  }
  else {
    double w_max {0.0};
    for (int i {0}; i < n_procs; i++) {
      w_max += proc.partial_max_weights[i];
    }
    proc_data[0][5] = w_max;
  }
  n_ptcl_max = proc.n_ptcl();
#else
  std::vector<std::vector<double>> proc_data(n_procs,
                                             std::vector<double>(6, 0));
  for (int i {0}; i < n_procs; i++) {
    proc_data[i][0] = i + 1;
    proc_data[i][1] = proc.n_ptcl();
    // We do not support NLO processes yet.
    proc_data[i][2] = -1;
    if (proc.has_partial_results()) {
      proc_data[i][3] = proc.partial_results[i].mean();
      proc_data[i][4] = proc.partial_results[i].stddev();
      proc_data[i][5] = proc.partial_max_weights[i];
    }
    else {
      proc_data[i][3] = proc_data[i][4] = proc_data[i][5] = NAN;
    }
    n_ptcl_max = std::max(n_ptcl_max, proc.n_ptcl());
  }
#endif
  auto proc_info_data_set =
      file->createDataSet<double>("procInfo", DataSpace::From(proc_data));
  proc_info_data_set.write(proc_data, transfer_props);
  proc_info_data_set.createAttribute("properties", proc_data_keys);

  // Needed to set maximum dataset dimensions below. Note that we need to
  // multiply by the number of ranks because we have a single database that is
  // shared by all MPI ranks.
  const long n_events_max {n_batches * batch_size * Mpi::n_ranks()};

  // LHEF event information
  const std::vector<std::string> evt_data_keys {
      "pid",    "nparticles", "start", "trials", "scale",
      "fscale", "rscale",     "aqed",  "aqcd",   "NOMINAL"};
  DataSetCreateProps evt_props {};
  evt_props.add(Chunking({100, evt_data_keys.size()}));
  std::vector<size_t> evt_data_dimensions_min {0, evt_data_keys.size()};
  std::vector<size_t> evt_data_dimensions_max {
      static_cast<size_t>(n_events_max), evt_data_keys.size()};
  auto evt_data_space {
      DataSpace(evt_data_dimensions_min, evt_data_dimensions_max)};
  file->createDataSet<double>("events", evt_data_space, evt_props)
      .createAttribute("events", evt_data_keys);

  // LHEF particle information
  std::vector<std::string> ptcl_data_keys {
      "id", "status", "mother1", "mother2", "color1",   "color2", "px",
      "py", "pz",     "e",       "m",       "lifetime", "spin"};
  DataSetCreateProps ptcl_props {};
  ptcl_props.add(Chunking({static_cast<size_t>(100*n_ptcl_max), evt_data_keys.size()}));
  std::vector<size_t> ptcl_data_dimensions_min {0, ptcl_data_keys.size()};
  std::vector<size_t> ptcl_data_dimensions_max {
      static_cast<size_t>(n_events_max * n_ptcl_max), ptcl_data_keys.size()};
  auto ptcl_data_space {
      DataSpace(ptcl_data_dimensions_min, ptcl_data_dimensions_max)};
  file->createDataSet<double>("particles", ptcl_data_space, ptcl_props)
      .createAttribute("properties", ptcl_data_keys);
}

void Hdf5_writer::write_n(const Event_handle& evt, int n,
                          const MC_result&, long n_nonzero,
                          long n_trials, const Process& proc)
{
  assert(initialised);

  Timer timer;

  n_cached_batches++;

  if (n_nonzero == 0) {
    n_zero_events += n_trials;
  }
  else {
    // To ensure proper normalization of the total cross section, the "trials"
    // needs to be evenly distributed over all the events in the batch.
    // Otherwise, when events are rejected the total cross section will be modified.
    // TODO: reduce duplication of this trials remainder treatment between this
    // and other writers (e.g. by moving the implementation to the common base
    // class)
    size_t trials_add = (n_trials + n_zero_events) / n_nonzero;
    std::vector<size_t> trials_remainder(n_nonzero, 0);
    for (size_t i = 0; i < (n_trials + n_zero_events) - trials_add * n_nonzero; ++i) {
      trials_remainder[i] = 1;
    }
    n_zero_events = 0;
    // TODO: remove non-determinism of trial remainder treatment by using the
    // global rng instead of creating a new one here with random device seeding
    std::random_device device;
    std::mt19937 gen(device());
    std::shuffle(trials_remainder.begin(), trials_remainder.end(), gen);

    size_t remainder_idx = 0;
    for (int i {0}; i < n; ++i) {

      if (evt.host.w(i) == 0.0)
        continue;

      const auto n_evt_trials = trials_add + trials_remainder[remainder_idx++];

      // The start index in the particle
      const auto ptcl_start_idx {(n_written_across_all_ranks + evt_cache.size()) *
        n_ptcl_max};

      // fill event information
      evt_cache.push_back({
#if SHERPA_READIN_COMPAT_MODE
          static_cast<double>(1),
#else
          static_cast<double>(evt.host.proc_idx + 1),
#endif
          static_cast<double>(evt.host.n_ptcl),
          static_cast<double>(ptcl_start_idx),
          static_cast<double>(n_evt_trials),
          std::sqrt(evt.host.mu2(i)), // muq
          std::sqrt(evt.host.mu2(i)), // muf
          std::sqrt(evt.host.mu2(i)), // mur
          ew_alpha,
          evt.host.alpha_s(i),
          evt.host.w(i) * evt.host.me2(i)
          });

      if (leading_colour_flow_output_enabled) {
        fill_leading_colour_information(evt, i, proc, colours, anticolours);
      }

      for (int p {0}; p < evt.host.n_ptcl; ++p) {
        const bool is_incoming {p < 2};
        Ptcl_num ptcl_num {proc.current_flavour_process().unmapped_ptcl(
            evt.host.flav_channels(i), p)};
        int mother_p1 {0};
        int mother_p2 {0};
        int colour {0};
        int anticolour {0};
        Vec4 mom {evt.host.p(i, p)};
        if (is_incoming) {
          mom *= -1;
        }
        else {
          // outgoing particles have the incoming ones as mothers
          mother_p1 = 1;
          mother_p2 = 2;
        }
        if (leading_colour_flow_output_enabled) {
          colour = colours[p];
          anticolour = anticolours[p];
          if (is_incoming)
            std::swap(colour, anticolour);
        }
        ptcl_cache.push_back({
            static_cast<double>(ptcl_num),
            1,
            static_cast<double>(mother_p1),
            static_cast<double>(mother_p2),
            static_cast<double>(colour),
            static_cast<double>(anticolour),
            mom[1],
            mom[2],
            mom[3],
            mom[0],
            sm_properties::getInstance().hmass(ptcl_num),
            0,
            2 * spin(ptcl_num)
            });
      }

      if (event_cache_size > 0 && evt_cache.size() == event_cache_size)
        flush_cache();
    }
  }

  if (batch_cache_size > 0 && n_cached_batches == batch_cache_size)
    flush_cache();

  write_events_duration += timer.elapsed();
}

void Hdf5_writer::write_remaining_n_zero_events()
{
  if (n_written_across_all_ranks == 0) {
    // If there are no nonzero events that have been written, there is no need
    // to increment the number of trial events. The result is zero in any case.
    return;
  }

  Mpi::all_sum_sizes(n_zero_events);

  if (n_zero_events == 0)
    return;

  // adjust the n_trials entry of the last non-zero event to account for the
  // remaining n_zero_events
  std::vector<double> last_evt_n_zero_events;
  auto evt_data_set {file->getDataSet("events")};
  Selection last_evt_n_zero_events_sel =
      evt_data_set.select({n_written_across_all_ranks - 1, 3}, {1, 1});
  last_evt_n_zero_events_sel.read(last_evt_n_zero_events, transfer_props);
  last_evt_n_zero_events[0] += static_cast<double>(n_zero_events);
  last_evt_n_zero_events_sel.write(last_evt_n_zero_events, transfer_props);

  n_zero_events = 0;
}

void Hdf5_writer::flush_cache()
{
    write_cache();
    clear_cache();
}

void Hdf5_writer::write_cache()
{
  const size_t n {evt_cache.size()};

#if MPI_FOUND && H5_HAVE_PARALLEL
  const int rank {Mpi::rank()};
  std::vector<int> n_all(Mpi::n_ranks(), n);
  // TODO: get rid of gather operations, which scale linearly with the number of
  // nodes (note that Sherpa currently uses the same algorithm, i.e. suffers
  // from the same problem, and the measurements seemed to indicate that this
  // strategy is fine up to a few thousand ranks, but nonetheless ...)
  Mpi::all_gather_i(n_all);
  const size_t n_total = std::reduce(n_all.begin(), n_all.end());
  size_t relative_evt_idx = std::reduce(n_all.begin(), n_all.begin() + rank);
  for (auto& evt : evt_cache) {
    evt[2] += relative_evt_idx * n_ptcl_max;
  }
#else
  const size_t n_total {n};
  const size_t relative_evt_idx {0};
#endif

  if (n_total == 0)
    return;

  auto evt_data_set {file->getDataSet("events")};
  auto ptcl_data_set {file->getDataSet("particles")};

  evt_data_set.resize({n_written_across_all_ranks + n_total, 10});
  ptcl_data_set.resize(
      {(n_written_across_all_ranks + n_total) * n_ptcl_max, 13});

  const size_t absolute_evt_idx {n_written_across_all_ranks + relative_evt_idx};

  evt_data_set
    .select({absolute_evt_idx, 0}, {n, 10})
    .write(evt_cache, transfer_props);

  ptcl_data_set
    .select({absolute_evt_idx * n_ptcl_max, 0}, {n * n_ptcl_max, 13})
    .write(ptcl_cache, transfer_props);

  n_written_across_all_ranks += n_total;
}

void Hdf5_writer::clear_cache()
{
  evt_cache.clear();
  ptcl_cache.clear();
  n_cached_batches = 0;
}
