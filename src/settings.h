// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_SETTINGS_H
#define PEPPER_SETTINGS_H

#include "helicity_configurations.h"
#include "cuts.h"
#include "print.h"
#include <filesystem>
#include <memory>
#include <mini/ini.h>
#include <string>
#include <unordered_map>
#include <vector>

struct Phase_space_settings {
  struct Chili_settings {
    int n_s_channels {99};
    int start_vegas_bins {100};
    int max_vegas_bins {100};
    bool should_optimize {true};
    double beta {1.0};
  };

  struct Warmup_settings {
    int n_iter {10};
    double n_nonzero_min {10000.0};
    double n_nonzero_min_growth_factor {1.0};
  };

  std::string generator {"Rambo"};
  Chili_settings generator_settings;
  bool write_cached_results {true};
  bool use_cached_results_if_possible {true};
  std::string cache_path {"pepper_cache"};
  bool integration_info_output_enabled {false};
  bool helicity_integrator_enabled {true};
  Warmup_settings optimisation {7, 5e3, 1.44};
  Warmup_settings integration {3, 45e3, 1.0};

  std::string bias {"None"};
};

class Argument_parser;

// user options (set to their default values)
struct Settings {

  Settings();
  void read(int argc, char** argv, const std::string& argprefix = "");
  void write_resolved_settings();
  std::filesystem::path ini_path {"pepper.ini"};

  // main settings
  std::vector<std::string> process_specs;
  long batch_size {1};
  long n_batches {1};
  Helicity_settings helicity_settings;
  Msg::Colourfulness msg_colourfulness {Msg::colourfulness};
  Msg::Verbosity msg_verbosity {Msg::verbosity};
  int device_id {0};
  int seed_offset {0};
  std::string scale2_spec {"m_Z^2"};
  double fixed_alpha_s {-1.0}; /// "-1.0" -> dynamically set using LHAPDF
  double e_cms {14000};
  double max_eps {0.001};

  // dev settings
  bool me2_only {false};
  bool dummy_averaging_factors_enabled {false};
  bool dummy_symmetry_factors_enabled {false};
  bool dummy_colour_factors_allowed {false};
  bool diagnostic_output_enabled {false};
  std::string diagnostic_path {"pepper_diagnostics"};

  // events settings
  std::string event_output_path {""};
  bool event_output_disabled {false};
  bool lhef_event_output_enabled {false};
  bool zero_weight_event_output_enabled {true};
  bool unweighting_disabled {false};
  bool leading_colour_flow_output_enabled {true};

  // phase-space settings
  Phase_space_settings phase_space_settings;

  // PDF settings
  bool pdf_disabled {false};
  std::string pdfset {"NNPDF30_nlo_as_0118"};
  int pdfmember {0};

  // selector settings
  detail::Cuts_data cuts;

  // other settings
  std::filesystem::path data_path;

private:
  std::shared_ptr<mINI::INIFile> ini_file;
  mINI::INIStructure ini_structure;

};

#endif
