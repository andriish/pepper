// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "paths.h"

#include "print.h"
#include "process.h"
#include "settings.h"
#include <filesystem>

namespace fs = std::filesystem;

namespace Paths {

std::string diagnostics;
std::string cache;
std::string temp;
std::string logs;

void create_paths(const Settings& s, const Process& proc)
{
  const std::string process_uid {proc.uid()};

  cache = s.phase_space_settings.cache_path + "/" + process_uid;
  DEBUG("Create cache directory: " + cache);
  fs::create_directories({cache});

  if (s.diagnostic_output_enabled) {
    diagnostics = s.diagnostic_path + "/" + process_uid;
    DEBUG("Create diagnostics directory: " + diagnostics);
    fs::create_directories({diagnostics});
  }

  temp = cache + "/temp";
  fs::create_directories({temp});

  logs = "pepper_logs/" + process_uid;
  fs::create_directories({logs});
}

void erase_cache()
{
  fs::remove_all("pepper_cache");
}

std::string path_with_secondary_extension(const std::string& filepath,
                                          const std::string& extension)
{
  // find first "." from the right
  size_t pos {filepath.rfind('.')};
  assert(pos != std::string::npos);

  // if the extension is ".gz", we want to go further, to the second "."
  if (filepath.substr(pos) == ".gz")
    pos = filepath.rfind('.', pos - 1);
  assert(pos != std::string::npos);

  // insert ".<extension>" before the extension(s) and return
  std::string ret {filepath};
  ret.insert(pos, "." + extension);
  return ret;
}

std::string path_with_rank_pattern(const std::string& filepath)
{
  if (!Mpi::has_workers())
    return filepath;
  return path_with_secondary_extension(filepath, "{rank}");
}

std::string path_with_rank_info(const std::string& filepath)
{
  if (!Mpi::has_workers())
    return filepath;
  return path_with_secondary_extension(filepath, std::to_string(Mpi::rank()));
}

} // namespace Paths
