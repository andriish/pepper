// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_COLOUR_SUMMER_KERNEL_H
#define PEPPER_COLOUR_SUMMER_KERNEL_H

#include "event_handle.h"
#include "kernel_macros.h"
#include <cassert>

template<typename event_data>
KOKKOS_INLINE_FUNCTION void update_me2_kernel(const event_data& evt, int i,
                                            int n_perm,
                                            const Kokkos::View<double*>& colour_factors,
                                            bool cached_summing,
                                            double prefactor)
{
  if (evt.w(i) == 0.0)
    return;

  int coulor_counter {0};
  for (int i1 {0}; i1 < n_perm; ++i1) {
    for (int i2 {i1}; i2 < n_perm; ++i2) {
      const double& c {colour_factors[coulor_counter]};
      double me2 {0.0};
      if (i1 == i2) {
        me2 += evt.me_real(i, i1) * evt.me_real(i, i1) * c;
        me2 += evt.me_imag(i, i1) * evt.me_imag(i, i1) * c;
      }
      else {
        me2 += 2.0 * evt.me_real(i, i1) * evt.me_real(i, i2) * c;
        me2 += 2.0 * evt.me_imag(i, i1) * evt.me_imag(i, i2) * c;
      }
      if (cached_summing) {
        if (i1 == i2) {
          me2 += evt.alt_me_real(i, i1) * evt.alt_me_real(i, i1) * c;
          me2 += evt.alt_me_imag(i, i1) * evt.alt_me_imag(i, i1) * c;
        }
        else {
          me2 +=
              2.0 * evt.alt_me_real(i, i1) * evt.alt_me_real(i, i2) * c;
          me2 +=
              2.0 * evt.alt_me_imag(i, i1) * evt.alt_me_imag(i, i2) * c;
        }
      }
      evt.me2(i) += prefactor * me2;
      coulor_counter++;
    }
  }
}
DEFINE_CUDA_KERNEL_NARGS_4(update_me2_kernel, int, n_perm, const Kokkos::View<double*>&, c,
                           bool, cached_summing, double, prefactor)

template<typename event_data>
KOKKOS_INLINE_FUNCTION void select_leading_colour_configuration_kernel(
    const event_data& evt, int i, int n_perm,
    const Kokkos::View<int*>& leading_colour_factors_mask, bool cached_summing)
{
  if (evt.w(i) == 0.0)
    return;

  // calculate ME2_j for the j'th permutation, if it has a non-vanishing
  // leading-colour contribution; in order to save storage, we'll
  // re-use the me_real storage and write the ME2_j into it
  double sum {0.0};
  for (int j {0}; j < n_perm; ++j) {
    if (!leading_colour_factors_mask[j])
      continue;
    evt.me_real(i, j) = square(evt.me_real(i, j)) + square(evt.me_imag(i, j));
    if (cached_summing) {
      evt.me_real(i, j) +=
          square(evt.alt_me_real(i, j)) + square(evt.alt_me_imag(i, j));
    }
    sum += evt.me_real(i, j);
  }

  // randomly pick one permutation with weights according to the relative ME2_j
  // size
  const double r {sum * evt.r_lc_perm(i)};
  sum = 0.0;
  int active_perm {-1};
  for (int j {0}; j < n_perm; ++j) {
    if (!leading_colour_factors_mask[j])
      continue;
    sum += evt.me_real(i, j);
    if (r < sum) {
      active_perm = j;
      break;
    }
  }
  assert(active_perm >= 0);
  
  evt.active_lc_conf_idx(i) = active_perm;
}
DEFINE_CUDA_KERNEL_NARGS_3(select_leading_colour_configuration_kernel, int,
                           n_perm, const Kokkos::View<double*>&, leading_colour_factors_mask,
                           bool, cached_summing)

#endif
