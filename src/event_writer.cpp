// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "event_writer.h"
#include "debug_writer.h"
#include "mpi.h"
#include "settings.h"
#include "utilities.h"

#if HDF5_FOUND
#include "hdf5_writer.h"
#endif

#if HepMC3_FOUND
#include "hepmc3_writer.h"
#include "lhef_writer.h"
#endif


Event_output_format detect_event_output_format(const std::string& filepath)
{
  if (case_insensitive_ends_with(filepath, ".debug"))
    return Event_output_format::debug;
  if (case_insensitive_ends_with(filepath, ".hepmc3") ||
      case_insensitive_ends_with(filepath, ".hepmc3.gz"))
    return Event_output_format::hepmc3;
  if (case_insensitive_ends_with(filepath, ".lhef") ||
      case_insensitive_ends_with(filepath, ".lhef.gz"))
    return Event_output_format::lhef;
  if (case_insensitive_ends_with(filepath, ".hdf5"))
    return Event_output_format::hdf5;
  throw std::invalid_argument {"`" + filepath +
                               "' has no known event output extension."
                               " Use `.hepmc3[.gz]`, `.lhef[.gz]` or `.hdf5` "
                               "(or `.debug` for debugging purposes)."};
}

std::unique_ptr<Event_writer> create_event_writer(const Settings& s,
                                                  const SM_parameters& sm)
{
  (void)sm;
  if (s.event_output_disabled)
    return nullptr;
  if (!s.event_output_path.empty()) {
    Event_output_format format =
        detect_event_output_format(s.event_output_path);
    if (format == Event_output_format::debug) {
      return std::make_unique<Debug_writer>(s.event_output_path, s);
    }
    if (format == Event_output_format::hdf5) {
#if HDF5_FOUND
      return std::make_unique<Hdf5_writer>(s.event_output_path, s, sm);
#else
      throw std::invalid_argument {
        "Can not write to `" + s.event_output_path +
          "' because HDF5 has not been found at configuration."};
#endif
    }
    if (format == Event_output_format::hepmc3) {
#if HepMC3_FOUND
      return std::make_unique<Hepmc3_writer>(s.event_output_path, s, sm);
#else
      throw std::invalid_argument {
        "Can not write to `" + s.event_output_path +
          "' because HepMC3 has not been found at configuration."};
#endif
    }
    if (format == Event_output_format::lhef) {
#if HepMC3_FOUND
      return std::make_unique<Lhef_writer>(s.event_output_path, s, sm);
#else
      throw std::invalid_argument {
        "Can not write to `" + s.event_output_path +
          "' because HepMC3 has not been found at configuration."};
#endif
    }
    return nullptr;
  }
  // For now, writing events to cout is only supported by HepMC3/LHEF ...
#if HepMC3_FOUND
  // ... however, even then it only makes sense if cout is not shared with
  // worker nodes.
  if (!Mpi::has_workers()) {
    if (s.lhef_event_output_enabled)
      return std::make_unique<Lhef_writer>(std::cout, s, sm);
    return std::make_unique<Hepmc3_writer>(std::cout, s, sm);
  }
  VERBOSE("Disabling event output to cout"
          " since this is an MPI job with workers.");
#endif
  return nullptr;
}
