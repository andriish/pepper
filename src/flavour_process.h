// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_PROCESS_H
#define PEPPER_PROCESS_H

#include "sm.h"

#include <map>
#include <string>
#include <vector>

struct Helicity_settings;
struct MC_result;
struct Settings;

class Flavour_process {

public:
  Flavour_process(const std::string& proc_str);
  void add_flavour_channel(std::string);
  Ptcl_num operator[](int i) const { return particles[i]; }
  Ptcl_num particle_all_outgoing(int i) const;
  int n_ptcl() const { return n; };
  int n_qcd() const { return _n_gluons + 2 * _n_quark_pairs; };
  int n_gluons() const { return _n_gluons; };
  int n_photons() const { return _n_photons; };
  int n_quark_pairs() const { return _n_quark_pairs; };
  int n_lepton_pairs() const { return _n_lepton_pairs; };
  std::map<Ptcl_num, int> quark_pair_counts() const;
  std::string qcd_only_uid() const;
  std::string uid() const;
  std::string particle_name(int i) const
  {
    return string_from_particle(particles[i]);
  }
  double averaging_factor(const Helicity_settings&) const;
  double coupling_factor(double g_s, double g_em) const;
  double initial_state_symmetry_factor() const;
  double final_state_symmetry_factor() const;
  int n_channels() const { return _n_channels; };
  Ptcl_num unmapped_ptcl(int channel, int p) const
  {
    return particles[channel * n + p];
  }

  friend std::ostream& operator<<(std::ostream& o, const Flavour_process& p)
  {
    return o << p.uid();
  }

  std::string spec() const { return particles_to_process_spec_n(particles, n); }

  void add_helicity_training_data(int i, double weight)
  {
    per_helicity_results[i].sum += weight;
    per_helicity_results[i].sum2 += weight * weight;
    per_helicity_results[i].n_trials += 1;
    if (weight != 0.0) {
      per_helicity_results[i].n_nonzero_after_cuts += 1;
      per_helicity_results[i].n_nonzero += 1;
    }
  }

  std::vector<MC_result>& helicity_mc_results() { return per_helicity_results; }
  double helicity_selection_weight(int i) const
  {
    return _hel_selection_weights[i];
  }
  double& helicity_selection_weight(int i)
  {
    return _hel_selection_weights[i];
  }
  void init_default_selection_weights();
  void init_default_selection_weights(int);
  void set_selection_weights_from_variances();

private:
  void parse_proc_string(std::string proc_str);
  int n {0};
  int _n_gluons {0};
  int _n_photons {0};
  int _n_quark_pairs {0};
  int _n_lepton_pairs {0};
  int _n_channels {0};
  // _n_channels x n elements, where m is the number of flavour process
  // channels, and each successive series of n elements denotes the particles
  // of one mapped process
  std::vector<Ptcl_num> particles;
  std::vector<MC_result> per_helicity_results;
  std::vector<double> _hel_selection_weights;
};

#endif
