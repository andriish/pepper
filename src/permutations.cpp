// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "permutations.h"

#include "flavour_process.h"
#include "math.h"
#include "melia_dyck_permutator.h"
#include "melia_dyck_words.h"
#include "print.h"
#include "sm.h"

#include <algorithm>
#include <cassert>
#include <numeric>
#include <stack>

Permutations::Permutations(const Flavour_process& proc):
    _perm_size {proc.n_ptcl()},
    d_permutations(Kokkos::ViewAllocateWithoutInitializing("d_permutations"),0)
{
  Melia_dyck_words dyck_words {proc};

  // number of elements for a given Melia permutation (= number of QCD
  // particles)
  const int melia_perm_size {proc.n_qcd()};

  // Calculate the number of Melia QCD permutations.
  int symm_fac = 1;
  for (const auto& [key, value] : proc.quark_pair_counts())
    symm_fac *= factorial(value);
  n_qcdperm =
      factorial(melia_perm_size - 2) / factorial(proc.n_quark_pairs()) * symm_fac;

  // We do not know a priori how many permutations we will have in the end (we
  // did not come up with a formula that also takes into account the QED
  // insertions correctly). But let's at least reserve enough space to store
  // all QCD permutations, since that gives us a lower limit.
  permutations.reserve(n_qcdperm * dyck_words()[0].size());

  // There are exactly n_qcdperm non-trivial signs (see below), so we can use
  // resize().
  signs.resize(n_qcdperm);

  int melia_perm_idx {0};
  // Iterate dyck words, as a first step to find all permutations required by
  // the Melia basis.
  for (const auto& dyck_array : dyck_words()) {
    auto base_to_current_dyck_perm = dyck_words.from_base_perm(dyck_array);
    // Permute flavoured dyck word accordingly
    std::vector<Ptcl_num> flavour_dyck_array(melia_perm_size, -1);
    for (int i {0}; i < melia_perm_size; ++i) {
      flavour_dyck_array[base_to_current_dyck_perm[i]] =
          dyck_words.flavoured_base()[i];
    }
    // Now iterate over permutations within each dyck word. With that, all QCD
    // permutations required by the Melia basis will be processed.
    Melia_dyck_permutator dyck_permutator {melia_perm_size, proc.n_quark_pairs(),
                                           dyck_array, flavour_dyck_array};
    do {
      const auto quark_pair_and_gluon_perm = dyck_permutator.current();
      const auto melia_perm =
          concat_perms(dyck_words.proc_to_base_perm(),
                       base_to_current_dyck_perm, quark_pair_and_gluon_perm);

      // Now, given the Melia QCD permutation, append the permutations of all
      // particles corresponding to it, including non-coloured particles.
      append_full_permutations_for_melia_permuation(proc, melia_perm);

      // Calculate sign for the permutation, relative to the ordering of the
      // base dyck word. Note that it's sufficient to take into account the
      // Melia QCD permutation, because for now we assume that only a single
      // lepton pair is added on top of the QCD content of the process, hence
      // there can be no additional sign from the QED sector.
      std::vector<int> combined(melia_perm_size, -1);
      for (int i(0); i < melia_perm_size; ++i) {
        combined[quark_pair_and_gluon_perm[base_to_current_dyck_perm[i]]] = i;
      }
      int nswaps = 0;
      while (true) {
        int rswaps = 0;
        int tswaps = 0;
        for (int id(0); id < melia_perm_size - 1; ++id) {
          if (combined[id] > combined[id + 1]) {
            std::swap(combined[id], combined[id + 1]);
            tswaps++;
            if (dyck_words.base()[combined[id]] != GLUON &&
                dyck_words.base()[combined[id + 1]] != GLUON)
              rswaps++; // only add sign for spinor swap
          }
        }
        if (tswaps == 0)
          break;
        else
          nswaps += rswaps;
      }
      signs[melia_perm_idx++] = (1 - 2 * (nswaps % 2));
    } while (dyck_permutator.advance());
  } // end permutation loop

  Kokkos::resize(d_permutations,permutations.size());
  Kokkos::View<int*,Kokkos::HostSpace> h_p(&permutations[0],permutations.size());
  Kokkos::deep_copy(d_permutations,h_p);
}

void Permutations::append_full_permutations_for_melia_permuation(
    const Flavour_process& proc, const std::vector<int>& melia_perm)
{
  // External photons are not supported yet.
  assert(proc.n_photons() == 0);

  // Treat trivial zero-lepton case.
  if (proc.n_lepton_pairs() == 0) {
    for (size_t i {0}; i < melia_perm.size(); ++i) {
      permutations.push_back(melia_perm[i]);
    }
    n_perm++;
    qed_permutation_counts.push_back(1);
    return;
  }

  // NOTE: For now, we assume here that there is only one lepton pair.
  assert(proc.n_lepton_pairs() == 1);

  // First determine where to insert the lepton pair.
  std::vector<size_t> insertion_points;
  int quark_counter {0};
  for (size_t i {2}; i < melia_perm.size(); ++i) {
    if (proc[melia_perm[i]] == GLUON) {
      if (quark_counter % 2 == 0) {
        insertion_points.push_back(i);
      }
    }
    else {
      if (quark_counter % 2 == 0) {
        insertion_points.push_back(i);
      }
      quark_counter++;
    }
  }
  insertion_points.push_back(melia_perm.size());

  // Now, for each insertion, append the full permutation.
  for (auto j : insertion_points) {
    for (size_t i {0}; i < melia_perm.size(); ++i) {
      permutations.push_back(melia_perm[i]);
      if (j == i + 1) {
        // Find and append lepton.
        int idx;
        for (idx = 0; idx < proc.n_ptcl(); ++idx) {
          const Ptcl_num p {proc.particle_all_outgoing(idx)};
          if (is_lepton(p) && !is_antiparticle(p))
            break;
        }
        permutations.push_back(idx);
        // Find and append positron.
        for (idx = 0; idx < proc.n_ptcl(); ++idx) {
          const Ptcl_num p {proc.particle_all_outgoing(idx)};
          if (is_lepton(p) && is_antiparticle(p))
            break;
        }
        permutations.push_back(idx);
      }
    }
  }

  n_perm += insertion_points.size();
  qed_permutation_counts.push_back(insertion_points.size());
}

std::vector<int> Permutations::mapped_qcd_permutation_for_flavour_process(
    const Flavour_process& flav_proc, int flav_channel, int qcd_perm_idx) const
{
  // copy permutation (which is valid for the base flavour channel)
  const int start {get_index_for_qcd_permutation_group(qcd_perm_idx) *
                   _perm_size};
  std::vector<int> qcdperm(_perm_size);
  std::copy_n(permutations.begin() + start, _perm_size, qcdperm.begin());

  // a stack of misplaced closing Dyck bracket indizes for each flavour
  std::map<Ptcl_num, std::stack<int>> misplaced_index_map;

  // the number of currently open Dyck brackets for each flavour
  std::map<Ptcl_num, int> n_open_brackets_map;

  for (int p {0}; p < _perm_size; ++p) {

    // retrieve unmapped flavour
    Ptcl_num ptcl {flav_proc.unmapped_ptcl(flav_channel, qcdperm[p])};

    // we only care about quarks
    if (is_quark(ptcl)) {

      // enforce all-outgoing convention
      if (qcdperm[p] < 2)
        ptcl = antiparticle(ptcl);

      if (is_antiparticle(ptcl)) {
        // anti-quarks
        if (n_open_brackets_map[-ptcl] == 0) {
          // there is no matching opening bracket for this antiquark, remember
          // this
          misplaced_index_map[-ptcl].push(p);
        }
        else {
          // there is a matching opening bracket, close it
          n_open_brackets_map[-ptcl]--;
        }
      }
      else {
        // quarks
        if (!misplaced_index_map[ptcl].empty()) {
          // there is a matching misplaced antiquark closing bracket;
          // swap the brackets to obtain the right ordering
          std::swap(qcdperm[misplaced_index_map[ptcl].top()], qcdperm[p]);

          // this bracket is now considered properly closed after the swap
          misplaced_index_map[ptcl].pop();
        }
        else {
          // there is no matching misplaced closing bracket; so remember we open
          // here
          n_open_brackets_map[ptcl]++;
        }
      }
    }
  }

  return qcdperm;
}
