// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_DYCK_PERMUTATION_H
#define PEPPER_DYCK_PERMUTATION_H

#include <map>
#include <vector>

class Melia_dyck_permutator {
public:
  // The first dyck array must have distinct quark labels, while the second
  // should have flavour labels (which can occur more than once of course).
  Melia_dyck_permutator(int n, int k, const std::vector<int>&,
                  const std::vector<int>&);

  bool advance();
  std::vector<int> current();

private:
  std::vector<int> dyck_array;
  int n_inner_quark_pairs, n_gluons;
  bool gluon_only;
  std::map<int, std::vector<int>> identical_antiquark_orderings,
      identical_antiquark_indizes;
  std::vector<int> quark_pair_ordering, gluon_ordering;
  std::vector<int> gluon_indizes, quark_indizes, antiquark_indizes;
  std::map<int, std::vector<int>> fermion_lines;
};

#endif
