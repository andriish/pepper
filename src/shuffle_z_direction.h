// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_SHUFFLE_Z_DIRECTION_H
#define PEPPER_SHUFFLE_Z_DIRECTION_H

#include "event_handle.h"
#include "rng.h"

void shuffle_z_direction_n(Event_handle& evt, Rng& rng, int n)
{
  for (int i {0}; i < n; ++i) {
    if (evt.host.w(i) == 0)
      continue;
    if (rng.d() < 0.5) {
      for (int p {0}; p < evt.host.n_ptcl; p++) {
        evt.host.pz(i, p) =  evt.host.pz(i, p) * -1.0;
        // Uncommenting the following line would make this into a 180° rotation
        // about the y axis; however, this should only be relevant for
        // polarised beams. We currently do not support this anyway.
        // evt.px(i, p) *= -1.0;
      }
    }
  }
}

#endif
