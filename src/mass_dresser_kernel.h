// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_MASS_DRESSER_KERNEL_H
#define PEPPER_MASS_DRESSER_KERNEL_H


#include "event_handle.h"
#include "kernel_macros.h"
#include "rambo.h"
#include <cmath>
#include <vector>

template<typename event_type,typename data_type>
KOKKOS_INLINE_FUNCTION void
mass_dresser_transform_momenta_kernel(const event_type& evt, int i,
                                      const data_type& in_data,
                                      const sm_properties& smprop)
{
  const auto& data = in_data();
  // Transform initial-state momenta
  evt.pz(i, 0) = -std::sqrt(evt.pz(i, 0) * evt.pz(i, 0) - 
                            smprop.mass(evt.ptcl(0)) * smprop.mass(evt.ptcl(0)));
  evt.pz(i, 1) = std::sqrt(evt.pz(i, 1) * evt.pz(i, 1) - 
                           smprop.mass(evt.ptcl(1)) * smprop.mass(evt.ptcl(1)));


  // NOTE: Use final-state rapidity data to store the original energies
  // determined by the massless RAMBO, because they will anyway be be
  // overwritten when the cuts are evaluated, *after* RAMBO has concluded. We
  // can also be sure that a cudaDeviceSynchronize will happen in between,
  // because the cuts kernel will need this to access the final momenta.
  for (int p {2}; p < evt.n_ptcl; p++) {
    evt.y(i, p - 2) = evt.e(i, p) * evt.e(i, p);
  }

  // Numerically determine chi of (4.2) in Computer Physics Communications 40
  // (1986) 359—373
  double chi = data.chi_start;
  int it {0};
  while (true) {
    double f0 {-data.e_cms};
    double g0 {0.0};
    const double chi2 {chi * chi};
    for (int p {2}; p < evt.n_ptcl; p++) {
      evt.e(i, p) =
          std::sqrt(smprop.mass(evt.ptcl(p)) * smprop.mass(evt.ptcl(p)) + chi2 * evt.y(i, p - 2));
      f0 += evt.e(i, p);
      g0 += evt.y(i, p - 2) / evt.e(i, p);
    }
    if (std::abs(f0) < data.accuracy)
      break;
    it++;
    if (it > data.itmax)
      break;
    chi -= f0 / (chi * g0);
  }

  // Transform massless momenta to massive momenta according to (4.2) in
  // Computer Physics Communications 40 (1986) 359—373
  for (int p {2}; p < evt.n_ptcl; p++) {
    evt.px(i, p) *= chi;
    evt.py(i, p) *= chi;
    evt.pz(i, p) *= chi;
  }
}
DEFINE_CUDA_KERNEL_NARGS_2(mass_dresser_transform_momenta_kernel,
                           const detail::Mass_dresser_data&, data,
                           const sm_properties&, smprop)


template<typename event_type,typename data_type>
KOKKOS_INLINE_FUNCTION void
mass_dresser_update_weights_kernel(const event_type& evt, int i,
                                   const data_type& in_data)
{
  const auto& data = in_data();
  if (evt.w(i) == 0.0)
    return;
  // Calculate terms of (4.11) in Computer Physics Communications 40 (1986)
  // 359—373
  double ksum {0.0};
  double kprod {1.0};
  double k2sum {0.0};
  for (int p {2}; p < evt.n_ptcl; p++) {
    const double k {std::sqrt(evt.px(i, p) * evt.px(i, p) +
                              evt.py(i, p) * evt.py(i, p) +
                              evt.pz(i, p) * evt.pz(i, p))};
    ksum += k;
    kprod *= k / evt.e(i, p);
    k2sum += k * k / evt.e(i, p);
  }
  // NOTE: The last factor, the centre-of-mass energy, is not present in the
  // above-mentioned (4.11). However, without it, the overall factor is several
  // orders of magnitude away from 1.0, and, even more importantly, not
  // dimensionless. It's unclear why it is missing in CPC 40 (1986). It is
  // included however in the implementation in Sherpa (`Rambo::MassiveWeight`).
  evt.w(i) = evt.w(i) * (pow(ksum / data.e_cms, 2 * (evt.n_ptcl - 2) - 3) * kprod / k2sum * data.e_cms);

  // NOTE: Sherpa implements the calculation using exp(log(...)). Perhaps due
  // to numerical reasons, but we have not seen an issue with the above
  // version. For reference, the Sherpa version would like the following:
  //    evt.w(i) *= exp(log(ksum / data.e_cms) * (2 * (evt.n_ptcl - 2) - 3)
  //                    + log(kprod / k2sum * data.e_cms));
}
DEFINE_CUDA_KERNEL_NARGS_1(mass_dresser_update_weights_kernel,
                           const detail::Mass_dresser_data&, data)

#endif
