// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "helicity_configurations_mask.h"
#include "event_handle.h"
#include "helicity_configurations.h"
#include "print.h"
#include <iomanip>

void Helicity_configurations_mask::update(Event_handle& evt) const
{
  std::vector<int> acthel;
  for (size_t i {0}; i < mask.size(); i++) {
    if (mask[i]) {
      acthel.push_back(i);
    }
  }
  Kokkos::View<int const*,Kokkos::HostSpace> h_active_hels(&acthel[0],acthel.size());
  Kokkos::resize(evt.device.active_hels,acthel.size());
  Kokkos::deep_copy(evt.device.active_hels,h_active_hels);
  evt.host.active_hels = Kokkos::create_mirror_view(Kokkos::HostSpace(),evt.device.active_hels);
  evt.host.n_active_helicities() = (mask.size() - n_disabled);
  Kokkos::deep_copy(evt.device.n_active_helicities,evt.host.n_active_helicities);
}

void Helicity_configurations_mask::debug_enabled_helicities(
    const Event_handle& evt) const
{
  if (Msg::verbosity < Msg::Verbosity::debug)
    return;
  DEBUG("Enabled helicity configurations:");
  for (size_t i {0}; i < mask.size(); ++i) {
    print(MARK_DEBUG, " - ", std::setw(3), i, " (");
    for (int p {0}; p < evt.host.n_ptcl; ++p) {
      print(calculate_helicity(i, p));
    }
    println(") = ", mask[i]);
  }
  DEBUG(mask.size() - n_disabled, " of ", mask.size(),
        " helicities are active.");
}
