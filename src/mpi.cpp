// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "mpi.h"
#include "print.h"

#include <climits>
#include <cstdint>

#if CHILI_ENABLE_MPI
#include "compiler_warning_macros.h"
PEPPER_SUPPRESS_COMMON_WARNINGS_PUSH
#include "Chili/Tools/MPI.hh"
PEPPER_SUPPRESS_COMMON_WARNINGS_POP
#endif

// select MPI type for sending size_t data
#if SIZE_MAX == UCHAR_MAX
  #define PEPPER_MPI_SIZE_T MPI_UNSIGNED_CHAR
#elif SIZE_MAX == USHRT_MAX
  #define PEPPER_MPI_SIZE_T MPI_UNSIGNED_SHORT
#elif SIZE_MAX == UINT_MAX
  #define PEPPER_MPI_SIZE_T MPI_UNSIGNED
#elif SIZE_MAX == ULONG_MAX
  #define PEPPER_MPI_SIZE_T MPI_UNSIGNED_LONG
#elif SIZE_MAX == ULLONG_MAX
  #define PEPPER_MPI_SIZE_T MPI_UNSIGNED_LONG_LONG
#else
  #error "Unexpected SIZE_MAX definition."
#endif

void finalise()
{
#if MPI_FOUND
#if CHILI_ENABLE_MPI
  // finalise via Chili (implicitly via its MPI handler's dtor)
#else
  MPI_Finalize();
#endif
#endif
}

void Mpi::initialise_and_register_finalise()
{
#if MPI_FOUND
#if CHILI_ENABLE_MPI
  // init via Chili (such that it can do its registration of MPI types)
  auto& mpi = chili::MPIHandler::Instance();
  mpi.Init(0, NULL);
#else
  // init direcly
  MPI_Init(0, NULL);
#endif
  std::atexit(finalise);
#endif
}

int Mpi::rank()
{
#if MPI_FOUND
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  return world_rank;
#else
  return main_rank;
#endif
}

int Mpi::n_ranks()
{
#if MPI_FOUND
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  return world_size;
#else
  return 1;
#endif
}

void Mpi::cast_d(double& d)
{
#if MPI_FOUND
  MPI_Bcast(&d, 1, MPI_DOUBLE, Mpi::main_rank, MPI_COMM_WORLD);
#else
  (void)d;
#endif
}

void Mpi::cast_i(int& d)
{
#if MPI_FOUND
  MPI_Bcast(&d, 1, MPI_INT, Mpi::main_rank, MPI_COMM_WORLD);
#else
  (void)d;
#endif
}

void Mpi::cast_d(std::vector<double>& v)
{
#if MPI_FOUND
  MPI_Bcast(v.data(), v.size(), MPI_DOUBLE, Mpi::main_rank, MPI_COMM_WORLD);
#else
  (void)v;
#endif
}

void Mpi::sum_d(std::vector<double>& a)
{
#if MPI_FOUND
  if (is_main_rank())
    MPI_Reduce(MPI_IN_PLACE, a.data(), a.size(), MPI_DOUBLE, MPI_SUM,
               Mpi::main_rank, MPI_COMM_WORLD);
  else
    MPI_Reduce(a.data(), a.data(), a.size(), MPI_DOUBLE, MPI_SUM,
               Mpi::main_rank, MPI_COMM_WORLD);
#else
  (void)a;
#endif
}

void Mpi::max_d(double& d)
{
#if MPI_FOUND
  if (is_main_rank())
    MPI_Reduce(MPI_IN_PLACE, &d, 1, MPI_DOUBLE, MPI_MAX, Mpi::main_rank,
               MPI_COMM_WORLD);
  else
    MPI_Reduce(&d, &d, 1, MPI_DOUBLE, MPI_MAX, Mpi::main_rank, MPI_COMM_WORLD);
#else
  (void)d;
#endif
}

bool Mpi::equal_i(int i)
{
#if MPI_FOUND
  std::array<int, 2> p {-i, i};
  MPI_Allreduce(MPI_IN_PLACE, p.data(), 2, MPI_INT, MPI_MIN, MPI_COMM_WORLD);
  return (p[0] == -p[1]);
#else
  (void)i;
  return true;
#endif
}

void Mpi::all_sum_d(double& d)
{
#if MPI_FOUND
  MPI_Allreduce(MPI_IN_PLACE, &d, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#else
  (void)d;
#endif
}

void Mpi::sum_d(double& d)
{
#if MPI_FOUND
  if (is_main_rank())
    MPI_Reduce(MPI_IN_PLACE, &d, 1, MPI_DOUBLE, MPI_SUM, Mpi::main_rank,
               MPI_COMM_WORLD);
  else
    MPI_Reduce(&d, &d, 1, MPI_DOUBLE, MPI_SUM, Mpi::main_rank, MPI_COMM_WORLD);
#else
  (void)d;
#endif
}

void Mpi::all_sum_sizes(size_t& i)
{
#if MPI_FOUND
  MPI_Allreduce(MPI_IN_PLACE, &i, 1, PEPPER_MPI_SIZE_T, MPI_SUM,
                MPI_COMM_WORLD);
#else
  (void)i;
#endif
}

void Mpi::all_gather_i(std::vector<int>& v)
{
#if MPI_FOUND
  MPI_Allgather(MPI_IN_PLACE, 0, MPI_DATATYPE_NULL, &v[0], 1, MPI_INT, MPI_COMM_WORLD);
#else
  (void)v;
#endif
}
