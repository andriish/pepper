// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "t_channel_integrator.h"

#include "citations.h"
#include "event_handle.h"
#include "flavour_process.h"
#include "phase_space.h"
#include "print.h"
#include "timing.h"
#include "t_channel_kernel.h"
#include "utilities.h"

T_channel::T_channel(const Flavour_process& proc, const Settings& s)
  : Phase_space_generator {s.e_cms},
    n_ptcl {proc.n_ptcl()},
    vegas {n_random_numbers(), 100, 1},
    d_data(Kokkos::ViewAllocateWithoutInitializing("cuts_data")),
    h_data(Kokkos::create_mirror_view(d_data)),
    d_etmin(Kokkos::ViewAllocateWithoutInitializing("etmin"),0),
    d_ms(Kokkos::ViewAllocateWithoutInitializing("ms"),0)
{
  Citations::register_citation("Chili", "Bothmann:2023siu");
  h_data() = s.cuts;
  Kokkos::deep_copy(d_data,h_data);
  
  should_optimise = true;
  type = 1;
  if (s.phase_space_settings.bias != "None") {
    should_bias_phase_space = true;
    biaser =
      create_phase_space_manipulator(s.phase_space_settings.bias);
  }


  const auto& smprops = sm_properties::getInstance();

  // fill masses and et's
  for(int i{0}; i<n_ptcl; ++i) {
    const int p = std::abs(proc[i]);
    if(is_colour_charged(p)) {
      const double m = smprops.hmass(p);
      const double et = std::max(m,s.cuts.pT_min);
      etmin.push_back(et);
      ms.push_back(m*m);
    } else {
      // This is probably not correct for ew particles
      type = 2;
      etmin.push_back(0);
      ms.push_back(0);
    }
  }

  Kokkos::resize(d_etmin,etmin.size());
  Kokkos::View<double*,Kokkos::HostSpace> h_etmin(&etmin[0],etmin.size());
  Kokkos::deep_copy(d_etmin,h_etmin);

  Kokkos::resize(d_ms,ms.size());
  Kokkos::View<double*,Kokkos::HostSpace> h_ms(&ms[0],ms.size());
  Kokkos::deep_copy(d_ms,h_ms);
}

void T_channel::fill_momenta_and_weights(Event_handle& evt) const
{
  vegas.GeneratePoint(evt);
  Kokkos::fence(__func__);
  const auto& _e_cms = e_cms;
  const auto& _d_etmin = d_etmin;
  const auto& _d_ms = d_ms;
  const auto& _d_data = d_data;
  const auto& smprops = sm_properties::getInstance();
  const auto& _type = type;
  RUN_KERNEL(t_channel_fill_momenta_and_weights_kernel, evt, _e_cms, _d_etmin, _d_ms, _d_data,
             _type,smprops);

  if(should_bias_phase_space)
    biaser->fill_biasing_factor(evt);
}

void T_channel::add_training_data(Event_handle& evt)
{
  vegas.AddPoint(evt);
}

void T_channel::optimise()
{
  vegas.Optimize();
}

void T_channel::update_weights(Event_handle& evt) const
{
  Kokkos::Profiling::pushRegion("T_channel::update_weights");
  vegas.GenerateWeight(evt);
  Kokkos::fence("T_channel::update_weights");
  Kokkos::Profiling::popRegion();
}

void T_channel::stop_optimisation()
{
  should_optimise = false;
}
