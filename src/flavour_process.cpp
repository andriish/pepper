// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "flavour_process.h"

#include "pepper/config.h"

#include "helicity_configurations.h"
#include "math.h"
#include "print.h"
#include "settings.h"
#include "utilities.h"
#include <cassert>
#include <iterator>
#include <sstream>
#include <vector>

Flavour_process::Flavour_process(const std::string& proc_str)
{
  parse_proc_string(proc_str);
  init_default_selection_weights();
}

void Flavour_process::add_flavour_channel(std::string proc_str)
{
  // The user can use double underscores instead of an arrow.
  replace_all(proc_str, "__", " -> ");

  // The user can use underscores or whitespace between tokens.
  replace_all(proc_str, "_", " ");

  // Extract tokens from the process string.
  std::istringstream process_iss {proc_str};
  std::vector<std::string> proc_tokens {
      std::istream_iterator<std::string> {process_iss},
      std::istream_iterator<std::string> {}};
  proc_tokens.erase(std::find(proc_tokens.begin(), proc_tokens.end(), "->"));

  // Translate into particle IDs.
  std::transform(
      proc_tokens.begin(), proc_tokens.end(), std::back_inserter(particles),
      [](const std::string& s) -> Ptcl_num { return particle_from_string(s); });

  _n_channels += 1;
}

void Flavour_process::parse_proc_string(std::string proc_str)
{
  add_flavour_channel(proc_str);

  // Count bosons and fermion pairs.
  n = particles.size();
  for (int i {0}; i < n; ++i) {
    Ptcl_num ptcl = particle_all_outgoing(i);
    if (ptcl == GLUON) {
      _n_gluons++;
    }
    else if (ptcl == PHOTON) {
      _n_photons++;
    }
    else if (ptcl > 0) {
      if (is_quark(ptcl)) {
        _n_quark_pairs++;
      }
      else if (is_lepton(ptcl)) {
        _n_lepton_pairs++;
      }
    }
  }
}

Ptcl_num Flavour_process::particle_all_outgoing(int i) const
{
  if (i < 2)
    return antiparticle(particles[i]);
  else
    return particles[i];
}

std::map<Ptcl_num, int> Flavour_process::quark_pair_counts() const
{
  std::map<int, int> counts;
  for (int i {0}; i < n; ++i) {
    const int ptcl {particle_all_outgoing(i)};
    if (is_quark(ptcl) && !is_antiparticle(ptcl)) {
      counts[ptcl]++;
    }
  }
  return counts;
}

std::string Flavour_process::qcd_only_uid() const
{
  std::string id = "2_" + std::to_string(n_qcd() - 2);
  for (int i {0}; i < n; ++i) {
    if (is_colour_charged(particles[i]))
      id += "__" + particle_name(i);
  }
  return id;
}

std::string Flavour_process::uid() const
{
  std::string id = "2_" + std::to_string(n - 2);
  for (int i {0}; i < n; ++i) {
    id += "__" + particle_name(i);
  }
  return id;
}

double Flavour_process::averaging_factor(const Helicity_settings& hel_settings) const
{
  double n_colours {1.0};
  for (int i {0}; i < 2; ++i) {
    n_colours *= n_colour_states(particles[i]);
  }
  const double colour_factor {1.0 / n_colours};

  double helicity_factor {1.0};
  if (hel_settings.treatment != Helicity_treatment::sampled) {
    double n_helicities {1.0};
    for (int i {0}; i < 2; ++i) {
      n_helicities *= 2.0;
    }
    helicity_factor = 1.0 / n_helicities;
  }
  else {
    for (int i {0}; i < n - 2; ++i)
      helicity_factor *= 2.0;
  }

  return colour_factor * helicity_factor;
}

double Flavour_process::coupling_factor(double g_s, double g_em) const
{
  double fac {std::pow(g_s, 2 * (n - 2 * _n_lepton_pairs) - 4)};
  fac *= std::pow(g_em, 4 * _n_lepton_pairs);
  return fac;
}

double Flavour_process::initial_state_symmetry_factor() const
{
  if (particles[0] == particles[1])
    return 1.0;
  else
    return 2.0;
}

double Flavour_process::final_state_symmetry_factor() const
{
  std::map<Ptcl_num, int> multiplicities;
  for (int i {2}; i < n; ++i) {
    Ptcl_num p = particles[i];
    if (!multiplicities.insert(std::make_pair(p, 1)).second)
      ++multiplicities[p];
  }
  double fac = {1.0};
  for (auto pair : multiplicities) {
    assert(pair.second > 0);
    fac *= factorial(pair.second);
  }
  return 1.0 / fac;
}

void Flavour_process::init_default_selection_weights()
{
  int n_hels {1};
  for (int i {0}; i < n; i++) {
    n_hels *= 2;
  }
  init_default_selection_weights(n_hels);
}

void Flavour_process::init_default_selection_weights(int n_hels)
{
  _hel_selection_weights.resize(n_hels);
  std::fill(_hel_selection_weights.begin(),
            _hel_selection_weights.end(), 1.0 / n_hels);
}

void Flavour_process::set_selection_weights_from_variances()
{
  // ensure that helicity weights are of appropriate size
  const size_t n_active_hels {per_helicity_results.size()};
  if (_hel_selection_weights.size() != n_active_hels) {
    DEBUG("Helicity selection weight container was not of appropriate size!\n"
          "This might indicate an error in the workflow (optimisation called "
          "before initialization).");
    _hel_selection_weights.resize(n_active_hels);
  }

#if MPI_FOUND
  auto unsummed_per_helicity_results = per_helicity_results;
  for (size_t i {0}; i < n_active_hels; ++i) {
    per_helicity_results[i].all_mpi_sum();
  }
#endif

  // Check if every helicity weight has been reached; if not, skip the
  // optimisation and wait for more points.
  for (size_t i {0}; i < n_active_hels; ++i) {
    if (per_helicity_results[i].n_nonzero == 0) {
#if MPI_FOUND
      // reset results to the ones obtained on this rank only,
      // such that subsequent calls to all_mpi_sum() do not introduce
      // double-counting
      per_helicity_results = unsummed_per_helicity_results;
#endif
      return;
    }
  }

  // compute the updates
  double sum {0};
  for (size_t i {0}; i < n_active_hels; ++i) {
    const auto& res {per_helicity_results[i]};
    const double new_weight {std::pow(res.integrand_stddev(), 2.0 / 3.0)};
    assert(std::isfinite(new_weight));
    _hel_selection_weights[i] = new_weight;
    per_helicity_results[i].clear();
    sum += new_weight;
  }

  // make another pass to stabilise the optimisation
  double sum_v2 {0};
  for (size_t i {0}; i < n_active_hels; ++i) {
    const double new_weight {_hel_selection_weights[i] / sum};

    // Proposed method to stabilise weight updates even further; if we find
    // significant spikes in the integration, we might want to reinsert the
    // following:
    //
    //   new_weight = pow((1-new_weight) / std::log(1./new_weight), beta);
    //
    // Here, beta is a tunable parameter, e.g. beta = 1. Otherwise, we can use
    // the simple method below.

    _hel_selection_weights[i] = new_weight;
    sum_v2 += _hel_selection_weights[i];
  }

  // normalise again
  for (size_t i {0}; i < n_active_hels; ++i)
    _hel_selection_weights[i] /= sum_v2;
}
