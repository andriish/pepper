// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "chili_interface.h"
#include "citations.h"
#include "cuts.h"
#include "mpi.h"
#include "paths.h"
#include "run_statistics.h"
#include "settings.h"
#include "sm.h"

#include <filesystem>
#include <limits>

namespace fs = std::filesystem;

chili::Model create_chili_model()
{
  chili::Model model(Particle_combiner {});

  for (Ptcl_num i {DOWN_QUARK}; i <= TOP_QUARK; ++i) {
    model.Mass(i) = mass(i);
    model.Width(i) = 0.0;
  }

  for (Ptcl_num i {GLUON}; i <= Z; ++i) {
    model.Mass(i) = mass(i);
    model.Width(i) = width(i);
  }

  for (Ptcl_num i {ELECTRON}; i <= TAU_NEUTRINO; ++i) {
    model.Mass(i) = mass(i);
    model.Width(i) = 0.0;
  }

  return model;
}

/** \brief Create and return Chili-internal cuts
 *
 *  Creates and returns the internal cuts of the Chili phase-space generator.
 *  Chili has internal $\Delta R$ cuts, $p_T$ cuts and internal $s_\text{min}$
 *  cuts for the internal propagators.
 */
chili::Cuts create_chili_cuts(const chili::Model& model,
                              const detail::Cuts_data& cuts,
                              const std::vector<int>& particles)
{
  chili::Cuts internal_cuts;

  // add internal s_external cuts for outgoing particles
  for (size_t i {2}; i < particles.size(); ++i)
    internal_cuts.sexternal.push_back(square(model.Mass(particles[i])));

  // add internal DeltaR cuts
  for (auto f1 : particles) {
    for (auto f2 : particles) {
      const bool v1 = is_jet(f1) || f1 == PHOTON;
      const bool v2 = is_jet(f2) || f1 == PHOTON;
      if (v1 && v2)
        internal_cuts.deltaR[{f1, f2}] = cuts.dR_min;
      else
        internal_cuts.deltaR[{f1, f2}] = 0.0;
    }
  }

  // add internal DeltaR cuts also with particles that can be generated
  // internally, but are not part of the external particles (e.g. gluons via qqb
  // -> g)
  for (auto f1 : particles) {
    const bool v1 = is_jet(f1) || f1 == PHOTON;
    if (v1) {
      internal_cuts.deltaR[{21, f1}] = cuts.dR_min;
      internal_cuts.deltaR[{f1, 21}] = cuts.dR_min;
    }
    else {
      internal_cuts.deltaR[{21, f1}] = 0.0;
      internal_cuts.deltaR[{f1, 21}] = 0.0;
    }
    internal_cuts.deltaR[{23, f1}] = 0.0;
    internal_cuts.deltaR[{f1, 23}] = 0.0;
  }
  internal_cuts.deltaR[{21, 21}] = cuts.dR_min;

  // add internal s_min cuts
  for (size_t i {0}; i < particles.size(); ++i)
    internal_cuts.smin[1 << i] = square(model.Mass(particles[i]));

  // add internal s_min cut for the boson splitting into a lepton pair
  int i1 {-1};
  int i2 {-1};
  for (size_t i {0}; i < particles.size(); ++i) {
    if (is_lepton(particles[i])) {
      int idx = 1 << i;
      if (i1 == -1) {
        i1 = idx;
      }
      else {
        i2 = idx;
        break;
      }
    }
  }
  if (i1 != -1 && i2 != -1)
    internal_cuts.smin[i1 | i2] = cuts.ll_m2_min;

  // add internal pT_min cuts
  for (size_t i {0}; i < particles.size(); ++i) {
    const Ptcl_num f1 {particles[i]};
    const bool v1 = is_jet(f1) || f1 == PHOTON;
    if (v1)
      internal_cuts.ptmin[1 << i] = cuts.pT_min;
    else
      internal_cuts.ptmin[1 << i] = 0.;
  }

  // add internal eta_max cuts
  for (size_t i {0}; i < particles.size(); ++i) {
    if (is_jet(particles[i]))
      internal_cuts.etamax[1 << i] = cuts.nu_max;
    else
      internal_cuts.etamax[1 << i] = std::numeric_limits<double>::max();
  }

  return internal_cuts;
}

Chili::Chili(const Flavour_process& proc, const Settings& s)
    : Phase_space_generator {s.e_cms},
      n_ptcl {proc.n_ptcl()},
      seed_offset {s.seed_offset},
      current_point(n_ptcl)
{
  Citations::register_citation("Chili", "Bothmann:2023siu");
  std::vector<Ptcl_num> particles(n_ptcl, NO_PTCL);
  for (int i {0}; i < n_ptcl; ++i)
    particles[i] = proc.particle_all_outgoing(i);

  // init Chili-internal model and cuts
  const auto model = create_chili_model();
  auto cuts = create_chili_cuts(model, s.cuts, particles);

  // init Chili settings
  auto settings = s.phase_space_settings.generator_settings;

  // construct channels, last number is the number of s-channels
  auto mappings = chili::ConstructChannels(s.e_cms, particles, model, cuts,
                                           settings.n_s_channels);

  // Setup integrator
  for (auto& mapping : mappings) {
    chili::Channel<chili::FourVector> channel;
    channel.mapping = std::move(mapping);
    // The constructor takes the number of integration dimensions and the
    // number of bins for vegas to start with.
    chili::AdaptiveMap map(channel.mapping->NDims(), settings.start_vegas_bins);
    // The constructor takes the adaptive map and settings (found in the
    // VegasParams struct).
    channel.integrator = chili::Vegas(map, chili::VegasParams {});
    integrand.AddChannel(std::move(channel));
  }

  integrator = chili::MultiChannel(integrand.NDims(), integrand.NChannels(), {});

  // set some of the remaining multi-channel parameters
  auto& params = integrator.Parameters();
  params.max_bins = settings.max_vegas_bins;
  params.beta = settings.beta;
  params.should_optimize = settings.should_optimize;

  nchannels = integrator.NChannels();
  ndims = integrator.Dimensions();

  should_optimise = true;
  train_data.resize(nchannels);
  ichannel.resize(s.batch_size);
  ps_weight.resize(s.batch_size);
  rans.resize(s.batch_size, std::vector<double>(ndims));
  densities.resize(s.batch_size, std::vector<double>(nchannels));
}

void Chili::report_run_statistics(const Flavour_process& proc)
{
  if (n_nonfinite_weight_occurrences > 0) {
    Run_statistics::register_occurences("Non-finite Chili weights (" +
                                            proc.spec() + ")",
                                        n_nonfinite_weight_occurrences);
  }
}

void Chili::fill_momenta_and_weights(Event_data& evt) const
{
  const int batch_size {evt.host.batch_size};
  for (int i {0}; i < batch_size; ++i) {

    // generate Chili phase-space point
    const size_t channel {integrator.GeneratePoint(integrand, current_point)};
    if (should_optimise)
      ichannel[i] = channel;

    // copy Chili momenta into our data structure
    for (int p {0}; p < n_ptcl; ++p) {
      evt.host.e(i, p) = current_point[p][0];
      evt.host.px(i, p) = current_point[p][1];
      evt.host.py(i, p) = current_point[p][2];
      evt.host.pz(i, p) = current_point[p][3];
    }

    // update momentum fractions
    evt.host.x1(i) = std::abs(current_point[0][0] / (e_cms / 2.0));
    evt.host.x2(i) = std::abs(current_point[1][0] / (e_cms / 2.0));

    // fill weight
    evt.w(i) = 1.0;
  }
}

void Chili::update_weights(Event_handle& evt) const
{
  std::vector<chili::FourVector> point(n_ptcl);

  for (int i {0}; i < evt.host.batch_size; ++i) {
    if (evt.host.w(i) == 0)
      continue;

    // copy momenta into Chili-internal data structure
    for (int p {0}; p < n_ptcl; ++p) {
      point[p][0] = evt.host.e(i, p);
      point[p][1] = evt.host.px(i, p);
      point[p][2] = evt.host.py(i, p);
      point[p][3] = evt.host.pz(i, p);
    }

    double wgt;
    if (should_optimise) {
      wgt = integrator.GenerateWeight(integrand, point, ichannel[i],
                                      densities[i], rans[i]);
    }
    else {
      wgt = integrator.GenerateWeight(integrand, point, -1);
    }
    if (!std::isfinite(wgt)) {
      WARN_ONCE("Chili has returned a non-finite weight = " +
                std::to_string(wgt) + ". Will set to 0.0 and continue. ");
      n_nonfinite_weight_occurrences++;
      wgt = 0.0;
    }
    ps_weight[i] = wgt;
    evt.host.w(i) = wgt;
  }
}

void Chili::optimise()
{
  // this should only be called when the integrator has not been frozen
  if (!should_optimise) {
    ERROR(
        "Phase space generator tried to optimise although it has been frozen");
    ERROR("Skipping the optimisation");
    return;
  }
  integrator.Train(integrand, train_data);

  // reset everything that needs resetting
  std::fill(train_data.begin(), train_data.end(), 0);
  need_init = true;
}

void Chili::add_training_data(Event_handle& evt)
{
  // this should only be called when the integrator has not been frozen
  if (!should_optimise) {
    ERROR(
        "Phase space generator tried to optimise although it has been frozen");
    ERROR("Skipping the optimisation");
    return;
  }

  // this should happen after the pdf etc have been added to the weights
  if (need_init) {
    integrand.InitializeTrain();
    need_init = false;
  }

  for (int i {0}; i < evt.host.batch_size; ++i) {
    if (evt.host.w(i) == 0)
      continue;
    integrator.AddTrainData(integrand, ichannel[i], evt.host.w(i) * evt.host.me2(i),
                            ps_weight[i], train_data, rans[i], densities[i]);
  }
}

void Chili::stop_optimisation()
{
  should_optimise = false;

  // clear data only required for optimisation
  train_data.clear();
  ichannel.clear();
  ps_weight.clear();
  rans.clear();
  densities.clear();
}

void Chili::reset_rng()
{
  chili::Random::Instance().Seed(4UL + seed_offset + Mpi::rank());
}

void Chili::write_to_cache(const std::string& procname)
{
  fs::path out_path {Paths::cache + "/Chili/"};
  fs::create_directories({out_path});
  integrator.SaveAs(integrand, out_path.string() + procname + ".chili");
}

void Chili::read_from_cache(const std::string& procname)
{
  fs::path out_path {Paths::cache + "/Chili/"};
  fs::create_directories({out_path});
  integrator.LoadFrom(integrand, out_path.string() + procname + ".chili");
}
