// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "print.h"
#include "settings.h"
#include "system.h"

#include <iomanip>
#include <optional>
#include <unistd.h>

using namespace Msg;

Verbosity Msg::to_verbosity(const std::string& s)
{
  auto c = std::tolower(s[0]);
  switch (c) {
  case 'e':
    return Verbosity::error;
  case 'w':
    return Verbosity::warn;
  case 'i':
    return Verbosity::info;
  case 'v':
    return Verbosity::verbose;
  case 'd':
    return Verbosity::debug;
  default:
    throw std::invalid_argument {"`" + s +
                                 "' can not be translated to a verbosity."};
  }
}
std::string Msg::to_string(Verbosity v)
{
  switch (v) {
  case Verbosity::error:
    return "error";
  case Verbosity::warn:
    return "warn";
  case Verbosity::info:
    return "info";
  case Verbosity::verbose:
    return "verbose";
  case Verbosity::debug:
    return "debug";
  }
  assert(false);
  return "unknown";
}

Colourfulness Msg::to_colourfulness(const std::string& s)
{
  auto c = std::tolower(s[1]);
  switch (c) {
  case 'e':
    return Colourfulness::never;
  case 'u':
    return Colourfulness::automatic;
  case 'l':
    return Colourfulness::always;
  default:
    throw std::invalid_argument {"`" + s +
                                 "' can not be translated to a colourfulness."};
  }
}
std::string Msg::to_string(Colourfulness c)
{
  switch (c) {
  case Colourfulness::never:
    return "never";
  case Colourfulness::automatic:
    return "automatic";
  case Colourfulness::always:
    return "always";
  }
  assert(false);
  return "unknown";
}

void Msg::configure(const Settings& s)
{
  verbosity = s.msg_verbosity;

  // The heuristics below for Colourfulness::automatic fails when the
  // executable is wrapped with mpiexec (or mpirun, equivalently), because
  // isatty() gives back false in this case, cf.
  // https://stackoverflow.com/questions/73156434/tell-if-stdout-is-a-tty-using-mpi-in-c
  // There is no portable way to figure out if stderr/out is connected to a tty
  // *via* mpiexec.  Hence, when the user wants color when running with mpirun,
  // she/he must explicitly enforce this by using --color on the command line.
  if (s.msg_colourfulness == Colourfulness::never ||
      (s.msg_colourfulness == Colourfulness::automatic &&
       (!isatty(STDERR_FILENO) ||
        (get_environment_variable("NO_COLOR", "") == "1") ||
        (get_environment_variable("TERM", "") == "dumb") ||
        (get_environment_variable("BG2_NO_COLOR", "") == "1")))) {
    bold = "";
    red = "";
    green = "";
    yellow = "";
    blue = "";
    magenta = "";
    cyan = "";
    reset = "";
    colourfulness = Colourfulness::never;
  }
  else {
    colourfulness = Colourfulness::always;
  }
}

std::string Msg::version_string()
{
  std::string s {PROJECT_VERSION};
  if (PACKAGE_GIT_VERSION != std::string("Unknown"))
    s += " (" PACKAGE_GIT_VERSION ")";
  return s;
}

void Msg::print_banner()
{
  println();
  std::string label {PROJECT_NAME " v" + version_string()};
  print_left_facing_bee_with_label(label);
}

void Msg::print_left_facing_bee_with_label(const std::string& label)
{
  println(R"(   \\//   _ _)");
  println("  (°", yellow, ")))", reset, ">- _-_-   ", label);
  println("    ```");
}

void Msg::print_right_facing_bee()
{
  println(R"(   \\//)");
  println(" -<", yellow, "(((", reset, "°)");
  println("   ´´´");
}

// adapted from https://stackoverflow.com/a/58127222
std::ostream& operator<<(std::ostream& os, std::chrono::milliseconds ms)
{
  using namespace std::chrono;
  using days = duration<int, std::ratio<86400>>;
  auto d = duration_cast<days>(ms);
  ms -= d;
  auto h = duration_cast<hours>(ms);
  ms -= h;
  auto m = duration_cast<minutes>(ms);
  ms -= m;
  auto s = duration_cast<seconds>(ms);
  ms -= s;

  std::optional<int> fs_count;
  switch (os.precision()) {
  case 3:
    fs_count = duration_cast<milliseconds>(ms).count();
    break;
  }

  char fill = os.fill('0');
  if (d.count())
    os << d.count() << "d ";
  if (d.count() || h.count())
    os << std::setw(2) << h.count() << "h ";
  if (d.count() || h.count() || m.count())
    os << std::setw(d.count() || h.count() ? 2 : 1) << m.count() << "min ";
  os << std::setw(d.count() || h.count() || m.count() ? 2 : 1) << s.count();
  if (fs_count.has_value())
    os << "." << std::setw(os.precision()) << fs_count.value();
  os << "s";

  os.fill(fill);
  return os;
}
