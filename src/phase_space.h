// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_PHASE_SPACE_H
#define PEPPER_PHASE_SPACE_H

#include "vec4.h"
#include <memory>
#include <vector>

class Event_handle;
class Flavour_process;
struct Settings;

class Phase_space_generator {

public:
  Phase_space_generator(double _e_cms)
      : e_cms {_e_cms},
        _flux_factor {1.0 / (2 * e_cms * e_cms)}
  {
  }
  virtual ~Phase_space_generator() = default;

  double flux_factor() const { return _flux_factor; }

  // functions that must be overridden by derived classes
  virtual void fill_momenta_and_weights(Event_handle&) const = 0;
  virtual void update_weights(Event_handle&) const = 0;

  // Optional optimisation function that can be overridden by derived classes.
  // The default implementation does nothing.
  // TODO: We use both optimise and train to indicate the same thing, do we not?
  // Let's unify that nomenclature.
  virtual void update_weights_and_train(Event_handle&);
  virtual void add_training_data(Event_handle& evt);
  virtual void optimise();
  virtual void stop_optimisation();

  // Optional method to report run statistics at the end of the run.
  // The default implementation does nothing.
  virtual void report_run_statistics(const Flavour_process&) {}

  // Optional optimisation function that can be overridden by derived classes.
  // The default implementation prints an error and aborts execution.
  virtual void set_fixed_momenta(const std::vector<Vec4>&);

  // Optionally reserve n random numbers to be generated for each event.
  // The default implementation does nothing and returns 0.
  virtual int n_random_numbers() const { return 0; }

  // Optionally declare that the generator does not support running on device.
  // The default implementation returns false.
  virtual bool is_host_only() const { return false; }

  // Optionally declare that the generator allows optimisation.
  // The default implementation returns true.
  virtual bool allows_optimisation() const { return true; }

  // Optionally declare that the generator supports multiple flavour procs.
  // The default implementation returns true.
  virtual bool allows_multiple_flavour_processes() const { return true; }

  // Optionally reset random number generator if the phasespace uses its own.
  // The default implementation does nothing.
  virtual void reset_rng() {}

  // Optionally write to/read from cache.
  // The default implementations do nothing.
  virtual void write_to_cache(const std::string& procname);
  virtual void read_from_cache(const std::string& procname);

  // Optionally reset performance timers.
  virtual void reset_timing() const {}

protected:
  double e_cms;
  bool should_optimise {false};

private:
  double _flux_factor;
};

std::unique_ptr<Phase_space_generator>
create_phase_space_generator(const Settings&, const Flavour_process&);

#endif
