// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_RECURSION_H
#define PEPPER_RECURSION_H

#include "math.h"
#include "Kokkos_Core.hpp"
#include "print.h"
#include "sm.h"
#include <memory>

class Event_handle;
class Permutations;
class Helicity_configurations;
struct SM_parameters;

namespace detail {
  struct BG_recursion_data {
    int n;
    C stripped_g_ew_right;
    C stripped_g_ew_left_1;
    C stripped_g_ew_left_2;
  };
}

class BG_recursion {
public:
  BG_recursion(int n_ptcl, const SM_parameters&);
  ~BG_recursion();
  void prepare(Event_handle&, const Helicity_configurations&,
               const Permutations&,const int& perm_idx) const;
  // Reset all currents to zero and mark them for re-calculation.
  void reset_internal_currents(Event_handle&);
  void perform(Event_handle&) const;
  void reset_matrix_elements(Event_handle&, const Helicity_configurations&,
                             int qcd_perm_idx) const;
  void update_matrix_elements(Event_handle&, const Helicity_configurations&,
                              const Permutations&, int qcd_perm_idx) const;

  void reset_timing() const
  {
    prepare_momenta_duration = 0.0;
    prepare_currents_duration = 0.0;
    reset_internal_currents_duration = 0.0;
    reset_internal_particle_information_duration = 0.0;
    perform_duration = 0.0;
    reset_me_duration = 0.0;
    update_me_duration = 0.0;
  }

private:

  Kokkos::View<detail::BG_recursion_data> d_data;
  Kokkos::View<detail::BG_recursion_data,Kokkos::HostSpace> h_data;
  

  // The neutral gauge boson used to combine lepton-antilepton pairs.
  Ptcl_num active_neutral_gauge_boson;

  // These are used to restrict the currents that have to be re-calculated in
  // the recursion. Everything else is read-only.
  int min_row {0};
  int min_column {0};

  mutable double prepare_momenta_duration {0};
  mutable double prepare_currents_duration {0};
  mutable double reset_internal_currents_duration {0};
  mutable double reset_internal_particle_information_duration {0};
  mutable double perform_duration {0};
  mutable double reset_me_duration {0};
  mutable double update_me_duration {0};
};

KOKKOS_INLINE_FUNCTION int index(int k, int l, int n)
{
  const int m = l - k + 1;
  return n * (m - 1) + k - (m * (m - 1)) / 2;
  // Alternate form (with double--int-conversions and double division, and
  // therefore not very efficient):
  // return (m-1)*(h.n-m/2.) + k;
}

KOKKOS_INLINE_FUNCTION int row(int k, int n)
{
  return (n - 1) - k - 1;
}

KOKKOS_INLINE_FUNCTION int column(int m, int k)
{
  return k + m;
}

KOKKOS_INLINE_FUNCTION bool is_readonly(int m, int k, int n, int min_row,
                                      int min_column)
{
  return ((min_row != 0 && row(k, n) < min_row) ||
          (min_column != 0 && column(m, k) < min_column));
}

#endif
