// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "process_integrator.h"
#include "mpi.h"
#include "paths.h"
#include "print.h"
#include "process.h"

#include <fstream>
#include <limits>

Process_integrator::Process_integrator(Process& _proc,
                                       bool _diagnostic_output_enabled)
    : proc {_proc},
      diagnostic_output_enabled {_diagnostic_output_enabled}
{
  // create one weight histogram per flavour process group
  const int n_procs {proc.n_procs()};
  for (int i {0}; i < n_procs; i++) {
    weight_histograms.push_back(make_weight_histogram());
  }
}

void Process_integrator::start() { proc.start_integration(); }

void Process_integrator::stop()
{
  // synchronise histograms from all workers
  const int n_procs {proc.n_procs()};
  for (int i {0}; i < n_procs; ++i) {
    for (auto x : indexed(weight_histograms[i], boost::histogram::coverage::all)) {
      double n_entries {*x};
      Mpi::sum_d(n_entries);
      if (Mpi::is_main_rank())
        weight_histograms[i].at(x.index()) = n_entries;
    }
  }

  // find maximum weight for each flavour process group, applying the condition
  // that only one permille of the cross section is allowed to come from events
  // with a weight larger than that maximum weight (epsilon prescription);
  // working with the histogram, this means that we reverse-iterate over bins,
  // calculating the cumulative area, until it is larger than 0.001 times the
  // sum of weights in the histogram; at that point, the respective upper bin
  // edge is our reduced weight maximum
  for (int i {0}; i < n_procs; ++i) {

    const double mean_weight_i {proc.partial_mean_abs_weights[i]};

    const double allowed_overweight_weight_sum {
      proc.max_eps * proc.partial_abs_results[i].sum};

    const auto& h_i = weight_histograms[i];

    // Find maximum weight from the histogram using the algorithm described
    // above, starting from the overflow bin ...
    double w_max_from_histo {std::numeric_limits<double>::max()};
    double sum {h_i.axis(0).bin(h_i.size() - 2).lower() * h_i[h_i.size() - 2] *
                mean_weight_i};
    if (sum > 0.0) {
      WARN("A weight histogram overflow is not empty. We will assume the "
           "weight of the events within it to be 1e6 times the mean weight.");
    }
    // ... and then working our way in reverse order through the other bins.
    for (int b {static_cast<int>(h_i.size() - 3)}; b >= 0; b--) {
      sum += h_i.axis(0).bin(b).center() * h_i[b] * mean_weight_i;
      if (sum > allowed_overweight_weight_sum) {
        w_max_from_histo = h_i.axis(0).bin(b).center() * mean_weight_i;
        break;
      }
    }
    // If we select the last filled bin to determine w_max_from_histo and then
    // take the right bin edge, we have w_max_from_histo > w_max. In that case,
    // we just use w_max.
    Mpi::max_d(proc.partial_max_weights[i]);
    proc.partial_max_weights[i] =
        std::min(proc.partial_max_weights[i], w_max_from_histo);
    Mpi::cast_d(proc.partial_max_weights[i]);
  }

  if (Mpi::is_main_rank()) {
    if (diagnostic_output_enabled) {
      for (int i {0}; i < n_procs; ++i) {
        // output weight histogram in CSV format
        std::ofstream f {Paths::diagnostics + "/weight_histogram_" +
                         proc[i].uid() + ".csv"};
        f << "#w_mean=" << proc.partial_mean_abs_weights[i] << '\n';
        f << "#n_events=" << proc.partial_abs_results[i].n_nonzero << '\n';
        f << "#w_max=" << proc.partial_max_weights[i] << '\n';
        f << "#generator=Pepper\n";
        f << "Weight (left bin edge),Number of events\n";
        for (auto x :
             indexed(weight_histograms[i], boost::histogram::coverage::all)) {
          f << x.bin(0) << ',' << *x << '\n';
        }
      }
    }
  }

  proc.stop_integration();
}

void Process_integrator::add_weight(double weight)
{
  if (weight != 0.0) {
    const int idx {proc.current()};
    weight_histograms[idx](std::abs(weight / proc.sampling_factor() /
                                    proc.partial_mean_abs_weights[idx]));
  }

  proc.update_iter_weights(weight);
}

bool Process_integrator::has_saturated() const
{
  for (const auto& res : proc.partial_results) {
    if (res.sum == 0.0)
      return false;
  }
  return true;
}
