// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_MASSLESS_RAMBO_KERNEL_H
#define PEPPER_MASSLESS_RAMBO_KERNEL_H

#include "event_handle.h"
#include "kernel_macros.h"
#include "math.h"
#include "vec4.h"

template<typename event_data>
KOKKOS_INLINE_FUNCTION
void massless_rambo_fill_momenta_and_weights_kernel(const event_data& evt, int i,
                                                    double e_cms)
{
  // Incoming particles
  evt.e(i, 0) = -evt.x1(i) * e_cms / 2.0;
  evt.px(i, 0) = 0.0;
  evt.py(i, 0) = 0.0;
  evt.pz(i, 0) = -evt.x1(i) * e_cms / 2.0;
  evt.e(i, 1) = -evt.x2(i) * e_cms / 2.0;
  evt.px(i, 1) = 0.0;
  evt.py(i, 1) = 0.0;
  evt.pz(i, 1) = evt.x2(i) * e_cms / 2.0;

  // In the cms frame, p1+p2 = (w,0,0,0).
  const double w = sqrt(evt.x1(i) * evt.x2(i)) * e_cms;

  // Apply trafo (3.1) in Computer Physics Communications 40 (1986) 359—373
  // and calculate sum of momenta Q to prepare the transformation in (2.5).
  Vec4 Q {0.0, 0.0, 0.0, 0.0};
  for (int p {2}; p < evt.n_ptcl; p++) {
    const double c = 2.0 * evt.r_rambo(i, p, 0) - 1.0;
    const double phi = 2.0 * M_PI * evt.r_rambo(i, p, 1);
    evt.e(i, p) =
        -std::log(evt.r_rambo(i, p, 2) * evt.r_rambo(i, p, 3));
    evt.px(i, p) = evt.e(i, p) * std::sqrt(1.0 - c * c) * std::cos(phi);
    evt.py(i, p) = evt.e(i, p) * std::sqrt(1.0 - c * c) * std::sin(phi);
    evt.pz(i, p) = evt.e(i, p) * c;
    Q += Vec4 (evt.e(i, p), evt.px(i, p), evt.py(i, p), evt.pz(i, p));
  }

  // Apply trafo (2.5) in Computer Physics Communications 40 (1986) 359—373
  const double M = Q.abs();
  const double gamma = Q[0] / M;
  const double a = 1.0 / (1.0 + gamma);
  const double x = w / M;
  const Vec3 b = Q.vec3() / (-M);

  // Apply trafo (2.4) in Computer Physics Communications 40 (1986) 359—373
  for (int p {2}; p < evt.n_ptcl; p++) {
    const double bq =
        b[0] * evt.px(i, p) + b[1] * evt.py(i, p) + b[2] * evt.pz(i, p);
    const double q0 = evt.e(i, p);
    evt.e(i, p) = x * (gamma * q0 + bq);
    evt.px(i, p) = x * (evt.px(i, p) + b[0] * q0 + a * bq * b[0]);
    evt.py(i, p) = x * (evt.py(i, p) + b[1] * q0 + a * bq * b[1]);
    evt.pz(i, p) = x * (evt.pz(i, p) + b[2] * q0 + a * bq * b[2]);
  }
  // Boost the momenta in the respective rest frame
  const double beta = -(evt.x1(i) - evt.x2(i)) / (evt.x1(i) + evt.x2(i));
  const double gamma_factor = sqrt(1. / (1. - beta * beta));
  for (int p {2}; p < evt.n_ptcl; p++) {
    const double e = evt.e(i, p);
    const double pz = evt.pz(i, p);
    evt.e(i, p) = gamma_factor * (e - beta * pz);
    evt.pz(i, p) = gamma_factor * (pz - beta * e);
  }

  // fill weight
  evt.w(i) = 1.0;
}
DEFINE_CUDA_KERNEL_NARGS_1(massless_rambo_fill_momenta_and_weights_kernel, double, e_cms)

template<typename event_data>
KOKKOS_INLINE_FUNCTION
void massless_rambo_update_weights_kernel(const event_data& evt, int i,
                                                 double e_cms)
{
  if (evt.w(i) == 0.0)
    return;
  // Corresponds to (2.14) in Computer Physics Communications 40 (1986)
  // 359—373
  const int n {evt.n_ptcl};
  const double w {sqrt(evt.x1(i) * evt.x2(i)) * e_cms};
  const double dlips {pow(2 * M_PI, 4) * 1. / pow(2 * M_PI, 3 * (n - 2))};
  const double gamma_product {
      static_cast<double>(factorial(n - 3) * factorial(n - 4))};
  const double Vn =
      pow(M_PI / 2., n - 3) * pow(w, 2 * (n - 2) - 4) / gamma_product * dlips;
  evt.w(i) *= Vn;
}
DEFINE_CUDA_KERNEL_NARGS_1(massless_rambo_update_weights_kernel, double, e_cms)

#endif
