// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_CHILI_INTERFACE_H
#define PEPPER_CHILI_INTERFACE_H

#include "pepper/config.h"

#include "event_handle.h"
#include "phase_space.h"
#include "flavour_process.h"

// Chili includes
#include "compiler_warning_macros.h"
PEPPER_SUPPRESS_COMMON_WARNINGS_PUSH
#include "Chili/Channel/Channel.hh"
#include "Chili/Channel/ChannelNode.hh"
#include "Chili/Channel/Integrand.hh"
#include "Chili/Channel/MultiChannel.hh"
#include "Chili/Model/Model.hh"
#include "Chili/Tools/Cuts.hh"
PEPPER_SUPPRESS_COMMON_WARNINGS_POP

namespace detail {
struct Cuts_data;
}

class Chili : public Phase_space_generator {
public:
  Chili(const Flavour_process&, const Settings&);

  void fill_momenta_and_weights(Event_handle&) const override;
  void update_weights(Event_handle&) const override;

  // methods to train the integrator, i.e. optimise vegas grids and channel
  // weights
  void add_training_data(Event_handle&) override;
  void optimise() override;
  void stop_optimisation() override;

  void report_run_statistics(const Flavour_process&) override;

  bool is_host_only() const override { return true; }
  void reset_rng() override;

  void write_to_cache(const std::string& procname) override;
  void read_from_cache(const std::string& procname) override;

private:
  int n_ptcl;

  // methods to handle chili Multichannel
  size_t n_channels() const { return integrator.NChannels(); }
  size_t n_dimensions() const { return integrator.Dimensions(); }

  chili::MultiChannel integrator;
  // TODO: Can we use our Vec4 class instead?
  chili::Integrand<chili::FourVector> integrand;

  // Chili-internal variables necessary for the training; some of these are
  // probably not necessary as member variables, but for now this is convenient
  int nchannels;
  int ndims;
  bool need_init {true};

  int seed_offset;

  // 'working' vector
  mutable std::vector<chili::FourVector> current_point;
  mutable std::vector<double> train_data;

  // need to remember this per point
  mutable std::vector<size_t> ichannel;
  mutable std::vector<double> ps_weight;
  mutable std::vector<std::vector<double>> rans;
  mutable std::vector<std::vector<double>> densities;

  // diagnostics
  mutable int n_nonfinite_weight_occurrences {0};
};

#endif
