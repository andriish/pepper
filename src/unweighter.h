// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef UNWEIGHTER_H
#define UNWEIGHTER_H

class Event_handle;

struct Unweighter {

  Unweighter() {}
  void unweight(Event_handle&) const;
  bool enabled() const { return w_max > 0.0; }

  // For a negative or zero w_max, unweight() does nothing.
  double w_max {-1.0};
};

#endif
