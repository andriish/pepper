// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_GENERATOR_H
#define PEPPER_GENERATOR_H

#include "pepper/config.h"

#include "cuts.h"
#include "event_handle.h"
#include "event_writer.h"
#include "helicity_configurations.h"
#include "math.h"
#include "pdf_and_alpha_s.h"
#include "process.h"
#include "recursion.h"
#include "rng.h"
#include "scale_setter.h"
#include <memory>

namespace Pepper {
struct Vec4;
}

struct Settings;
struct SM_parameters;

/** \brief Monte-Carlo event generator class.
 *
 *  This class has the high-level control over the generation of Monte-Carlo events.
 *  This includes all warm-up phases necessary for optimising the event generation
 *  (and unweighting), and the write-out of the events.
 */
class Generator {
public:
  Generator(Settings&, const SM_parameters&);
  ~Generator();
  void optimise();
  void setup_and_optimise_event_unweighting();
  MC_result generate_batches(int);
  const Event_handle& event_data() const { return evt; }
  const BG_recursion& bg_recursion() const { return bg; }
  Process& process_group() { return proc; }
  Helicity_configurations& helicity_configurations() { return hel_conf; }
  double me2(const std::vector<Pepper::Vec4>&);

private:
  // warm-up phase
  void find_active_helicity_configurations();
  void optimise_event_generation();
  void print_integration_info(double elapsed_s) const;

  // warm-up helpers
  double rescaled_n_nonzero_min(double n_nonzero_min) const;

  // event-generation helpers
  void evaluate_points(Event_handle&);
  void perform_colour_sum(Event_handle&, Helicity_configurations&);
  double prefactor() const;
  void fill_rng(Event_handle&);
public:
  void fill_momenta_and_apply_cuts(Event_handle&);
private:
  void reset_timing() const;

  Process proc;
  Rng rng;
  Cuts cuts;
  Helicity_configurations hel_conf;
  bool should_precalculate_all_external_states;
  Event_handle evt;
  BG_recursion bg;

  bool me2_only;
  std::unique_ptr<Event_writer> event_writer;
  std::unique_ptr<Scale_setter> scale_setter;

  Pdf_and_alpha_s pdf_and_alpha_s;
  Phase_space_settings ps_settings;
  mutable double write_events_duration {0.0};
  mutable double evaluate_points_duration {0.0};
  mutable double recursion_duration {0.0};
  mutable double phase_space_duration {0.0};
  mutable double fill_momenta_and_weights_duration {0.0};
  mutable double phase_space_weights_duration {0.0};
  mutable double me2_update_duration {0.0};

  bool diagnostic_output_enabled;
};

#endif
