// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "histogram.h"
#include "math.h"
#include <vector>

class Process;

class Process_integrator {
public:
  Process_integrator(Process&, bool diagnostic_output_enabled);
  void start();
  void stop();
  void add_weight(double);

  // Users can call this to decide if the integration should be stopped.
  // Non-saturation means that not all of the processes have been sampled.
  bool has_saturated() const;

private:
  std::vector<Weight_histogram> weight_histograms;
  Process& proc;

  bool diagnostic_output_enabled;
};
