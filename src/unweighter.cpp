// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "unweighter.h"
#include "event_handle.h"
#include "unweight_kernel.h"

void Unweighter::unweight(Event_handle& evt) const
{
  if (w_max <= 0.0)
    return;
  const auto& _w_max = w_max;
  RUN_KERNEL(unweight_kernel, evt, _w_max);
}
