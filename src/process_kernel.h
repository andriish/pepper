// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_HELICITY_CONFIGURATIONS_KERNEL_H
#define PEPPER_HELICITY_CONFIGURATIONS_KERNEL_H


#include "event_data.h"
#include "kernel_macros.h"

template <typename event_type>
KOKKOS_INLINE_FUNCTION void
update_helicity_weights_kernel(const event_type& evt, int i)
{
  if (evt.w(i) == 0.0)
    return;
  const int block {i / HELICITY_BLOCK_SIZE};
  const int hel {evt.active_hel_conf_idx(block)};
  const double sel_wgt {evt.hel_selection_weights(hel)};
  evt.w(i) = evt.w(i) / (sel_wgt * evt.n_active_helicities());
}
DEFINE_CUDA_KERNEL_NARGS_0(update_helicity_weights_kernel);

#endif
