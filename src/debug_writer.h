// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_DEBUG_WRITER_H
#define PEPPER_DEBUG_WRITER_H

#include "event_writer.h"
#include "helicity_configurations.h"

#include <fstream>

struct Settings;

class Debug_writer : public Event_writer {

public:
  Debug_writer(const std::string& filepath, const Settings&);

  void initialise(const Process&, const Pdf_and_alpha_s&) override;

  void write_n(const Event_handle&, int n, const MC_result&, long n_nonzero,
               long n_trials, const Process&) override;

  bool prints_zero_events() const override
  {
    return zero_weight_event_output_enabled;
  }

  bool prints_leading_colour_information() const override
  {
    return leading_colour_flow_output_enabled;
  }


private:
  long n_nonzero_written {0};
  std::ofstream out;
  Helicity_treatment hel_treatment;
  bool zero_weight_event_output_enabled {false};
  bool leading_colour_flow_output_enabled;
  std::vector<int> colours;
  std::vector<int> anticolours;
};

#endif
