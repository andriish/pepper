// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_FILL_LEADING_COLOUR_INFORMATION_H
#define PEPPER_FILL_LEADING_COLOUR_INFORMATION_H

#include "event_handle.h"
#include "process.h"

#include <vector>

inline void fill_leading_colour_information(const Event_handle& evt, int i,
                                            const Process& proc,
                                            std::vector<int>& colours,
                                            std::vector<int>& anticolours)
{
  if (evt.host.w(i) == 0)
    return;

  // get the actual permutation for the given flavour process channel
  std::vector<int> perm =
      proc.permutations().mapped_qcd_permutation_for_flavour_process(
          proc.current_flavour_process(), evt.host.flav_channels(i),
          evt.host.active_lc_conf_idx(i));

  // cycle through particles and assign leading colour information
  // "To avoid confusion it is recommended that integer tags larger than
  // MAXNUP (i.e. 500) are used. The actual value of the tag has no meaning
  // beyond distinguishing the lines in a given process." [hep-ph/0109068v1]
  int last_colour {-1};
  for (int p {0}, current_colour {500}; p < evt.host.n_ptcl; ++p) {
    const int permuted_p {perm[p]};
    Ptcl_num ptcl {proc.current_flavour_process().unmapped_ptcl(
        evt.host.flav_channels(i), permuted_p)};
    if (permuted_p < 2)
      ptcl = antiparticle(ptcl);
    if (is_quark(ptcl)) {
      if (is_antiparticle(ptcl)) {
        colours[permuted_p] = 0;
        ++current_colour;
        anticolours[permuted_p] = current_colour;
        last_colour = current_colour;
      }
      else {
        anticolours[permuted_p] = 0;
        if (last_colour > 0) {
          colours[permuted_p] = last_colour;
        }
      }
    } else if (ptcl == GLUON) {
      ++current_colour;
      anticolours[permuted_p] = current_colour;
      colours[permuted_p] = last_colour;
      last_colour = current_colour;
    }
    else {
      colours[permuted_p] = 0;
      anticolours[permuted_p] = 0;
    }
  }
  colours[perm[0]] = last_colour;
}

#endif
