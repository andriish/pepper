// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_RUN_STATISTICS_H
#define PEPPER_RUN_STATISTICS_H

#include <iostream>
#include <map>
#include <string>

namespace Run_statistics {
namespace detail {
extern std::map<std::string, int> occurrences;
};

inline void register_occurences(const std::string& label, int n_occurrences)
{
  detail::occurrences[label] = n_occurrences;
}

void print_occurrences(std::ostream&);

} // namespace Run_statistics

#endif
