// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_CUTS_H
#define PEPPER_CUTS_H

#include "pepper/config.h"

#include "Kokkos_Core.hpp"
#include "math.h"
#include <memory>

class Event_handle;

namespace detail {
struct Cuts_data {
  double pT_min {30.0};
  double nu_max {5.0};
  double dR_min {0.4};
  double e_cms {14000};
  // invariant mass of lepton pairs
  double ll_m2_min {square(66.0)};
  double ll_m2_max {square(116.0)};
};
} // namespace detail

struct Cuts {
  Cuts(detail::Cuts_data);
  ~Cuts();

  void fill(Event_handle&) const;

  void reset_timing() const { kernel_duration = 0.0; }

private:
  // std::unique_ptr<detail::Cuts_data> data;
  Kokkos::View<detail::Cuts_data> d_data;
  Kokkos::View<detail::Cuts_data,Kokkos::HostSpace> h_data;

  mutable double kernel_duration {0};
};

#endif
