// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2024 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "pepper/pepper.h"

#include "citations.h"
#include "generator.h"
#include "kokkos.h"
#include "paths.h"
#include "pdf_and_alpha_s.h"
#include "print.h"
#include "run_statistics.h"
#include "settings.h"
#include "timing.h"

std::unique_ptr<Generator> generator;
Timer timer;

std::string Pepper::version()
{
  return Msg::version_string();
}

struct Pepper::Initialization_settings::Impl {
  Impl(int& _argc, char** _argv)
      : argc {&_argc},
        argv {_argv}
  {
  }
  int* argc;
  char** argv {nullptr};
  bool mpi_initialization_enabled {true};
  bool kokkos_initialization_enabled {true};
  Settings run_settings;
};

Pepper::Initialization_settings::Initialization_settings(int& argc, char** argv):
  pimpl {std::make_unique<Impl>(argc, argv)}
{
}

int& Pepper::Initialization_settings::argc()
{
  return *pimpl->argc;
}

char** Pepper::Initialization_settings::argv()
{
  return pimpl->argv;
}

void Pepper::Initialization_settings::disable_mpi_initialization()
{
  pimpl->mpi_initialization_enabled = false;
}

void Pepper::Initialization_settings::disable_kokkos_initialization()
{
  pimpl->kokkos_initialization_enabled = false;
}

void Pepper::Initialization_settings::set_process(std::string proc)
{
  pimpl->run_settings.process_specs = {std::move(proc)};
}

// This dtor must be defined here (not implicitly in pepper.h), see
// https://stackoverflow.com/questions/34072862/why-is-error-invalid-application-of-sizeof-to-an-incomplete-type-using-uniqu
Pepper::Initialization_settings::~Initialization_settings() = default;

void Pepper::initialize(int& argc, char** argv)
{
  Initialization_settings init_settings {argc, argv};
  initialize(init_settings);
}

void initialize_generator(Settings& s)
{
  validate_and_sync_particle_properties_with_pdf_info(s);

  SM_parameters sm;
  generator = std::make_unique<Generator>(s, sm);
}

void Pepper::initialize(Initialization_settings& init_settings)
{
  // TODO: Reduce duplication between this and main & test/main
  Mpi::initialise_and_register_finalise();
  Pepper_kokkos::initialise_and_register_finalise(init_settings.argc(),
                                                  init_settings.argv());
  Timing::initialise();
  timer.reset();

  Settings& s {init_settings.pimpl->run_settings};

  s.msg_verbosity = Msg::Verbosity::warn;
  s.helicity_settings.treatment = Helicity_treatment::summed;
  s.batch_size = 1;
  s.n_batches = 1;
  s.me2_only = true;
  s.event_output_disabled = true;
  s.phase_space_settings.use_cached_results_if_possible = false;
  s.cuts.ll_m2_min = 0.0;
  s.cuts.ll_m2_max = std::numeric_limits<double>::max();
  s.fixed_alpha_s = 0.118;

  // read command line arguments (with "pepper" as a prefix, e.g.
  // "--pepper-help") and an input file if present
  s.read(init_settings.argc(), init_settings.argv(), "pepper");
  Msg::configure(s);

  if (Mpi::is_main_rank())
    Msg::print_banner();

  initialize_generator(s);

  if (Mpi::is_main_rank()) {
    Citations::register_citation("Pepper v" PROJECT_VERSION,
                                 PEPPER_INSPIRE_TEXKEY);
    std::ofstream citations_file {"pepper_citations.tex"};
    Citations::print_citations(citations_file);
    if (Msg::verbosity >= Msg::Verbosity::info)
      Run_statistics::print_occurrences(std::cerr);
    if (s.diagnostic_output_enabled) {
      s.write_resolved_settings();
      std::ofstream file {Paths::diagnostics + "/timers.csv"};
      Timing::print_durations(file);
    }
  }
}

void Pepper::finalize()
{
  generator.reset();
  Timing::finalise(timer.elapsed());
}

double Pepper::me2(const std::vector<Vec4>& momenta)
{
  return generator->me2(momenta);
}
