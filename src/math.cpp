// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "math.h"
#include "mpi.h"
#include "print.h"
#include <array>
#include <sstream>

double MC_result::mean() const
{
  if (n_trials == 0)
    return 0.0;
  return sum / n_trials;
}

double MC_result::variance() const
{
  const double integ_var {integrand_variance()};
  if (n_trials < 2)
    return integ_var;
  return integ_var / (n_trials - 1);
}

double MC_result::stddev() const
{
  const double _mean {mean()};
  if (n_trials < 2)
    return std::abs(_mean);
  return std::sqrt((sum2 / n_trials - _mean * _mean) / (n_trials - 1));
}

double MC_result::integrand_variance() const
{
  const double _mean {mean()};
  if (n_trials < 2)
    return _mean * _mean;
  return sum2 / n_trials - _mean * _mean;
}

double MC_result::integrand_stddev() const
{
  const double _mean {mean()};
  if (n_trials < 2)
    return std::abs(_mean);
  return std::sqrt(sum2 / n_trials - _mean * _mean);
}

double MC_result::efficiency() const { return (double)n_nonzero / n_trials; }

double MC_result::cut_efficiency() const
{
  return (double)n_nonzero_after_cuts / n_trials;
}

double MC_result::unweighting_efficiency() const
{
  return (double)n_nonzero / n_nonzero_after_cuts;
}

double MC_result::kish_sample_size() const { return sum * sum / sum2; }

void MC_result::all_mpi_sum()
{
#if MPI_FOUND
  std::array<double, 2> sums {sum, sum2};
  Mpi::all_sum_d(sums);
  sum = sums[0];
  sum2 = sums[1];

  std::array<long, 3> n_events {n_trials, n_nonzero_after_cuts, n_nonzero};
  Mpi::all_sum_i(n_events);
  n_trials = n_events[0];
  n_nonzero_after_cuts = n_events[1];
  n_nonzero = n_events[2];
#endif
}

MC_result& MC_result::operator+=(const MC_result& rhs)
{
  sum += rhs.sum;
  sum2 += rhs.sum2;
  n_trials += rhs.n_trials;
  n_nonzero_after_cuts += rhs.n_nonzero_after_cuts;
  n_nonzero += rhs.n_nonzero;
  return *this;
}

std::ostream& operator<<(std::ostream& os, const MC_result& res)
{
  double rel_unc {1.0};
  if (res.mean() != 0.0)
    rel_unc = res.stddev() / res.mean();

  double rel_nonzero {1.0};
  if (res.n_trials != 0.0)
    rel_nonzero = double(res.n_nonzero) / res.n_trials;

  os << Msg::green << res.mean() << " +- " << res.stddev() << " pb"
     << Msg::reset << " (" << rel_unc * 100.0 << " %"
     << ")\n";
  os << res.n_nonzero << " of " << res.n_trials << " events are non-zero ("
     << 100.0 * rel_nonzero << " %)";
  return os;
}

std::ostream& operator<<(std::ostream& os, const MC_result::Raw_printer& raw)
{
  return os << raw.data.sum << ' ' << raw.data.sum2 << ' ' << raw.data.n_trials
            << ' ' << raw.data.n_nonzero_after_cuts << ' '
            << raw.data.n_nonzero;
}

std::istream& operator>>(std::istream& os, MC_result::Raw_printer raw)
{
  return os >> raw.data.sum >> raw.data.sum2 >> raw.data.n_trials >>
         raw.data.n_nonzero_after_cuts >> raw.data.n_nonzero;
}

std::vector<int> revert_perm(const std::vector<int>& perm)
{
  const size_t n {perm.size()};
  std::vector<int> rperm(n);
  for (size_t i {0}; i < n; ++i) {
    rperm[perm[i]] = i;
  }
  return rperm;
}

std::vector<int> concat_perms(const std::vector<int>& a,
                              const std::vector<int>& b,
                              const std::vector<int>& c)
{
  size_t size {a.size()};
  std::vector<int> combined(c.size(), -1);
  for (size_t i {0}, j {0}; i < size; ++i) {
    if (a[i] == -1 || b[a[i]] == -1) {
      j++;
      continue;
    }
    combined[c[b[a[i]]]] = j;
    j++;
  }
  return combined;
}
