// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "system.h"

std::string get_environment_variable(const std::string& key,
                                     const std::string& def)
{
  char* val = std::getenv(key.c_str());
  return (val == NULL) ? def : std::string(val);
}
