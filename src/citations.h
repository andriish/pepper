// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_CITATIONS_H
#define PEPPER_CITATIONS_H

#include <iostream>
#include <map>
#include <string>

#include "mpi.h"

// Facilities to print a list of citations that can be dumped
// into https://inspirehep.net/bibliography-generator to generate
// a bibliography.
namespace Citations {
namespace detail {
extern std::map<std::string, std::string> citations;
};

// see the instructions in https://inspirehep.net/bibliography-generator
// on what citation keys are valid
inline void register_citation(const std::string& label, const std::string& citation_key)
{
  if (!Mpi::is_main_rank())
    return;
  detail::citations[label] = citation_key;
}

void print_citations(std::ostream&);

} // namespace Citations

#endif
