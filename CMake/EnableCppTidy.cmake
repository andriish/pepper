if (PEPPER_CLANG_TIDY_ENABLED)
  find_program(CLANG_TIDY_COMMAND NAMES clang-tidy REQUIRED)
  message(STATUS "This build will check source files using clang-tidy.")
endif()
