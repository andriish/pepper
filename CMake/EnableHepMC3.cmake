if (NOT PEPPER_HepMC3_DISABLED)

  find_package(HepMC3 3.02.04 QUIET)

  if (HepMC3_FOUND)

    message(STATUS "This build will use HepMC3.")
    include_directories("${HEPMC3_INCLUDE_DIR}")

    # make zstr available for compressing on the fly
    FetchContent_Declare(
      zstr
      GIT_REPOSITORY https://github.com/mateidavid/zstr.git
      GIT_TAG v1.0.7
      )
    FetchContent_MakeAvailable(zstr)
    if (NOT BUILD_SHARED_LIBS)
      # zstr exports build directory specific include directories, which breaks
      # exporting Pepper as a library (which depends on zstr). Here, we
      # override the include directory to properly differentiate between build
      # and install interfaces.
      # First read the directories, so we can use them after removing the property.
      get_property(
        ZSTR_INTERFACE_INCLUDE_DIRECTORIES
        TARGET zstr
        PROPERTY INTERFACE_INCLUDE_DIRECTORIES
        )
      # Second, remove the property (we can not just use
      # target_include_directories without explicit removal first, because
      # target_include_directories only adds additional directories, but does
      # not change/remove existing ones).
      set_property(TARGET zstr PROPERTY INTERFACE_INCLUDE_DIRECTORIES "")
      # Third, and finally, set proper include directories, wrapping the given
      # property in the correct build interface, and adding an install
      # interface.
      target_include_directories(
        zstr
        INTERFACE
        $<BUILD_INTERFACE:${ZSTR_INTERFACE_INCLUDE_DIRECTORIES}>
        $<INSTALL_INTERFACE:include/pepper>
        )
      install(TARGETS zstr EXPORT PepperTargets)
    endif()

  else()

    message(STATUS "This build will not use HepMC3 (not found).")

  endif()

else()

  message(STATUS "This build will not use HepMC3 support (disabled by user).")
  set(HepMC3_FOUND 0)

endif()
