if (NOT PEPPER_HDF5_DISABLED)

  set(HDF5_PREFER_PARALLEL true)
  find_package(HDF5 QUIET)

  if (HDF5_FOUND)

    message(STATUS "This build will use HDF5 support.")

    # turn off everything we can before fetching HighFive
    set(HIGHFIVE_UNIT_TESTS Off CACHE STRING "Enable unit tests (requires Catch2 to be present)")
    mark_as_advanced(HIGHFIVE_UNIT_TESTS)
    set(HIGHFIVE_EXAMPLES Off)
    set(HIGHFIVE_BUILD_DOCS Off)
    set(HIGHFIVE_USE_BOOST Off)

    # make HighFive available
    FetchContent_Declare(
      highfive
      GIT_REPOSITORY https://github.com/BlueBrain/HighFive.git
      GIT_TAG v2.6.2
      )
    # WORKAROUND: Use the following instead of FetchContent_MakeAvailable,
    # because EXCLUDE_FROM_ALL needs to be added via an explicit call to
    # add_subdirectory before CMake 3.28 (which we do not require yet).
    # See https://stackoverflow.com/questions/65527126/disable-install-for-fetchcontent
    FetchContent_GetProperties(highfive)
    if(NOT highfive_POPULATED)
      FetchContent_Populate(highfive)
      add_subdirectory(${highfive_SOURCE_DIR} ${highfive_BINARY_DIR} EXCLUDE_FROM_ALL)
    endif()
    if (NOT BUILD_SHARED_LIBS)
      install(TARGETS HighFive libheaders libdeps EXPORT PepperTargets)
    endif()

  else()

    message(STATUS "This build will not use HDF5 (not found).")

  endif()

else()

  message(STATUS "This build will not use HDF5 support (disabled by user).")
  set(HDF5_FOUND 0)

endif()
