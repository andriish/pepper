find_program(CPPCHECK_COMMAND NAMES cppcheck)
if (CPPCHECK_COMMAND)
  message(STATUS "This build will check source files using cppcheck.")
  set(
    CMAKE_CXX_CPPCHECK
    "${CPPCHECK_COMMAND}"
    "--quiet"
    # ignore errors in external code
    "--suppress=*:${CMAKE_SOURCE_DIR}/tests/unit_tests/doctest.h"
    "--suppress=*:${CMAKE_SOURCE_DIR}/extern/HighFive/include/highfive/bits/*.hpp"
    # ignore some internal cppcheck errors in our code
    "--suppress=cppcheckError:${CMAKE_SOURCE_DIR}/src/recursion.cpp"
    "--suppress=cppcheckError:${CMAKE_SOURCE_DIR}/src/weyl.cpp"
    "--suppress=internalAstError:${CMAKE_SOURCE_DIR}/src/process_integrator.cpp"
    # the following can be used when we want to be more strict:
    #"--enable=warning,style"
    )
endif()
