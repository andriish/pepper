# This file is adapted from CMake's own cmake-3.24/Modules/FindGSL.cmake

#[=======================================================================[.rst:
FindLHAPDF
--------

Find the LHAPDF library includes and libraries.

Imported Targets
^^^^^^^^^^^^^^^^

If LHAPDF is found, this module defines the following :prop_tgt:`IMPORTED`
targets::

 LHAPDF::lhapdf      - The main LHAPDF library.

Result Variables
^^^^^^^^^^^^^^^^

This module will set the following variables in your project::

 LHAPDF_FOUND          - True if LHAPDF found on the local system
 LHAPDF_INCLUDE_DIRS   - Location of LHAPDF header files.
 LHAPDF_LIBRARIES      - The LHAPDF libraries.
 LHAPDF_VERSION        - The version of the discovered LHAPDF install.

Hints
^^^^^

Set ``LHAPDF_ROOT_DIR`` to a directory that contains a LHAPDF installation.

This script expects to find libraries at ``$LHAPDF_ROOT_DIR/lib`` and the LHAPDF
headers at ``$LHAPDF_ROOT_DIR/include/lhapdf``.  The library directory may
optionally provide Release and Debug folders. If available, the library
named ``lhapdfd`` is recognized as a debug library.
For Unix-like systems, this script will use ``$LHAPDF_ROOT_DIR/bin/lhapdf-config``
(if found) to aid in the discovery of LHAPDF.

Cache Variables
^^^^^^^^^^^^^^^

This module may set the following variables depending on platform and type
of LHAPDF installation discovered.  These variables may optionally be set to
help this module find the correct files::

 LHAPDF_CONFIG_EXECUTABLE   - Location of the ``lhapdf-config`` script (if any).
 LHAPDF_LIBRARY             - Location of the LHAPDF library.
 LHAPDF_LIBRARY_DEBUG       - Location of the debug LHAPDF library (if any).

#]=======================================================================]

include(FindPackageHandleStandardArgs)

#=============================================================================
# If the user has provided ``LHAPDF_ROOT_DIR``, use it!  Choose items found
# at this location over system locations.
if( EXISTS "$ENV{LHAPDF_ROOT_DIR}" )
  file( TO_CMAKE_PATH "$ENV{LHAPDF_ROOT_DIR}" LHAPDF_ROOT_DIR )
  set( LHAPDF_ROOT_DIR "${LHAPDF_ROOT_DIR}" CACHE PATH "Prefix for LHAPDF installation." )
endif()

#=============================================================================
# As a first try, use the PkgConfig module.  This will work on many
# *NIX systems.  See :module:`findpkgconfig`
# This will return ``LHAPDF_INCLUDEDIR`` and ``LHAPDF_LIBDIR`` used below.
if( NOT EXISTS "${LHAPDF_ROOT_DIR}" )
  find_package(PkgConfig QUIET)
  pkg_check_modules( LHAPDF QUIET lhapdf )

  if( EXISTS "${LHAPDF_INCLUDEDIR}" )
    get_filename_component( LHAPDF_ROOT_DIR "${LHAPDF_INCLUDEDIR}" DIRECTORY CACHE)
    set( LHAPDF_USE_PKGCONFIG ON )
  endif()
endif()

#=============================================================================
# As a second try, use the lhapdf-config executable.
if( NOT EXISTS "${LHAPDF_ROOT_DIR}" )
  find_program( LHAPDF_CONFIG_EXECUTABLE
    NAMES lhapdf-config
    )
  if( EXISTS "${LHAPDF_CONFIG_EXECUTABLE}" )
    execute_process(
      COMMAND "${LHAPDF_CONFIG_EXECUTABLE}" --prefix
      OUTPUT_VARIABLE LHAPDF_ROOT_DIR
      OUTPUT_STRIP_TRAILING_WHITESPACE )
    set( LHAPDF_USE_CONFIG ON )
  endif()
endif()

#=============================================================================
# Set LHAPDF_INCLUDE_DIRS and LHAPDF_LIBRARIES. If we skipped the PkgConfig step, try
# to find the libraries at $LHAPDF_ROOT_DIR (if provided) or in standard system
# locations.  These find_library and find_path calls will prefer custom
# locations over standard locations (HINTS).  If the requested file is not found
# at the HINTS location, standard system locations will be still be searched
# (/usr/lib64 (Redhat), lib/i386-linux-gnu (Debian)).

find_path( LHAPDF_INCLUDE_DIR
  NAMES LHAPDF/LHAPDF.h
  HINTS ${LHAPDF_ROOT_DIR}/include ${LHAPDF_INCLUDEDIR}
)
find_library( LHAPDF_LIBRARY
  NAMES LHAPDF
  HINTS ${LHAPDF_ROOT_DIR}/lib ${LHAPDF_LIBDIR}
  PATH_SUFFIXES Release Debug
)
# Do we also have debug versions?
find_library( LHAPDF_LIBRARY_DEBUG
  NAMES LHAPDFd LHAPDF
  HINTS ${LHAPDF_ROOT_DIR}/lib ${LHAPDF_LIBDIR}
  PATH_SUFFIXES Debug
)
set( LHAPDF_INCLUDE_DIRS ${LHAPDF_INCLUDE_DIR} )
set( LHAPDF_LIBRARIES ${LHAPDF_LIBRARY} )

# If we didn't use PkgConfig, try to find the version via lhapdf-config or by
# reading lhapdf/Version.h.
if( NOT LHAPDF_VERSION )
  # 1. If lhapdf-config exists, query for the version.
  find_program( LHAPDF_CONFIG_EXECUTABLE
    NAMES lhapdf-config
    HINTS "${LHAPDF_ROOT_DIR}/bin"
    )
  if( EXISTS "${LHAPDF_CONFIG_EXECUTABLE}" )
    execute_process(
      COMMAND "${LHAPDF_CONFIG_EXECUTABLE}" --version
      OUTPUT_VARIABLE LHAPDF_VERSION
      OUTPUT_STRIP_TRAILING_WHITESPACE )
  endif()

  # 2. If lhapdf-config is not available, try looking in lhapdf/Version.h
  if( NOT LHAPDF_VERSION AND EXISTS "${LHAPDF_INCLUDE_DIRS}/LHAPDF/Version.h" )
    file( STRINGS "${LHAPDF_INCLUDE_DIRS}/LHAPDF/Version.h" lhapdf_version_h_contents REGEX "define LHAPDF_VERSION" )
    string( REGEX REPLACE ".*define[ ]+LHAPDF_VERSION[ ]+\"([^\"]*)\".*" "\\1" LHAPDF_VERSION ${lhapdf_version_h_contents} )
  endif()

  # might also try scraping the directory name for a regex match "lhapdf-X.X"
endif()

#=============================================================================
# handle the QUIETLY and REQUIRED arguments and set LHAPDF_FOUND to TRUE if all
# listed variables are TRUE
find_package_handle_standard_args( LHAPDF
  FOUND_VAR
    LHAPDF_FOUND
  REQUIRED_VARS
    LHAPDF_INCLUDE_DIR
    LHAPDF_LIBRARY
  VERSION_VAR
    LHAPDF_VERSION
    )

mark_as_advanced( LHAPDF_ROOT_DIR LHAPDF_VERSION LHAPDF_LIBRARY LHAPDF_INCLUDE_DIR
  LHPADF_LIBRARY_DEBUG
  LHAPDF_USE_PKGCONFIG LHAPDF_USE_CONFIG LHAPDF_CONFIG )

#=============================================================================
# Register imported libraries:
# 1. If we can find a Windows .dll file (or if we can find both Debug and
#    Release libraries), we will set appropriate target properties for these.
# 2. However, for most systems, we will only register the import location and
#    include directory.

# Look for dlls, or Release and Debug libraries.
if(WIN32)
  string( REPLACE ".lib" ".dll" LHAPDF_LIBRARY_DLL       "${LHAPDF_LIBRARY}" )
  string( REPLACE ".lib" ".dll" LHAPDF_LIBRARY_DEBUG_DLL "${LHAPDF_LIBRARY_DEBUG}" )
endif()

if( LHAPDF_FOUND AND NOT TARGET LHAPDF::lhapdf )
  if( EXISTS "${LHAPDF_LIBRARY_DLL}" )

    # Windows systems with dll libraries.
    add_library( LHAPDF::lhapdf      SHARED IMPORTED )

    # Windows with dlls, but only Release libraries.
    set_target_properties( LHAPDF::lhapdf PROPERTIES
      IMPORTED_LOCATION_RELEASE         "${LHAPDF_LIBRARY_DLL}"
      IMPORTED_IMPLIB                   "${LHAPDF_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES     "${LHAPDF_INCLUDE_DIRS}"
      IMPORTED_CONFIGURATIONS           Release
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX" )

    # If we have both Debug and Release libraries
    if( EXISTS "${LHAPDF_LIBRARY_DEBUG_DLL}" )
      set_property( TARGET LHAPDF::lhapdf APPEND PROPERTY IMPORTED_CONFIGURATIONS Debug )
      set_target_properties( LHAPDF::lhapdf PROPERTIES
        IMPORTED_LOCATION_DEBUG           "${LHAPDF_LIBRARY_DEBUG_DLL}"
        IMPORTED_IMPLIB_DEBUG             "${LHAPDF_LIBRARY_DEBUG}" )
    endif()

  else()

    # For all other environments (ones without dll libraries), create
    # the imported library targets.
    add_library( LHAPDF::lhapdf      UNKNOWN IMPORTED )
    set_target_properties( LHAPDF::lhapdf PROPERTIES
      IMPORTED_LOCATION                 "${LHAPDF_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES     "${LHAPDF_INCLUDE_DIRS}"
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX" )
  endif()
endif()
