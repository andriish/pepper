if (NOT PEPPER_Chili_DISABLED)

  find_package(Chili QUIET)

  if (Chili_FOUND)

    message(STATUS
      "This build will use the Chili phase-space generator support.")

  else()

    message(STATUS
      "This build will not use the Chili phase-space generator (not found).")

  endif()

else()

  message(STATUS "This build will not use Chili support (disabled by user).")
  set(Chili_FOUND 0)

endif()
