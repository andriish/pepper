function(SetValueColumnEntry name var)
  if(var)
    set(${name} "ON" PARENT_SCOPE)
  else()
    set(${name} "--" PARENT_SCOPE)
  endif()
endfunction()

function(PrintLine label var)
  SetValueColumnEntry(val "${var}")
  message(STATUS "│ ${label} ${val} │")
endfunction()

message(STATUS "┌────────── SUMMARY ──────────┐")
PrintLine("MPI parallelisation     " "${MPI_FOUND}")
PrintLine("VCL vectorisation       " "${VCL_ENABLED}")
PrintLine("HepMC3/LHEF output      " "${HepMC3_FOUND}")
PrintLine("LHEH5 (HDF5) output     " "${HDF5_FOUND}")
if(HDF5_FOUND)
PrintLine(" HDF5 parallelisation   " "${HDF5_IS_PARALLEL}")
endif()
PrintLine("Standalone Chili        " "${Chili_FOUND}")
PrintLine("LHAPDF                  " "${LHAPDF_FOUND}")
if(LHAPDF_FOUND)
PrintLine(" Kokkos support         " "${KOKKOS_LHAPDF_FOUND}")
endif()
message(STATUS "└─────────────────────────────┘")
