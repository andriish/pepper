include(FetchContent)
message(CHECK_START "Making sure process data is available (download and extract if necessary)")
if(POLICY CMP0135)
  cmake_policy(PUSH)
  cmake_policy(SET CMP0135 NEW)
endif()
FetchContent_Declare(
  process_data
  URL        https://zenodo.org/records/10207811/files/pepper_data.tar.gz?download=1
  URL_HASH   MD5=403b975806cfac870a010080e2a6d194
  SOURCE_DIR data
  )
FetchContent_MakeAvailable(process_data)
if(POLICY CMP0135)
  cmake_policy(POP)
endif()
message(CHECK_PASS "done")
set(PEPPER_BUILD_DATADIR "${CMAKE_CURRENT_BINARY_DIR}/data/")
install(
  DIRECTORY "${PEPPER_BUILD_DATADIR}"
  DESTINATION "${CMAKE_INSTALL_DATADIR}/pepper_data"
  MESSAGE_NEVER
  )
