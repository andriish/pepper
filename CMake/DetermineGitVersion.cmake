# Adapted from https://cliutils.gitlab.io/modern-cmake/chapters/projects/submodule.html

message(CHECK_START "Finding repository root")
if (EXISTS "${PROJECT_SOURCE_DIR}/.git")
  set(REPO_ROOT_FOUND TRUE)
  message(CHECK_PASS "found (\".git\")")
else()
  message(CHECK_FAIL "not found")
endif()

if (REPO_ROOT_FOUND)
  find_package(Git)
else()
  message(STATUS "Skip finding git (not needed as there is no repository root)")
endif()

set(PACKAGE_GIT_VERSION Unknown)
if (REPO_ROOT_FOUND AND GIT_FOUND)
  message(CHECK_START "Finding package git version")
  # put current git version number into PACKAGE_GIT_VERSION
  execute_process(
    COMMAND ${GIT_EXECUTABLE} rev-parse --short HEAD
    WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
    OUTPUT_VARIABLE PACKAGE_GIT_VERSION
    OUTPUT_STRIP_TRAILING_WHITESPACE
  )
  if (NOT ${PACKAGE_GIT_VERSION} EQUAL Unknown)
    message(CHECK_PASS "found (${PACKAGE_GIT_VERSION})")
  else()
    message(CHECK_FAIL "failed")
  endif()
endif()
