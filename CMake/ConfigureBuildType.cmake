
# For reference, the default CMake configurations and their initial flags are:
#
# - Release="-O3 -DNDEBUG"
# - Debug="-g"
# - RelWithDebInfo="-O2 -g -DNDEBUG"
# - MinSizeRel="-Os -DNDEBUG"
#
# This information has been found using:
#
#include(CMakePrintHelpers)
#cmake_print_variables(CMAKE_CXX_FLAGS_RELEASE
#  CMAKE_CXX_FLAGS_DEBUG
#  CMAKE_CXX_FLAGS_RELWITHDEBINFO
#  CMAKE_CXX_FLAGS_MINSIZEREL)
#
# Below, we define our own, "Trace", which is suited e.g. to generate flame
# graphs, and "Asan", which enables address sanitizer checks.

# set a default build type if none was specified
if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to 'RelWithDebInfo' as none was specified.")
  set(CMAKE_BUILD_TYPE RelWithDebInfo
      CACHE STRING "Choose the type of build." FORCE)
else()
  message(STATUS "This is a ${CMAKE_BUILD_TYPE} build.")
endif()

# set the possible values of build type for cmake-gui, ccmake
set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
  "Debug" "Release" "MinSizeRel" "RelWithDebInfo" "Asan" "Trace")

# set Asan compiler flags
set(CMAKE_C_FLAGS_ASAN
  "${CMAKE_C_FLAGS_DEBUG} -fsanitize=address -fno-omit-frame-pointer" CACHE STRING
  "Flags used by the C compiler for Asan build type or configuration." FORCE)
set(CMAKE_CXX_FLAGS_ASAN
  "${CMAKE_CXX_FLAGS_DEBUG} -fsanitize=address -fno-omit-frame-pointer" CACHE STRING
  "Flags used by the C++ compiler for Asan build type or configuration." FORCE)
set(CMAKE_EXE_LINKER_FLAGS_ASAN
  "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -fsanitize=address" CACHE STRING
  "Linker flags to be used to create executables for Asan build type." FORCE)
set(CMAKE_SHARED_LINKER_FLAGS_ASAN
  "${CMAKE_SHARED_LINKER_FLAGS_DEBUG} -fsanitize=address" CACHE STRING
  "Linker lags to be used to create shared libraries for Asan build type." FORCE)

# set Trace compiler flags
set(CMAKE_C_FLAGS_TRACE
  "${CMAKE_C_FLAGS_RELWITHDEBINFO} -fno-omit-frame-pointer" CACHE STRING
  "Flags used by the C compiler for Asan build type or configuration." FORCE)
set(CMAKE_CXX_FLAGS_TRACE
  "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} -fno-omit-frame-pointer" CACHE STRING
  "Flags used by the C++ compiler for Asan build type or configuration." FORCE)

add_compile_options(-Wall -Wextra -Wshadow)
