set(LHAPDF_FOUND FALSE)
set(KOKKOS_LHAPDF_FOUND FALSE)

if (NOT PEPPER_LHAPDF_DISABLED)

  # FindLHAPDF will only use LHAPDF_ROOT_DIR, but for consistency we prefer the
  # LHAPDF_ROOT variable
  if (EXISTS LHAPDF_ROOT)
    set (LHAPDF_ROOT_DIR "${LHAPDF_ROOT}")
  endif()
  find_package(LHAPDF 6.5)

  if (LHAPDF_FOUND)

    link_directories(${LHAPDF_LIBDIR})

    if (NOT KOKKOS_LHAPDF_DISABLED)
      find_file(CUPDF_H_PATH LHAPDF/CuPDF.h
        PATHS ${LHAPDF_INCLUDE_DIRS} NO_DEFAULT_PATH)
      if(CUPDF_H_PATH)
        set(KOKKOS_LHAPDF_FOUND TRUE)
      endif()
    endif()

    if (KOKKOS_LHAPDF_FOUND)
      message(STATUS "This build will use Kokkos-enabled LHAPDF.")
    else()
      message(STATUS "This build will use LHAPDF.")
    endif()

    # In order to make Pepper available as a library, we need to make our
    # FindLHAPDF CMake script available, such that we can use it when adding
    # Pepper as a dependency. We can remove this later, when we can be sure
    # that all LHAPDF versions we support ship such a script themselves.
    install(FILES "${PROJECT_SOURCE_DIR}/CMake/Modules/FindLHAPDF.cmake"
      DESTINATION "${CMAKE_INSTALL_LIBDIR}/cmake/Pepper/Modules")

  else()

    message(STATUS "This build will not use LHAPDF (not found).")

  endif()

else()

  message(STATUS "This build will not use LHAPDF (disabled by user).")

endif()
