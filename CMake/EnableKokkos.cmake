find_package(Kokkos)
if (NOT Kokkos_FOUND)
  message(FATAL_ERROR "Cannot find Kokkos (see above for details). The main"
    " versions of Pepper require Kokkos to compile for various CPU and GPU"
    " architectures and make use of their parallel computing power. If you"
    " can't or don't want to provide Kokkos now, you can switch to the"
    " \"native\" version of Pepper (either by switching to the \"native\" git"
    " branch, `git checkout native`, or by downloading one of the \"native\""
    " release tarballs from https://gitlab.com/spice-mc/pepper/-/releases)."
    " The \"native\" versions do not require Kokkos, but can only be compiled"
    " for running on a single CPU thread or on GPU with CUDA support.")
endif()

include(CMake/FixMacPortsKokkos.cmake)
install(
  FILES "${PROJECT_SOURCE_DIR}/CMake/FixMacPortsKokkos.cmake"
  DESTINATION "${CMAKE_INSTALL_LIBDIR}/cmake/Pepper/Utilities")
